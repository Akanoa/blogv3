+++
title = "Physique Nucléaire et Électrochimie"
date = 2023-01-12
draft = false
path = "/electrochimie"

[taxonomies]
categories = ["Sciences"]
tags = ["chimie", "physique"]

[extra]
lang = "fr"
toc = true
show_comment = true
math = true

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Physique Nucléaire et Électrochimie" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/electrochimie.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Physique Nucléaire et Électrochimie" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/electrochimie.png" },
    { property = "og:url", content="https://lafor.ge/electrochimie" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]
+++

Actuellement en rédaction d'un article sur les transistors en électronique.

Je me suis aperçu en rédigeant, que comprendre le domaine nécessitait mine de rien pas mal de base de la compréhension de la 
matière à l'échelle atomique et moléculaire. Ainsi que les instéractions qui s'y déroulent à de telles échelles.

Je vous propose donc de faire un rapide dégrossi des cours de physique et de chimie de lycée qui permettront à chacun et chacune de ne pas être 
paumé quand on rentrera dans le vif du sujet. ^^

Aussi veuillez m'excuser, j'utiliserai des représentation fausses mais pratiques pour comprendre comme le modèle orbitaire de l'atome.

Commençons avec un peu d'Histoire. 😀

## Physique nucléaire

Dans l'Antiquité, on pensais que la Matière était composé d'Air, d'Eau, de Terre et de Feu, oui, oui comme dans Avatar mais pas les schtroumpfs sous stéroïdes.

Tous? Non, deux personnes appellés [Démocrite](https://fr.wikipedia.org/wiki/D%C3%A9mocrite) et [Leucippe](https://fr.wikipedia.org/wiki/Leucippe) pensent différemment,
pour eux si on prend par exemple un morceau de charbon et que l'on coupe en deux ce morceau, on aura deux morceaux deux fois plus petit, on coupe encore en deux, 4 fois plus petit, etc.... Jusqu'à ce qu'on ne puisse plus rien couper. On a arrive à l'atome ( a : pas , tome : couper), incoupable.

Cette expérience de pensée postule que la Matière, toute la matière est composé de petite billes et que ces billes sont séparées par du vide, on est au Vème siècle avant JC.

{% note(title="") %}
Cette expérience de pensée a été réalisée également en [Inde](https://fr.wikipedia.org/wiki/Kanada) un siècle avant.
{% end %}

Mais ce n'est qu'à l'époque moderne, que l'on est capable de [visualiser](https://www.slate.fr/life/71883/ibm-produit-la-premiere-video-atomique) les atomes.

Par contre, les Anciens s'était trompés, l'atome n'est pas insécable.

### Proton

Le proton est une particule élémentaire, elle n'est théoriquement pas sécable et éternelles.

{% important() %}

C'est deux affiramations sont fausses:
- les protons peuvent se transformer en autre chose avec une très faible probabilité
- les protons sont composé d'autre choses

{% end %}

Voici un proton.

{{ image(path="transistor/proton.png", width="30%" alt="un proton") }}

Ce qu'il faut savoir de lui c'est qu'il est chargé positivement.

### Electron

Une autre particule élementaire qui elle est réellement élémentaire c'est l'électron, il est responsable entre autre de l'électricité!

Voici un électron.

{{ image(path="transistor/electron.png", width="30%" alt="un électron") }}

Contrairement au proton, l'électron est chargé négativement.

L'intensité de la charge, sa valeur est identique au proton mais inversée. Cette valeur se nomme charge élémentaire.

### Hydrogène

Ce qui fait que comme deux aimants de pôles opposés, l'électron et le proton s'attire naturellement.

Ce couple forme un atome.

{{ image(path="transistor/hydrogene.png", width="30%" alt="un atome d'hydrogène") }}

Comme la charge d'un proton et d'un électron est égale mais inversée en signe 

`X + (-X) = 0`.

L'atome est lui électriquement neutre et stable.

### Hélium instable

Mais ne nous arrêtons pas en si bon chemin. Essayons de mettre deux protons.

{{ image(path="transistor/helium1.png", width="50%" alt="un atome d'hélium sans neutrons") }}

On se retrouve avec deux protons et deux électrons.

`2X + (-2X) = 0`.

Le compte est bon !

Sauf que non, si les aimants s'attirent, ils se repoussent aussi.

{{ image(path="transistor/helium2.png", width="50%" alt="un atome d'hélium sans neutrons") }}

Et donc, comme des aimants de pôles identiques, les protons se repoussent !

Pour cela nous avons besoin d'un nouveau challenger.

### Neutron

Celui-ci se nomme neutron, de même masse que le proton.

Voici un neutron.


{{ image(path="transistor/neutron.png", width="30%" alt="un neutron") }}

Il est électriquement neutre, sa chage est de 0

Par contre, il a la propriété de pouvoir faire cohabiter les protons entre eux.

{{ image(path="transistor/helium3.png", width="50%" alt="2 protons et 2 neutrons") }}

### Hélium stable

Ainsi, nous allons pouvoir construire notre atome a 2 protons.

{{ image(path="transistor/helium4.png", width="70%" alt="un atome d'hélium") }}

Comme la charge du neutrons est nulle, elle n'entre pas en ligne de compte et donc notre atome est stable.

Voici donc l'hélium. Le deuxième élément le plus léger et abondant dans l'Univers après l'hydrogène.

{% note(title="") %}
Le nombre de protons et de neutrons, n'est pas obligatoirement égal.

Ici, il n'y a qu'un, seul proton.

{{ image(path="transistor/helium5.png", width="70%" alt="un atome d'hélium") }}

On appelle cela un isotope.

Si le nombre de neutrons est insuffisant ou excédentaires, l'atome se cassera, on nomme cela un isotope instable, c'est une des façon de faire de la radioactivité.

{% end %}

### Tableau de Mendeleïev

Un génie russe comme il y a peu dans l'histoire, a eu une intuition de génie. Il a créé un tableau des éléments. Ce tableau se nomme le tableau périodique des éléments.

Nous le devons à [Dmitri Mendeleïev](https://fr.wikipedia.org/wiki/Dmitri_Mendele%C3%AFev).


Ce tableau permet de connaître la nature d'un éléménents à son nombre de protons et de neutrons. Et donc par extensions être capables de déterminer le nombres d'électrons qui gravitent autours du noyaux de l'atome que l'on observe.

Pour plus d'information sur [DmitSwag](https://www.youtube.com/watch?v=0lNuTSz6KVM) et son tableau.

Ceux-ci sont rangé par poids, plus il y a de protons, plus c'est lourds.

{{ image(path="transistor/mendeleiv1.png", width="100%" alt="Tableau de Mendeleiv") }}

Si on zoom, sur un élément, l'hélium par exemple, on voit que son numéro atomique est `2`. Sous entendu il y a 2 protons.

{{ image(path="transistor/mendeleiv0.png", width="25%" alt="Tableau de Mendeleiv") }}

### Orbites atomiques

Donc nous avons un noyaux et des électrons.

{{ image(path="transistor/atome1.png", width="50%" alt="un noyau et des électron") }}

Sauf, que ces électrons ne se placent pas n'importe comment.

La première orbite peut contenir au maximum 2 électrons.

{{ image(path="transistor/atome2.png", width="50%" alt="Un noyau et deux électrons") }}

Si un troisième arrive, il ne pourra pas se placer sur la 1ère orbites.

{{ image(path="transistor/atome3.png", width="65%" alt="Un noyau et trois électrons") }}

Il viendra se mettre sur une orbites plus éloignée.

On donne des noms au orbites.

- K pour la première
- L pour la seconde
- ...

{{ image(path="transistor/atome4.png", width="65%" alt="Un noyau et deux électrons") }}

L'orbite K a au maximum 2 électrons.

L'orbite L a au maximum 8 électrons.

{{ image(path="transistor/atome5.png", width="65%" alt="Un noyau et deux électrons") }}

On peut alors créer des couches successives qui auront leurs nombres d'électrons possible.

{{ image(path="transistor/atome6.png", width="65%" alt="Un noyau et deux électrons") }}

Un explication est qu'il n'y a plus de place pour accueillirs l'électron. Et que comme pour une route embouteillée, on prend les petits chemins.

L'electron fait de même, il voudra bien être à côté du noyau et des protons qui l'attirent, mais il n'y a plus de place, du coup il monte d'un cran.

Plus l'orbite est éloignée, plus elle est grande et donc plus il y a de place.

C'est pour ça que le nombre de places grandi au fur et à mesure que l'on s'éloigne du noyau.

Dernière remarque, les électrons dans les couches externes sont vérouillés, il ne peuvent servir à aucune interaction ou réaction.

## Chimie

La chimie est l'art de manipuler les éléments de la nature pour en créer de nouvelles structures.

### Molécule

Maintenant que l'on a eu un aperçu de l'atome et de ce que c'était.

Que se passerait-il si l'on voulais par exemple crér du `CO2`, le fameux gaz carbonique.

Il nous faut d'abord des participants.

Nous avons besoin de `C`, le carbonne et de `O`, l'oxygène.

Allons les rechercher dans le tableau.

{{ image(path="transistor/mendeleiv2.png", width="100%" alt="Tableau de Mendeleiv zoom sur le carbone") }}

Ils sont respectivement à l'emplacement `6` et `8`.

Voyons le carbonne tout d'abord.

S'il y a 6 protons, c'est qu'il y a nécessairement 6 électrons.

On sait qu'il y en a 2 en couche `K`, il reste donc 4 à placer dans la couche `L`.

Ce dessin représente le modèle atomique orbitaire du carbone. On y voit les 4 emplacement vide de la couche `L`. 

{{ image(path="transistor/carbone.png", width="90%" alt="Modèle atomique du carbone") }}

On fait de même pour l'oxygène.

{{ image(path="transistor/oxygene.png", width="90%" alt="Modèle atomique de l'oxygène") }}

On a ici 2 emplacement vide sur la couche `L`.

On va alors simplifier les écritures en enlevant les électrons internes qui sont vérouillés au noyau, puis en faisant disparaître également l'orbite.

On applique la règle de l'octet, qui dit que les atomes recherche la stabilité en ayant 8 électrons autours d'eux en couche externe.

Il manque à notre atome de carbone 4 électrons pour y arrivers.

On ajoute alors, un `(-4)` pour symboliser le manque de 4 électrons sur la couche externe.

{{ image(path="transistor/carbone2.png", width="90%" alt="Modèle atomique simplifié du carbone") }}

On fait de même avec l'oxygène.

{{ image(path="transistor/oxygene2.png", width="90%" alt="Modèle atomique simplifié de l'oxygène") }}

Nous voulons faire du `CO2`, rapprochons du carbonne et de l'oxygène

{{ image(path="transistor/lewis1.png", width="70%" alt="carbone et oxygène") }}

En partageant un de ses électron avec le carbone, l'oxygène diminue son instabilité de 1.

De son côté, le carbone fait de même.

Instabilité du carbone `(-3)`, instabilité de l'oxygène `(-1)`.

{{ image(path="transistor/lewis2.png", width="50%" alt="carbone et oxygène") }}

Seulement, l'oxygène n'a pas qu'un seul emplacement à remplir. Il va donc partager un second électron avec le carbone.

Ce qui va permettre à l'oxygène de devenir stable. 😀

{{ image(path="transistor/lewis3.png", width="50%" alt="carbone et oxygène") }}

Par contre le carbone est toujours en déficit. Il attend deux nouveau électrons.

Ceux-ci vont lui être fournis par un second atome d'oxygène egalement en manque de deux électrons.

{{ image(path="transistor/lewis4.png", width="60%" alt="carbone et 2 oxygène") }}

En partageant également se électron, l'oxygène devient stable et le carbone aussi.

{{ image(path="transistor/lewis5.png", width="50%" alt="carbone et 2 oxygène") }}

Généralement on transforme les duets d'électrons en traits.

{{ image(path="transistor/lewis6.png", width="50%" alt="carbone et 2 oxygène") }}

Ceux du centre sont des duets liants et ceux des extrémités des duets non-liants.

On nomme ceci la représentation de Lewis, on utilisera celle-ci dans la suite de l'article.

### Ions

Un ion est un atome qui n'a pas le bon nombre d'électron soit il lui en manque, soit il en a en trop.

La manière la plus simple de faire des ions est de prendre du sel de cuisine et de le mettre dans l'eau.

Le sel est une molécule composé de Sodium très réactif avec l'eau, tapez "sodium eau" sur youtube. ^^ 

Et de Chlore, [mortel](https://www.cchst.ca/oshanswers/chemicals/chem_profiles/chlorine.html). ☠

> Du coup, pourquoi quand on met du sel dans l'eau, on ne meurt pas dans une explosion de gaz mortel?

Et bien, ce n'est pas du sodium ou du chlore, mais leurs ions que l'on retrouve dans l'eau.

L'eau est se qu'on nomme un solvant. Autrement dit, l'eau est capable de couper les liaison des molécules.

Prenons, le tableau de Mendeleïev, celui-ci nous dit que le chlore a pour numéro atomique `17` et le sodium `11`.

Autement dit, le sodium possède `1` électron sur sa couche `M`, il lui en manque `7` pour être stable.

Il aura donc tendance, à vouloir en perdre 1, pour avoir une couche complète la couche `L` en l'occurence et perdre sa couche `M`.

Mais alors, il devient chargé positivement, car il a plus de protons que d'électron, cela devient un ion sodium.

En voici la représentation de Lewis.

{{ image(path="transistor/sel1.png", width="60%" alt="représentation de lewis de l'ion sodium") }}

Le chlore possède `7` électrons sur sa couche `M`, il lui en manque `1` pour être stable.

Il aura tendance à capter un électron pour compléter sa couche `M`.

Mais se faisant, il possède un électron de plus que de proton, cela devient un ion chlore, chargé négativement.

En voici une représentation de Lewis.

{{ image(path="transistor/sel2.png", width="60%" alt="représentation de lewis de l'ion sodium") }}

Nous avons donc le sodium qui veut se débarasser d'un électron qui à devenir positif et de l'autre le chlore à qui il manque un
électron.

Bref, ils sont fait pour être les meilleurs amis !

{{ image(path="transistor/sel3.png", width="60%" alt="représentation de lewis de l'ion sodium") }}

Par contre, même si la molécule $NaCl$ est neutre. Les ions qui la composent, ne le sont pas.

Nous avons donc du $Na^+$ et du $Cl^-$ tous les deux chargés !

Sauf que bien qu'il soit lié, on pourrait dire qu'ils sont en liaison libre et que s'ils ont la possibilité d'aller voir ailleurs ils ne vont pas se gêner !

Or l'eau qui a pour formule $H_2O$, est aussi une molécule qui aime bien s'accrocher à ce qu'elle trouve.

Elle possède à la fois un côté positif, ses ions $H^+$ et un côté négatif son ion $O^{2-}$

Petite représentation de Lewis en accéléré.

{{ image(path="transistor/sel4.png", width="60%" alt="représentation de lewis de l'eau'") }}

Et une simplification pour les dessins.

{{ image(path="transistor/sel5.png", width="30%" alt="représentation simplifiée de la molécule d'eau") }}

On a donc une molécule polarisée des deux côtés.

Un bloc de sel, ça ressemble à ça au niveau atomique.

{{ image(path="transistor/sel6.png", width="40%" alt="cristal de sel") }}

Plongeons un bloc de sel dans l'eau.

Si on représente une seule des faces du cube de sel, les molécules d'eau vont venir se rapprocher des ions du sel.

L'oxygène $O^{2-}$ de l'eau chargée négativement, va se rapprocher des ions sodium $Na^+$ chargés positivement.

L'hydrogène $H^+$, elle, chargé positivement, viendra s'accrocher aux ion chlore $Cl^-$ chargés négativement.

{{ image(path="transistor/sel7.png", width="50%" alt="cristal de sel") }}

Et pour des raisons un peu compliqués que je ne vais pas expliquer ici, les ions sont plus attirés par l'eau que par leur copains ions.

{{ image(path="transistor/sel8.png", width="50%" alt="cristal de sel") }}

Du coup, les liaisons atomiques des ions du sel se cassent un par une, tandis que l'eau commence à entourer les ions.

{{ image(path="transistor/sel9.png", width="50%" alt="cristal de sel") }}

Finalement, l'intégralité des liaisons sont coupées, les ions sont libres et l'eau devient une solution chargée électriquement.

{{ image(path="transistor/sel10.png", width="50%" alt="cristal de sel") }}

On donne un petit nom aux ions en fonction de leur charges:
- `cations` : chargés positivement, ex $Na^+$
- `anions` : chargés négativement, ex $Cl^-$ 

## Oxydo-Réduction

L'oxydoréduction est la manière de caractériser la transformation qui réalise un échange d'électrons.

Par exemple, prenons le zinc ${_{30}Zn}$.

En le décomposant en couche on obtient: ${_{30}Zn} = (K)^2(L)^8(M)^{18}(N)^2$.

Autrement dit 2 électrons sur la couche $N$, ce qui signifie que pour se stabiliser, le $Zn$ doit perdre 2 électron et devenir $Zn^{2+}$

On défini ceci par ce que l'on appelle la demi-équation: $Zn^{2+} + 2e^- \rightleftarrows Zn$.

Le symbole $\rightleftarrows$ indique que la réaction est réversible, le $Zn^{2+}$ peut se transformer en $Zn$ et inversement.

Un peu de vocabulaire.

- Un `oxydant` que l'on nomme $Ox$ est une substance qui capte des électrons.
- Un `réducteur` que l'on nomme $Red$  est une substance qui donne des électrons.

On généralise la demi-équation en:

$Ox + ne^- \rightleftarrows Red$

Par contre le transfert ne peut pas se faire magiquement, il faut une autre demi-équation pour faire le travail.

Si l'on prend le cuivre $_{29}Cu$, il possède deux ions, le premier attendu $Cu^+$ et le second moins $Cu^{2+}$. Je vous laisse faire les calculs de couches
pour vous en convaincre et si vous voulez vraiment creuser, c'est [ici](http://bouteloup.pierre.free.fr/art/kle.pdf) ^^

Quoi qu'il en soit nous allons utilisé la demi-équation: $Cu^{2+} + 2e^- \rightleftarrows Cu$

La réaction d'oxydo-réduction c'est lorsque l'on met en intéractions l'oxydant d'un couple avec le réducteur de l'autre et inversement.

Nous avons donc ces deux équations:

$
\begin{cases}
  Cu^{2+} + 2e^- \rightleftarrows Cu \\\ \\
  Zn^{2+} + 2e^- \rightleftarrows Zn
\end{cases}
$

Comment déterminer qui sera le réducteur de qui sera l'oxydant ?

### Potentiel d'oxydo-réduction

Pour cela, il nous faut un nouvel outil, celui-ci est un indicateur de tendance à vouloir perdre ou capter des électrons.

Tout ceci a été déterminé expérimentalement et est répertorié dans un [tableau](https://www.lachimie.net/Table_potentiel_standard.pdf).

La première colonne défini l'oxydant. 

La seconde colonne, le réducteur.

Et la troisième une valeur $E^0$ en volts.

Deux lignes nous intéresse:

$Zn^{2+} +2 e- \rightleftarrows Zn : -0.76 V$

$Cu^{2+} +2 e- \rightleftarrows Cu : +0.34 V$

La valeur négative de $E^0$ signifie que le $Zn$ va donner des électrons pour se transformer en $Zn^{2+}$, le zinc va donc se dissoudre en ions.

Alors que la valeure positive $E^0$ signifie que le $Cu^{2+}$ va capter des électrons pour devenir du $Cu$, du cuivre métallique.

On peut alors enfin écrire notre vraie équation.

$Zn + Cu^{2+} \rightarrow Cu + Zn^{2+}$

Et cette fois-ci on a un flèche !  Ce qui signifie que la transformation va dans un sens bien défini.

{% note(title="Remarque") %}
Le zinc et le cuivre ne se touche jamais, seul les électrons se baladent.
{% end %}

Pour que la réaction se déroule, il faut que le réducteur qui donne suffisamment des électrons et que l'oxydant veuille bien accepter de les prendre.

## Pile de Daniell

Pour mettre en application notre savoir, nous allons construire une pile, celle-ci s'appelle la pile de Daniell.

Pour cela, il nous faut 2 récipients. 

L'un rempli de sulfate de cuivre, de formule $CuSO_4$.

- $Cu^{2+}$ est l'ion cuivre II, il est chargé positivement, c'est un cation.
- $SO_{4}^{2-}$ est l'ion sulfate, il est chargé négativement, c'est un anion.

On va également mettre un morceau de cuivre métallique de formule $Cu$.

On crée ce qu'on appelle une demi-pile.

{{ image(path="transistor/pile1.png", width="50%" alt="demi pile de cuivre") }}

L'autre rempli de sulfate de zinc, de formule $ZnSO_4$.

- $Zn^{2+}$ est l'ion zinc II, il est chargé positivement, c'est un cation.
- $SO_{4}^{2-}$ le même ion sulfate, il est chargé négativement, c'est un anion.

On va également mettre un morceau de cuivre métallique de formule $Zn$.

On crée ce qu'on appelle la deuxième demi-pile.

{{ image(path="transistor/pile2.png", width="50%" alt="demi pile de zinc") }}

{% note(title="Remarque") %}
Les deux liquides sont électriquement neutres, il y a autant de $+$ que de $-$.
{% end %}

On va alors connecter nos deux demi-piles avec un fil.

{{ image(path="transistor/pile3.png", width="60%" alt="connexion des demi-piles") }}

Rappelez-vous le couple $Zn/Zm^{2+}$ a un $E_0$ de $- 0.76 V$, autrement dit les électrons veulent partir du zinc, de l'autre côté le cuivre a un $E_0$ de $+ 0.34 V$, autrement il veut capter des électrons.

Les électrons du zinc métallique $Zn$ vont vouloir partir vers le $Cu^{2+}$ dans le deuxième récipient. Le seul chemin est le fil.

Ce qui produit un ion $Zn^{2+}$ qui va migrer dans le liquide dans lequel baigne le zinc originellement neutre.

Comme l'on rajoute du $+$ à du neutre on rend chargé positivement le liquide.

A chaque fois l'on libère 2 électrons.

On consomme aussi du zinc, la plaque diminue d'un atome de $Zn$

{{ image(path="transistor/pile4.png", width="60%" alt="libération d'électron") }}

Les électrons sortent alors du zinc et empruntent le fil en direction du cuivre.

{{ image(path="transistor/pile5.png", width="60%" alt="déplacement d'électrons dans le fil") }}

Et l'atteint finalement.

{{ image(path="transistor/pile7.png", width="60%" alt="recombination en cuivre") }}

Le $Cu^{2+}$ du liquide va alors capter les 2 électron et former du $Cu$, du cuivre métallique, qui va venir se déposer sur le cuivre existant.

En enlevant du $Cu^{2+}$ du liquide, on la rend négative.

{{ image(path="transistor/pile8.png", width="60%" alt="recombination en cuivre partie 2") }}

Atome après atome de zinc, on va rendre de plus en plus positive le liquide de gauche et négatif le liquide de droite. Et au bout d'un moment cela va stopper la réaction.

{{ image(path="transistor/pile9.png", width="60%" alt="saturation en ions") }}

Pour éviter cela, l'on va mettre un pont d'eau salée entre les deux et "fermer" le circuit.

Le principe est que l'eau salée possède à la fois des ions $Na^+$ et $Cl^-$.

Les $Cl^-$ vont se combiner avec les $Zn^{2+}$.

{{ image(path="transistor/pile10.png", width="60%" alt="migration ion chlorure") }}

et les $Na^+$ vont combler le manque de $Cu^{2+}$.

{{ image(path="transistor/pile12.png", width="60%" alt="migration ion sodium") }}

Maintenant que tout est neutre, un flux continue d'électron peut passer entre le zinc et le cuivre.

La plaque de zinc se fait manger, tandis que le cuivre grossi !

De plus en plus de $Na^+$ prenne la place du $Cu^{2+}$.

Et de l'autre côté les $Zn^{2+}$ sont neutralisé par les $Cl^-$.

Félicitations ! Vous produisez du courant !

{{ image(path="transistor/pile13.png", width="60%" alt="courant électrique") }}

Plus qu'à mettre une ampoule et on éclaire. 😁

En vrai on a une pile de 1.1 volt là ^^

$E_0(Cu/Cu^{2+}) - E_0(Zn/Zn^{2+}) = 0.34 - (- 0.76) = 1.1 V$ 

On appelle cela la *force électromotrice*. C'est ce qui oblige les électrons à se déplacer dans une direction précise.

{{ image(path="transistor/pile14.png", width="60%" alt="pile de 1.1V") }}

La réaction s'arrête lorsqu'il y a au choix:

- plus de zinc
- plus d'eau salée

{{ image(path="transistor/pile15.png", width="60%" alt="pont salin") }}

## Conclusion

Bon c'était un peu hors-sujet comme article, mais je me suis beaucoup amusé à l'écrire.

J'espère que vous avez eu plaisir à le lire également.

Nous avons toutes les bases pour attaquer l'études des transistors.

Donc la prochaine on attaquera ça. 😁