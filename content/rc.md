+++
title = "Reference Counting"
date = "2024-03-13"
draft = false
template  = 'post.html'
path = "/rust/rc"

[taxonomies]
categories = ["Rust par le métal"]
tags = ["rust", "rust_par_le_metal"]

[extra]
lang = "fr"
toc = true
math = true
mermaid = true
biscuit = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Reference Counting : Ne pas perdre ses références" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/rc.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Reference Counting : Ne pas perdre ses références" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/rc.png" },
    { property = "og:url", content="https://lafor.ge/rc" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]

+++
Bonjour à toutes et à tous 😀

Dans l'article sur le [boxing](/rust/box), on a vu que le clonage était très loin d'être gratuit.

Aujourd'hui nous allons voir une manière de cloner presque gratuitement. 😉


Utilisons le même exemple.

Déclarons une structure `Person`

```rust
struct Person {
    name: String,
    age: u8
}
```

Elle contient deux champs:
- name : une String donc une référence et une slice quelque part en Heap
- age : un entier naturel

Déclarons une instance de `Person` et affectons-la à la variable `p`.

{{image(path="rc/rc-30.jpg")}}


Nous nous retrouvons donc à avoir un espace mémoire dans la Stack qui est référencé par `p`.

Mais ce n'est pas tout car `p.name` référence aussi sa slice qui vie dans la Heap. (ici initialisé à "toto")

Nous possédons également `p.age` qui réside dans la Stack.

{{image(path="rc/rc-31.jpg")}}

Nous allons utiliser un autre outil de la bibliothèque standard qui se nomme [**Rc**](https://doc.rust-lang.org/std/rc/struct.Rc.html) pour Reference Counting.

Tout comme pour `Box`, la syntaxe est simple.

```rust
let rc = Rc::new(12);
```

`12` est désormais dans la Heap, mais il va y avoir quelques différences avec le `Box::new`. 😁

Appliquons le à `p`.

{{image(path="rc/rc-32.jpg")}}

Et voyons ce que ça produit en mémoire.

Tout d'abord on déplace `p` dans le contexte de `Rc::new`.

{{image(path="rc/rc-33.jpg")}}

On vient wrapper nos données dans un conteneur qui possède deux champs:
- data : nos données
- count: un entier

Le compteur est initialisé à `1`.

{% note() %}
En réalité le conteneur est une [`RcBox`](https://doc.rust-lang.org/src/alloc/rc.rs.html#288) et le champ `count` existe en deux version *weak* et *strong*. Mais
pour les besoins de vulgarisations je ne rentrerai pas dans les détails.
{% end %}

On vient alors allouer avec un `Box::new` de la place dans la Heap

{{image(path="rc/rc-35.jpg")}}

On déplace alors vers la Heap notre `RcBox`.

{{image(path="rc/rc-36.jpg")}}

On récupère alors une référence vers la `RcBox`

{{image(path="rc/rc-37.jpg")}}

Que l'on peut ensuite renvoyé dans le contexte `main`.

{{image(path="rc/rc-38.jpg")}}

On sort de la méthode `Rc::new` ce qui a pour action de détruire la frame.

On se retrouve alors avec `rc_p` une référence vers une `RcBox` dans la Heap.

{{image(path="rc/rc-39.jpg")}}

Bon pour le moment, ça semble juste un `Box::new` avec des étapes supplémentaires, mais promis tout ça a un sens. 😇

Pour cela, clonons `rc_p`.

{{image(path="rc/rc-40.jpg")}}

Même combat que pour `Box::clone`, on créé une référence que l'on copie dans `Rc::clone`.

{{image(path="rc/rc-41.jpg")}}

Mais cette fois-ci pas de vrai clone.

On incrémente seulement le compteur de la `RcBox`.

Il passe de `1` à `2`.

On créé une nouvelle référence qui pointe vers la même `RcBox`.

{{image(path="rc/rc-42.jpg")}}

Et on retourne la référence dans le contexte `main`.

{{image(path="rc/rc-43.jpg")}}

Nous avons maintenant dans `main`, deux références qui pointe vers la même `RcBox`.

Le compteur de la `RcBox` est de `2`.

{{image(path="rc/rc-44.jpg")}}

Maintenant on décide de déplacer la `Rc<Person>`, `rc_p_clone` dans la méthode `f1`.

Voyons ce qui se passe.

{{image(path="rc/rc-45.jpg")}}

La référence est déplacée dans le contexte de `f1`.

{{image(path="rc/rc-46.jpg")}}

`f1` termine.

{{image(path="rc/rc-48.jpg")}}

Le drop est déclenché.

Il a pour effet de décrémenter de `1` le compteur de la `RcBox`.

Mais comme le compteur est supérieur à `0`, il ne se passe rien.

{{image(path="rc/rc-49.jpg")}}

`f1` est terminé, mais la `RcBox` est toujours dans la Heap

{{image(path="rc/rc-50.jpg")}}

Appelons `f1` avec `rc_p` cette fois-ci, la dernière référence existante de la `RcBox`.

{{image(path="rc/rc-52.jpg")}}

Même combat, on déplace dans `f1`. 

{{image(path="rc/rc-53.jpg")}}

`f1` se termine

{{image(path="rc/rc-55.jpg")}}

Le drop est déclenché, on décrémente le compteur.

{{image(path="rc/rc-56.jpg")}}

Mais cette fois-ci, le compteur vaut maintenant 0.

On libère la mémoire occupée par la `RcBox` et par la `String` également.

{{image(path="rc/rc-57.jpg")}}

`f1` est terminée.

{{image(path="rc/rc-58.jpg")}}

On détruit la frame

{{image(path="rc/rc-59.jpg")}}

`main` se termine

{{image(path="rc/rc-60.jpg")}}

On détruit la frame, fin du programme.

{{image(path="rc/rc-61.jpg")}}

{% question() %}
Alors à quoi tout ça sert? 
{% end %}

En synchrone, à presque rien, en tout cas je n'ai jamais eu d'exemple vraiment parlant qui n'aurait pas pu se régler par une référence non-mutable
et une lifetime adéquate.

Par contre son grand-frère [`Arc`](https://doc.rust-lang.org/alloc/sync/struct.Arc.html) est la pierre angulaire de l'asynchronisme facile en Rust.

Il y a aussi le patterm de l'*Interior Mutability* qui utilise les Rc.

On verra ces deux aspects dans des prochains articles.

Tout ce que vous devez retenir c'est que cela permet à Rust de compter le nombre de référence active et de libérer la mémoire au moment propice.

C'est le même fonctionnement qu'un garbage collector, mais sans devoir arrêter le temps et l'espace pour compter vu que les objets se comptent eux-même.