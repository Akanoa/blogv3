FROM gitpod/workspace-full

RUN sudo mkdir -p /etc/apt/keyrings && curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/charm.gpg && echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list && sudo apt update && sudo apt install gum

RUN sudo apt update && sudo apt install gettext-base

RUN wget https://github.com/barnumbirr/zola-debian/releases/download/v0.19.2-1/zola_0.19.2-1_amd64_bookworm.deb && \
    sudo dpkg -i zola_0.19.2-1_amd64_bookworm.deb && \
    rm zola_0.19.2-1_amd64_bookworm.deb