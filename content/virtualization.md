+++
title = "Introduction à la Virtualisation (Partie 1)"
date = 2023-07-12
draft = false
path = "/virtualization-1"

[taxonomies]
categories = ["Virtualisation"]
tags = ["virtualisation"]

[extra]
lang = "fr"
math = true
toc = true
show_comment = true

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Introduction à la Virtualisation (Partie 1)" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/virtualization-1.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Introduction à la Virtualisation (Partie 1)" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/virtualization-1.png" },
    { property = "og:url", content="https://lafor.ge/virtualization-1" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]
+++

Bonjour à toutes et tous 🙂

Bienvenue dans un article encore une fois bien trop long.

Ce devait être l'introduction d'un article sur la création d'image disque dans un pipeline Gitlab CI, je vais pas 
vous le cacher, ça a un peu dérapé... 🤭

Je vous propose un tour d'horizon des concepts de la virtualisation que j'utiliserai par la suite dans l'article prévu au début.

Mais comme je sais pas m'arrêter et que j'aime que n'importe qui puisse comprendre quelque soit son niveau de départ, il y aura de l'extra 😁

Avant de commencer.

D'abord, un grand merci à [Samuel Ortez](https://twitter.com/sameo), [Julien Durillon](https://twitter.com/juuduu) et [Pierre-Antoine Grégoire](https://twitter.com/zepag) pour la [présentation](https://www.youtube.com/watch?v=As44YzdnqqE) que je vais tenter de résumer et compléter sans déformer dans les lignes qui vont suivre.

Prenez une inspiration, on descends ! 🤿

## Code Machine et Hardware

Oui, comme ça direct. ^^

Ne vous inquietez pas je vais vous guider.


### Code Machine

Premièrement ce qu'il faut savoir, c'est qu'un ordinateur est extrêment idiot, il ne parle ni français ni anglais, il ne connais qu'un langage, le 
Code Machine, basiquement des `0` et des `1`.

Impossible de lui donner à manger du C, du Java ou du JavaScript, il n'est pas fait pour ça.

Dans un ordinateur, ce qui est responsable de comprendre et d'exécuter du code se nomme un CPU (Central Processing Unit).

{{ image (path="rust/why/cpu1.png", width="60%" alt="le CPU ne prend que du code machine")}}

De même, un CPU parle un Code Machine bien spécifique et ne saura comprendre un Code Machine différent.

{{ image (path="rust/why/cpu2.png", width="60%", alt="Ce code machine doit être adapté au CPU")}}

&nbsp;

{% note(title="") %}
J'ai fait abstraction de l'interprétation et de la semi-compilation, plus de détail [dans cet article](/rust/pourquoi/#executer-du-code)
{% end %}

### CPU

Voyons un peu plus en détail les différents composants d'un ordinateur.

Le premier, nous l'avons déjà nommé est le CPU.

{{ image (path="virtualization/cpu.png", width="60%", alt="Anatomie d'un CPU")}}

Il est composé de deux parties principales (je fais abstraction de ce qui nous intéresse pas ).

- Les **Registres** : R1, R2, ... et Acc
- L' **A**rithmetic **L**ogic **U**nit ou ALU ou UAL en français (Unité Arithmétique et Logique)


Les **Registres** sont des petites mémoires qui ne permettent (en simplifiant) que de stocker une valeur.

Plus d'infos dans [cet article](https://medium.com/@nafisanaznin24cse041/microprocessor-i-general-purpose-registers-31cd43f3d6bd)

Ils sont en quelques sortes l'établi utiliser par un artisan pour poser ses planches avant la découpe.

Dans ces registres, un est un peu spécial et se nomme l'Accumulateur, ici je le représente par un **Acc**.

Si les registres sont l'établi, alors l'**ALU** sont les outils de l'artisan, son travail est de manipuler les registres pour
faire des opérations mathématique ou des transferts de données.

### RAM

Mais je l'ai dit plus haut, un registre ne contient qu'une seule valeur, ici on a 5 registres, donc on peut stocker 5 valeurs, nos programmes vont
être courts 🤭

L'idée est alors de stocker ailleurs la donnée, la place sur le CPU étant limitée, on vient stocker dans un autre composant appelé la [RAM](/physical_memory/).


{{ image (path="virtualization/cpu2.png", width="60%", alt="Connexion entre le CPU et la RAM")}}

Le CPU communique avec la RAM au moyen de deux canaux :

- bus d'adresse : défini ce que le CPU veut atteindre
- bus de données : transfert les données dans le sens CPU -> RAM ou RAM -> CPU

{% note(title="Précisions")%}

Ici je fais abstraction du cache CPU, d'ailleurs c'est en partie pour ça que les Apple M1 sont très rapides, ils ont beaucoup de cache ^^

J'assimile toute mémoire de travail à de la RAM pour plus de facilité. 

{% end %}

{{ image (path="virtualization/cpu4.png", width="80%", alt="Le programme dans la RAM")}}

On peut globalement y stocker n'importe quoi, tant que ce sont des `0` et des `1`. Et ça tombe bien notre programme est du Code Machine pouvant se représenter par du [binaire](rust/annex_bases/#base-2).

Chaque donnée est adressée par un nombre.

Dans cet exemple le CPU demande le contenu de l'adresse `@500` à la RAM.

Chouette on peut stocker plus de 5 valeurs. 🙂

### Périphériques

Mais le CPU à part faire des calculs, il ne sert pas à grand chose, il faut au moins une imprimante comme dans l'ancien temps pour lire le résultat.

{{ image (path="https://c7.alamy.com/compfr/cmrr74/1960-programmation-de-l-ordinateur-central-grand-homme-entoure-par-une-bande-de-donnees-cmrr74.jpg", raw = true, width="60%") }}

C'est pour cela que l'on a inventé les **Périphériques**. Ceux-ci peuvent être n'importe quoi: un disque dur, la carte graphique, la carte réseau, l'imprimante
qui marche jamais quand vous en avez besoin, etc ... 

{{ image (path="virtualization/cpu3.png", width="70%", alt="Périphériques")}}

## Assembleur et Instructions

C'est bien beau tout ça mais le binaire et l'esprit humain cela fait 2 (sans mauvais jeu de mots ^^'), c'est pour cela que l'on ne manipule pas directement les `0` et les `1`. 

On le remplace par une sorte de langage qui est transcriptible en `0` et `1`.

### Assembleur

Ce langage se nomme de l'**Assembleur** ou **ASM**

{% note(title="") %}
Il existe autant d'assembleur qu'il y a de de Code Machine différents et plusieurs Assembleur différents peuvent
mener au même Code Machine.

Cet assembleur est la première abstraction créé par l'Humain pour l'aider à parler le Machine sans devenir fou!
{% end %} 

Un exemple d'assembleur (rassurez-vous on détaille après 🙂).

```asm
LOD @5
```

Va être transcrit en utilisant le Code Machine de notre CPU fictif en `0101010` 

{{ image (path="virtualization/opcode1.png", width="100%", alt="Load opcode")}}

Notre ligne de programme en Assembleur peut alors se décomposer en deux parties

- **OpCode** : ce que l'on doit faire
- **Opérandes** : avec quoi ?

Un autre exemple d'instruction

```asm
STD @5
```

Qui devient

{{ image (path="virtualization/opcode2.png", width="90%", alt="Store opcode")}}

On voit ici que le OpCode change la transcription binaire, il aurait été de même si l'opérande avait également évolué. 


La langue du Code Machine est appelé un set d'instructions. 

### Instructions

Nous avons l'alphabet attaquons-nous aux mots.

La langue du CPU est décomposé en instructions, voici celle de notre CPU fictif.

| OpCode | Operande&nbsp;1 | Operande&nbsp;2 |                              Signification                   |
|:------:|:----------:|:----------:|:----------------------------------------------------------------------|
| MOV    | RX         | D          | Déplace la valeur D vers le registre RX, D peut être un autre registre |
| LOD    | RX         | @A         | Charge la valeur à l'adresse @A depuis la RAM vers le registre RX      |
| STD    | RX         | @A         | Stocke la valeur contenu dans le registre RX à l'adresse @A de la RAM  |
| ADD    | X          |            | Ajoute la valeur X à Acc, X peut être un registre                      |
| OUT    | X          | #VX        | Ecrire X sur le périphérique #VX                                       |
| JMP    | @A         |            | Sauter à l'instruction à l'adresse @A                                  |
| JNZ    | RX         | @A         | Sauter à l'instruction à l'adresse @A, si la valeur dans le registre RX n'est pas 0 |
| HLT    |            |            | Stop le CPU et met fin au programme |

Maintenant que nous avons des mots, nous allons pouvoir faire des phrases.

## Exécution d'un programme

Ces phrases vont être notre programme.

Je vous propose celui-ci.

{{ image (path="virtualization/programme1.png", width="90%", alt="Programme d'exemple")}}

Chaque ligne du programme est stockée à une adresse, ici on débute à l'adresse `@500`, biensûr c'est la représentation binaire qui est stockée
et non l'assembleur.

Nous avons donc 3 questions à résoudre :

- La ligne du programme est dans la RAM, comment la transmettre au CPU ?
- Une fois dans le CPU, comment fait-il pour comprendre la ligne ?
- Que se passe-t-il à l'exécution de la ligne ?


Pour cela, nous allons introduire 3 notions:

- Fetch
- Decode
- Exec

{{ image (path="virtualization/fetch_decode_exec.png", width="60%", alt="Fetch - Decode - Execute qui se chaînent avec des flèches")}}

Notre curseur d'exécution est à l'adresse `@500`, ce qui veut dire que la ligne d'instruction à rechercher dans la RAM est à l'adresse `@500`.

Le CPU demande donc le contenu de l'adresse  `@500` à la RAM au travers du bus d'adresses, la RAM lui renvoie ce contenu.

Cette opération est appelée le `Fetch`.

{{ image (path="virtualization/programme2.png", width="80%", alt="Opération de Fetch")}}

Une fois le contenu binaire de l'adresse `@500` en main, celui-ci est décodé.

{{ image (path="virtualization/programme3.png", width="60%", alt="Opération de Decode")}}

Ici le décodage nous annonce que l'opération à réaliser de mettre la valeur `1` dans le registre `Acc`.

```asm
MOV ACC, 1
```

Maintenant que le CPU, sait quoi faire, il va pouvoir l'exécuter.

{{ image (path="virtualization/programme4.png", width="70%", alt="Opération de Decode")}}

Dans le cas présent, mettre un `1` dans le registre `Acc`.

On passe alors à la ligne suivante en incrémentant le curseur d'exécution.


Et du coup, fetch de l'adresse `@501` cette fois-ci.

{{ image (path="virtualization/programme6.png", width="70%", alt="Opération de Fetch")}}

Décodage 

{{ image (path="virtualization/programme7.png", width="60%", alt="Opération de Decode")}}

en 

```asm
MOV R1, 4
```

Puis exécution

{{ image (path="virtualization/programme8.png", width="70%", alt="Opération d'Execute MOV")}}

Nous avons maintenant un `1` dans l'accumulateur et un `4` dans le registre `R1`.

On déroule le programme.

Celui-ci nous demande d'additionner le contenu de l'accumulateur avec le contenu du registre `R1` et de mettre
le résultat dans l'accumulateur.

```asm
ADD R1
```

{{ image (path="virtualization/programme9.png", width="70%", alt="Opération d'Execute ADD")}}

Nous avons maintenant `5` dans l'accumulateur.

On continue.

Plus compliqué, on nous demande de stocker le contenu de l'accumulateur dans la RAM à l'adresse `@15`.

```asm
STD ACC, @15
```

{{ image (path="virtualization/programme10.png", width="90%", alt="Opération d'Execute STD")}}

Le CPU va adresser la RAM pour que la valeur `5` contenue dans l'accumulateur soit stockée à l'adresse `@15`.

Ligne suivante, on fait le chemin inverse, on demande de charger le contenu de l'adresse `@15` dans le registre `R1`.

{{ image (path="virtualization/programme11.png", width="90%", alt="Opération d'Execute LOD")}}

Nous avons maintenant `5` à la place de `1` dans le registre `R1`

On déroule.

Nous devons déplacer ce qui est dans `R1` dans `R2`. Facile.

```asm
MOV R2, R1
```

{{ image (path="virtualization/programme12.png", width="90%", alt="Opération d'Execute MOV")}}

Nous avons maintenant `5` dans le registre `R2`.

Plus difficile, nous avons utilisé les registres, l'ALU, la RAM, mais pas encore les périphériques.

Nous allons demander d'écrire le contenu du registre `R2` sur le périphérique `#12`.

Ici, nous allons dire que c'est du disque dur.

Chaque périphérique est un micro ordinateur, qui possède également des registres.

```asm
OUT R2, #12 
```

Vient écrire sur le périphérique en `voie 1` dans le registre `X2`, la valeur contenue par le registre `R2`.

{{ image (path="virtualization/programme13.png", width="100%", alt="Opération d'Execute OUT")}}

`5` est maintenant écrit dans le registre, le disque viendra alors faire quelque chose avec cette valeur, mais ceci est hors scope de 
notre article.

La dernière instruction, éteint le CPU.

```asm
HLT
```

## Multi-Processus

### Race condition

Généralement, nous n'avons pas qu'un seul programme qui tourne sur un ordinateur.

Chacun de ces programmes sont stockés dans la RAM à des addresses différentes.

- Notre programme 1 débute à l'adresse `@126`
- Tandis que le programme 2 à l'adresse `@500`

{{ image (path="virtualization/programme14.png", width="80%", alt="Deux programme en RAM")}}

Comme nous n'avons qu'un seul CPU, et qu'il ne peut pas faire deux choses en même temps, il faut faire les choses de manière séquentielle.

Nous allons départager le temps que nous passons dans chaque programme.

D'ailleurs lorsqu'un programme tourne, nous allons l'appeler un **processus**.

{{ image (path="virtualization/programme15.png", width="80%", alt="Schedule")}}

Chaque passage entre processus sera appelé un `context switch`, on verra par la suite de quoi il en retourne.

Voici les deux programmes en questions.

Maintenant expert en assembleur, vous devriez les comprendre. ^^

{{ image (path="virtualization/programme16.png", width="90%", alt="Deux programmes")}}

On accélère la montre pour se placer à l'instruction 

```asm
STD ACC, @15
```

du programme 1, celle-ci écrit comme tout à l'heure la valeur `5` à l'adresse `@15`.

C'est alors qu'intervient le context switch, le processus 1 doit laisser la main au processus 2.

{{ image (path="virtualization/programme17.png", width="90%", alt="Instruction STD processus 1")}}

Nous sommes juste avant le début du programme 2, le switch de contexte ne peut pas faire grand chose, il est sensé remettre les 
valeurs du contexte d'exécution du processus 2.

Mais comme le programme n'a jamais tourné, c'est plus du bruit qui est défini ou bien les valeurs précédentes restent inchangées.

En tout cas, en l'état le processus 2, ne sait pas ce qu'il y a dans les registres.

{{ image (path="virtualization/programme18.png", width="90%", alt="Context switch entre processus 1 et processus 2")}}

Le programme 2 se déroule et atteint une instruction étrangement familière.

```asm
STD R1, @15
```

Comme un ordinateur est déterministe, une même action, provoque le même résultat.

A ceci prêt que l'opération précédente avait défini la valeur de `R1` comme étant `0`

{{ image (path="virtualization/programme19.png", width="90%", alt="Instruction STD processus 2")}}

Donc maintenant l'adresse `@15` vaut `0` !!!

Nous venons de créer une `race condition` : deux acteurs qui utilisent la même ressources sans se synchroniser sur l'écriture.

Temps écoulé pour le processus 2, il passe la main.

Le context switch restaure les états des registres pour que le processus 1 puisse de nouveau travailler.

On remet :

- 4 dans le registre R1
- 5 dans l'accumulateur

On restaure également le pointeur d'exécution.

{{ image (path="virtualization/programme20.png", width="90%", alt="Context switch entre processus 2 et processus 1")}}

On incrémente le curseur d'exécution.

```asm
LOD R1, @15
```

{{ image (path="virtualization/programme21.png", width="90%", alt="LOD processus 1")}}

Ah bah oui, mais non ... 

Le `5` n'est plus là. Il a été précédemment remplacé.

Le programme 1, n'est désormais plus déterministe, il est capable de faire n'importe quoi.

Echec et mat ! 

Sommes-nous condamner à n'exécuter qu'un seul programme par CPU.

Non, biensûr que non.

Mais pour ça, nous allons avoir besoin de plus de choses.

### Kernel et MMU

Il va nous falloir un chef de gare pour gérer un peu la discipline dans les rangs.

Je vous le présente, il s'agit du **Kernel**.

{{ image (path="virtualization/programme22.png", width="70%", alt="Kernel")}}

Son rôle va être de gérer les processus et de les empêcher de faire n'importe quoi.

{{ image (path="virtualization/programme23.png", width="70%", alt="Kernel et processus")}}

Et par n'importe quoi, je veux dire écrire n'importe où dans la mémoire.

Le but étant d'éviter la race condition, mais plus généralement que chaque processus reste chez lui et ne puisse
pas aller taper de la mémoire qui ne lui appartient pas ou qui est déjà utilisée.

{% note(title="") %}
Il a encore un milliard de rôles différents supplémentaires, mais on reste simple, c'est de la vulgarisation, pas un cours.
{% end %}

Pour l'aider dans sa tâche de cloisonnement, le Kernel peut faire appel à un outil.

Celui-ci se nomme la **M**emory **M**anagement **U**nit.

{{ image (path="virtualization/programme24.png", width="70%", alt="MMU")}}

Il s'agit globalement d'une liste d'adresse virtuelles qui pointent sur des adresses physique.

Pourquoi virtuelle ? Parce qu'elles n'ont aucune existence physique, elle n'existent pas dans la RAM, au contraire de l'adresse physique, qui est et bien
physiquement présente dans le silicium.

Pour permettre de s'y retrouver et aussi d'avoir plusieurs fois la même adresse virtuelle pour deux adresses physiques différentes, on découpe la MMU en tables.

Ici par exemple on a deux tables, dans ces deux tables l'adresses virtuelle `15` existe, mais ne pointe pas vers la même adresse physique.

{{ image (path="virtualization/programme25.png", width="70%", alt="Table du MMU")}}

Dans la table 1

$$15 \Rightarrow 1005$$

Dans la table 2

$$15 \Rightarrow 22$$

{% warning() %}
La MMU est un composant physique à la différence du Kernel qui est du logicielle.
{% end %}

### La MMU en action

Nous allons utiliser cette propriété pour résoudre notre problème de tout à l'heure.

Revenons dans le temps.

Nous sommes dans le processus 1, juste avant le stockage de la valeur en RAM.

Nous allons interposer la MMU sur le chemin du bus d'adressage.

Le CPU fait ce qu'il faisait avant, demander de stocker à l'adresse `@15`, mais la MMU est configurée par le Kernel pour utiliser 
la table `p1`.

Ainsi, l'adresse subit la modification suivante:

$$15 \Rightarrow 1013$$

{{ image (path="virtualization/programme26.png", width="100%", alt="Translation d'adresse")}}

Notre `5` est alors stocké à l'adresse physique `@1013` et non `@15`.

Cette fois-ci, c'est au processus 2 de vouloir stocker son `0` à l'adresse `@15`.

Mais même principe, le Kernel défini la table de la MMU sur `p2`.

Ainsi, l'adresse réellement utilisée devient

$$15 \Rightarrow 2862$$

{{ image (path="virtualization/programme27.png", width="100%", alt="Translation d'adresse")}}

Son `0` est alors stocké à l'adresse physique `@2862` et non `@15`.

Lorsque le processus 1 revient à la vie.

Il va lors du chargement de l'adresse `@15`, là aussi passer par le MMU.

Ainsi ce n'est pas `@15` mais à nouveau `@1013` qui est utilisé pour adresser la RAM.

{{ image (path="virtualization/programme27_2.png", width="100%", alt="Translation d'adresse")}}

Et donc c'est bien un `5` et non un `0` qui est désormais stocké dans le registre `R1`.

Le programme peut de nouveau continuer. 🌞

{% note(title="") %}

- La MMU est un composant physique.

- La MMU est une partie essentielle de la sécurité d'un ordinateur, sans lui un programme malveillant ou mal conçu pourrait réécrire la 
mémoire d'un autre programme ou même le programme lui-même.

- Du fait que l'isolation soit matérielle, cela rend très difficile le contournement de la sécurité.
{% end %}

### Syscall

De la même manière que nous ne voulons pas qu'un processus prenne la main sur la mémoire de ses petits camarades, nous allons isoler nos processus de l'accès direct aux ressources physiques.

Pour 2 raisons:
- Un programme ne sait pas forcément parler "Disque dur"
- On veut pas qu'il puisse effacer ce qu'il veut le Kernel compris (oui les programmes sont sur disque avant d'arriver dans la RAM ^^)

{{ image (path="virtualization/programme28.png", width="100%", alt="Syscall")}}

Lorsque qu'un processus désire réaliser une manipulation qui nécessite autre chose que de la RAM, ce processus va réaliser une doléance 
au Kernel, cette demande est appelée un **Syscall**.

Le syscall que nous allons utiliser se nomme [read](https://man7.org/linux/man-pages/man2/read.2.html). Son boulot est demander la lecture d'une ressource
un fichier par exemple qui serait sur le disque dur.

Mais de la même manière que le processus ne savait pas parler "Disque Dur", le Kernel ne sait pas non plus.

Heureusement, le **Driver** lui parle à la fois Kernel et Disque Dur, se sera donc notre interprète dans la transaction.

{% note(title="") %}
Pour les besoins du dessin je simplifie tout, mais il y a bien plus de chose à dire ^^
{% end %}

Détaillons ce qu'il se passe lors de la lecture de ce fichier.

{{ image (path="virtualization/programme29.png", width="100%", alt="Syscall")}}

Le processus vient définir qu'il a besoin en mettant l'information dans un registre (ici pour l'exemple R2 ), le nombre est ce qu'on appelle un *file descriptor*, c'est un identifiant qui est connu du Kernel et qu'il associe à une ressource, ici un fichier déjà ouvert par un précédent syscall [open](https://man7.org/linux/man-pages/man2/open.2.html).

{{ image (path="virtualization/programme30.png", width="100%", alt="Syscall")}}

Un **JMP** est une instruction appelée un Jump, celui-ci vient déplacer le curseur d'exécution à l'adresse définie ici `@809` qui correspond
au début de notre syscall imaginaire et simplifié *read_fd*.

Le syscall fait des trucs, dont stocker de la donnée à l'adresse `@10005`.

C'est alors que des mécanismes un peu complexes et que je n'ai pas totalement saisis vont aller réveiller le Kernel qui piquait une sieste en attendant qu'il
se passe quelque chose d'intéressant.

{{ image (path="virtualization/programme31.png", width="100%", alt="Syscall")}}

Cette magie noire se nomme une **interruption**.

Les instructions qui vont suivre sont appelées **privilégiées** car elle ne peuvent être exécuté que dans un mode spécifique du CPU, mode qui ne peut être
atteint que par le Kernel et ses modules dont les drivers.

Il va alors faire ce que le syscall lui demande, lire le fichier, mais comme il ne sait toujours pas parler Disque dur, il fait appel
au **Driver** qui lui sait.

{{ image (path="virtualization/programme32.png", width="100%", alt="Syscall")}}


Finalement le Disque dur est lu, au travers de l'opération 

```asm
OUT R2, #12
```

Le trajet de retour vers le processus est une autre histoire, peut-être un jour dans un autre article ^^"

Petit récapitulatif de ce qui s'est déroulé.

{{ image (path="virtualization/programme33.png", width="100%", alt="Syscall")}}

## Conclusion

Ainsi s'achève la première partie de notre épopée dans la Virtualisation, vous avez l'intuition de ce qui se passe dans un ordinateur physique.

Dans la seconde partie on commencera à esquisser le fonctionnement de la virtualisation elle même.

Merci de votre lecture ♥️
