+++
title = "Partie 10 : Clefs primaires"
date = 2024-12-18
draft = false
template  = 'post.html'

[taxonomies]
categories = ["Réimplémenter sqlite en Rust"]
tags = ["rust", "sqlite", "system"]

[extra]
lang = "fr"
toc = true
math = true
mermaid = false
biscuit = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Réimplémenter sqlite en Rust : Partie 10" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/rustqlite-10.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Réimplémenter sqlite en Rust : Partie 10" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/rustqlite-10.png" },
    { property = "og:url", content="https://lafor.ge/rustqlite-10" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]

+++


{% detail(title="Les articles de la série") %}
{{ summary(path="content/toc/rustqlite.toml") }}
{% end %}

Bonjour à toutes et tous 😃

Pour le moment notre seule façon de récupérer de la donnée depuis notre table consiste à réaliser un full-scan, ce qui est veut dire partir du premier byte de la table et
désésérialiser jusqu'à la fin.

Autant dire que ce n'est pas vraiment optimale si on cherche un tuple en milieu de table. 😅

Tout comme à l'époque du téléphone à fil, il y avait un gros bouquin qui référençait le nom de la personne par rapport à son numéro. Nous allons faire de même avec nos
tuples.

La question maintenant est de savoir dans notre cas qui est le "nom" et qu'est ce que le "numéro" de notre "annuaire" à nous.

Il va nous falloir plus d'outils pour répondre à ces questions.

## Modifiation du parser

La première chose que l'on va rajouter c'est des nouvelles capacité à notre parser.

### Nouvelle grammaire

Pour pouvoir définir qui sera le "nom" de notre annuaire, nous allons définir un flag sur un des champs du schéma de la table à la création de celle-ci.

Ce flag va se nommer `PRIMARY KEY`, un champ disposant de cette contrainte se nomme la "clef primaire" de la table, autrement dit la manière la plus simple de 
retrouver de la données dans une table.

Voici la nouvelle grammaire du parser pour la commande `CREATE TABLE`.

{%detail(title="CREATE TABLE avec PRIMARY KEY")%}

```bnf
CreateTable ::= 'CREATE' 'TABLE' LiteralString OpenParen ColumnDef* CloseParen Semicolon
ColumnDef ::= LiteralString ColumnType | LiteralString ColumnType Comma
ColumnType ::= 'INTEGER' Constraint* | ColumTypeText Constraint*
ColumTypeText ::= 'TEXT' OpenParen LiteralInteger CloseParen
LiteralString ::=  [A-Za-z0-9_]*
LiteralInteger ::= [0-9]+
Constraint ::= 'PRIMARY' 'KEY' | 'NOT' 'NULL'
OpenParen ::= '('
CloseParen ::= ')'
Comma ::= ','
Semicolon ::= ';'
```


<p style="font-size: 14px; font-weight:bold"><a name="CreateTable">CreateTable:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22613%22%20height%3D%2271%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2237%22%20width%3D%2272%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2235%22%20width%3D%2272%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2255%22%3ECREATE%3C%2Ftext%3E%3Crect%20x%3D%22123%22%20y%3D%2237%22%20width%3D%2262%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22121%22%20y%3D%2235%22%20width%3D%2262%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22131%22%20y%3D%2255%22%3ETABLE%3C%2Ftext%3E%3Crect%20x%3D%22205%22%20y%3D%2237%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22203%22%20y%3D%2235%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22213%22%20y%3D%2255%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22321%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22319%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22329%22%20y%3D%2255%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22387%22%20y%3D%223%22%20width%3D%2288%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22385%22%20y%3D%221%22%20width%3D%2288%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22395%22%20y%3D%2221%22%3EColumnDef%3C%2Ftext%3E%3Crect%20x%3D%22515%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22513%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22523%22%20y%3D%2255%22%3E%29%3C%2Ftext%3E%3Crect%20x%3D%22561%22%20y%3D%2237%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22559%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22569%22%20y%3D%2255%22%3E%3B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m0%200%20h10%20m72%200%20h10%20m0%200%20h10%20m62%200%20h10%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m26%200%20h10%20m20%200%20h10%20m0%200%20h98%20m-128%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m108%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-108%200%20h10%20m88%200%20h10%20m20%2034%20h10%20m26%200%20h10%20m0%200%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22603%2051%20611%2047%20611%2055%22%2F%3E%3Cpolygon%20points%3D%22603%2051%20595%2047%20595%2055%22%2F%3E%3C%2Fsvg%3E" height="71" width="613" usemap="#CreateTable.map"><map name="CreateTable.map"><area shape="rect" coords="203,35,299,67" href="#LiteralString" title="LiteralString"><area shape="rect" coords="385,1,473,33" href="#ColumnDef" title="ColumnDef"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="ColumnDef">ColumnDef:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22357%22%20height%3D%2269%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%223%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%221%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2239%22%20y%3D%2221%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22147%22%20y%3D%223%22%20width%3D%2298%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22145%22%20y%3D%221%22%20width%3D%2298%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22155%22%20y%3D%2221%22%3EColumnType%3C%2Ftext%3E%3Crect%20x%3D%22285%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22283%22%20y%3D%2233%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22293%22%20y%3D%2253%22%3E%2C%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m98%200%20h10%20m20%200%20h10%20m0%200%20h34%20m-64%200%20h20%20m44%200%20h20%20m-84%200%20q10%200%2010%2010%20m64%200%20q0%20-10%2010%20-10%20m-74%2010%20v12%20m64%200%20v-12%20m-64%2012%20q0%2010%2010%2010%20m44%200%20q10%200%2010%20-10%20m-54%2010%20h10%20m24%200%20h10%20m23%20-32%20h-3%22%2F%3E%3Cpolygon%20points%3D%22347%2017%20355%2013%20355%2021%22%2F%3E%3Cpolygon%20points%3D%22347%2017%20339%2013%20339%2021%22%2F%3E%3C%2Fsvg%3E" height="69" width="357" usemap="#ColumnDef.map"><map name="ColumnDef.map"><area shape="rect" coords="29,1,125,33" href="#LiteralString" title="LiteralString"><area shape="rect" coords="145,1,243,33" href="#ColumnType" title="ColumnType"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="ColumnType">ColumnType:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22361%22%20height%3D%22115%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%2237%22%20width%3D%2280%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2235%22%20width%3D%2280%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2255%22%3EINTEGER%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2281%22%20width%3D%22116%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2279%22%20width%3D%22116%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2299%22%3EColumTypeText%3C%2Ftext%3E%3Crect%20x%3D%22227%22%20y%3D%223%22%20width%3D%2286%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22225%22%20y%3D%221%22%20width%3D%2286%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22235%22%20y%3D%2221%22%3EConstraint%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m20%200%20h10%20m80%200%20h10%20m0%200%20h36%20m-156%200%20h20%20m136%200%20h20%20m-176%200%20q10%200%2010%2010%20m156%200%20q0%20-10%2010%20-10%20m-166%2010%20v24%20m156%200%20v-24%20m-156%2024%20q0%2010%2010%2010%20m136%200%20q10%200%2010%20-10%20m-146%2010%20h10%20m116%200%20h10%20m40%20-44%20h10%20m0%200%20h96%20m-126%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m106%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-106%200%20h10%20m86%200%20h10%20m23%2034%20h-3%22%2F%3E%3Cpolygon%20points%3D%22351%2051%20359%2047%20359%2055%22%2F%3E%3Cpolygon%20points%3D%22351%2051%20343%2047%20343%2055%22%2F%3E%3C%2Fsvg%3E" height="115" width="361" usemap="#ColumnType.map"><map name="ColumnType.map"><area shape="rect" coords="49,79,165,111" href="#ColumTypeText" title="ColumTypeText"><area shape="rect" coords="225,1,311,33" href="#Constraint" title="Constraint"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="ColumTypeText">ColumTypeText:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22331%22%20height%3D%2237%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%223%22%20width%3D%2254%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%221%22%20width%3D%2254%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2221%22%3ETEXT%3C%2Ftext%3E%3Crect%20x%3D%22105%22%20y%3D%223%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22103%22%20y%3D%221%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22113%22%20y%3D%2221%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22151%22%20y%3D%223%22%20width%3D%22106%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22149%22%20y%3D%221%22%20width%3D%22106%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22159%22%20y%3D%2221%22%3ELiteralInteger%3C%2Ftext%3E%3Crect%20x%3D%22277%22%20y%3D%223%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22275%22%20y%3D%221%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22285%22%20y%3D%2221%22%3E%29%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m0%200%20h10%20m54%200%20h10%20m0%200%20h10%20m26%200%20h10%20m0%200%20h10%20m106%200%20h10%20m0%200%20h10%20m26%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22321%2017%20329%2013%20329%2021%22%2F%3E%3Cpolygon%20points%3D%22321%2017%20313%2013%20313%2021%22%2F%3E%3C%2Fsvg%3E" height="37" width="331" usemap="#ColumTypeText.map"><map name="ColumTypeText.map"><area shape="rect" coords="149,1,255,33" href="#LiteralInteger" title="LiteralInteger"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%22189%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Cpolygon%20points%3D%2251%20151%2058%20135%20106%20135%20113%20151%20106%20167%2058%20167%22%2F%3E%3Cpolygon%20points%3D%2249%20149%2056%20133%20104%20133%20111%20149%20104%20165%2056%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%20107%2058%2091%20104%2091%20111%20107%20104%20123%2058%20123%22%2F%3E%3Cpolygon%20points%3D%2249%20105%2056%2089%20102%2089%20109%20105%20102%20121%2056%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%2063%2058%2047%20106%2047%20113%2063%20106%2079%2058%2079%22%2F%3E%3Cpolygon%20points%3D%2249%2061%2056%2045%20104%2045%20111%2061%20104%2077%2056%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m23%20166%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20159%20179%20159%20187%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20143%20179%20143%20187%22%2F%3E%3C%2Fsvg%3E" height="189" width="161"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="Constraint">Constraint:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22249%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2284%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2284%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3EPRIMARY%3C%2Ftext%3E%3Crect%20x%3D%22155%22%20y%3D%223%22%20width%3D%2246%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22153%22%20y%3D%221%22%20width%3D%2246%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22163%22%20y%3D%2221%22%3EKEY%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2248%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2248%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2265%22%3ENOT%3C%2Ftext%3E%3Crect%20x%3D%22119%22%20y%3D%2247%22%20width%3D%2256%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22117%22%20y%3D%2245%22%20width%3D%2256%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22127%22%20y%3D%2265%22%3ENULL%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m84%200%20h10%20m0%200%20h10%20m46%200%20h10%20m-190%200%20h20%20m170%200%20h20%20m-210%200%20q10%200%2010%2010%20m190%200%20q0%20-10%2010%20-10%20m-200%2010%20v24%20m190%200%20v-24%20m-190%2024%20q0%2010%2010%2010%20m170%200%20q10%200%2010%20-10%20m-180%2010%20h10%20m48%200%20h10%20m0%200%20h10%20m56%200%20h10%20m0%200%20h26%20m23%20-44%20h-3%22%2F%3E%3Cpolygon%20points%3D%22239%2017%20247%2013%20247%2021%22%2F%3E%3Cpolygon%20points%3D%22239%2017%20231%2013%20231%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="249"><br>


{%end%}

Les colonnes disposent désormais d'un nouveau composant `Constraint` qui va être soit l'enchaînement des tokens `PRIMARY KEY` soit `NOT NULL`.

Il est possible de d'avoir à la fois `PRIMARY KEY` et `NOT NULL` sur une même colonne.

La deuxième modification va être sur la commande `SELECT`.

Pour le moment, on réalise uniquement ce que l'on nomme des "full-scan", c'est à dire que l'on part de l'index 0 de nos tuples, démarrant à la page 0 de notre table, et on
itère jusqu'à ne plus avoir d'entrée à retourner.

Cela est du à la pauvreté de notre commande `SELECT` qui ne permet pas de nommer ce que l'on désire. Il nous manque littéralement des "mots" pour nous exprimer! 😅

Nous allons les rajouter avec cette nouvelle grammaire:

{%detail(title="SELECT FROM avec clause WHERE")%}

```bnf
Select ::= 'SELECT' Column* 'FROM' LiteralString WhereClause Semicolon
Column ::= '*' | LiteralString | LiteralString Comma
WhereCaluse ::= 'WHERE' Identifier '=' LiteralValue
LiteralString ::=  "'" [A-Za-z0-9_]* "'"
Identifier ::=  [A-Za-z0-9_]*
LiteralInteger ::= [0-9]+
LiteralValue ::= LiteralInteger | LiteralString
OpenParen ::= '('
CloseParen ::= ')'
Comma ::= ','
Semicolon ::= ';'
```

<p style="font-size: 14px; font-weight:bold"><a name="Select">Select:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22621%22%20height%3D%2271%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2237%22%20width%3D%2270%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2235%22%20width%3D%2270%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2255%22%3ESELECT%3C%2Ftext%3E%3Crect%20x%3D%22141%22%20y%3D%223%22%20width%3D%2268%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22139%22%20y%3D%221%22%20width%3D%2268%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22149%22%20y%3D%2221%22%3EColumn%3C%2Ftext%3E%3Crect%20x%3D%22249%22%20y%3D%2237%22%20width%3D%2260%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22247%22%20y%3D%2235%22%20width%3D%2260%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22257%22%20y%3D%2255%22%3EFROM%3C%2Ftext%3E%3Crect%20x%3D%22329%22%20y%3D%2237%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22327%22%20y%3D%2235%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22337%22%20y%3D%2255%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22445%22%20y%3D%2237%22%20width%3D%22104%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22443%22%20y%3D%2235%22%20width%3D%22104%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22453%22%20y%3D%2255%22%3EWhereClause%3C%2Ftext%3E%3Crect%20x%3D%22569%22%20y%3D%2237%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22567%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22577%22%20y%3D%2255%22%3E%3B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m0%200%20h10%20m70%200%20h10%20m20%200%20h10%20m0%200%20h78%20m-108%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m88%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-88%200%20h10%20m68%200%20h10%20m20%2034%20h10%20m60%200%20h10%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m104%200%20h10%20m0%200%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22611%2051%20619%2047%20619%2055%22%2F%3E%3Cpolygon%20points%3D%22611%2051%20603%2047%20603%2055%22%2F%3E%3C%2Fsvg%3E" height="71" width="621" usemap="#Select.map"><map name="Select.map"><area shape="rect" coords="139,1,207,33" href="#Column" title="Column"><area shape="rect" coords="327,35,423,67" href="#LiteralString" title="LiteralString"><area shape="rect" coords="443,35,547,67" href="#WhereClause" title="WhereClause"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="Column">Column:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22279%22%20height%3D%22113%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E%2A%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22187%22%20y%3D%2279%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22185%22%20y%3D%2277%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22195%22%20y%3D%2297%22%3E%2C%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m28%200%20h10%20m0%200%20h152%20m-220%200%20h20%20m200%200%20h20%20m-240%200%20q10%200%2010%2010%20m220%200%20q0%20-10%2010%20-10%20m-230%2010%20v24%20m220%200%20v-24%20m-220%2024%20q0%2010%2010%2010%20m200%200%20q10%200%2010%20-10%20m-210%2010%20h10%20m96%200%20h10%20m20%200%20h10%20m0%200%20h34%20m-64%200%20h20%20m44%200%20h20%20m-84%200%20q10%200%2010%2010%20m64%200%20q0%20-10%2010%20-10%20m-74%2010%20v12%20m64%200%20v-12%20m-64%2012%20q0%2010%2010%2010%20m44%200%20q10%200%2010%20-10%20m-54%2010%20h10%20m24%200%20h10%20m43%20-76%20h-3%22%2F%3E%3Cpolygon%20points%3D%22269%2017%20277%2013%20277%2021%22%2F%3E%3Cpolygon%20points%3D%22269%2017%20261%2013%20261%2021%22%2F%3E%3C%2Fsvg%3E" height="113" width="279" usemap="#Column.map"><map name="Column.map"><area shape="rect" coords="49,45,145,77" href="#LiteralString" title="LiteralString"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="WhereCaluse">WhereCaluse:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22391%22%20height%3D%2237%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%223%22%20width%3D%2270%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%221%22%20width%3D%2270%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2221%22%3EWHERE%3C%2Ftext%3E%3Crect%20x%3D%22121%22%20y%3D%223%22%20width%3D%2278%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22119%22%20y%3D%221%22%20width%3D%2278%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22129%22%20y%3D%2221%22%3EIdentifier%3C%2Ftext%3E%3Crect%20x%3D%22219%22%20y%3D%223%22%20width%3D%2230%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22217%22%20y%3D%221%22%20width%3D%2230%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22227%22%20y%3D%2221%22%3E%3D%3C%2Ftext%3E%3Crect%20x%3D%22269%22%20y%3D%223%22%20width%3D%2294%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22267%22%20y%3D%221%22%20width%3D%2294%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22277%22%20y%3D%2221%22%3ELiteralValue%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m0%200%20h10%20m70%200%20h10%20m0%200%20h10%20m78%200%20h10%20m0%200%20h10%20m30%200%20h10%20m0%200%20h10%20m94%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22381%2017%20389%2013%20389%2021%22%2F%3E%3Cpolygon%20points%3D%22381%2017%20373%2013%20373%2021%22%2F%3E%3C%2Fsvg%3E" height="37" width="391" usemap="#WhereCaluse.map"><map name="WhereCaluse.map"><area shape="rect" coords="119,1,197,33" href="#Identifier" title="Identifier"><area shape="rect" coords="267,1,361,33" href="#LiteralValue" title="LiteralValue"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22249%22%20height%3D%22203%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%22169%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%22167%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%22187%22%3E%27%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%20151%20102%20135%20150%20135%20157%20151%20150%20167%20102%20167%22%2F%3E%3Cpolygon%20points%3D%2293%20149%20100%20133%20148%20133%20155%20149%20148%20165%20100%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%20107%20102%2091%20148%2091%20155%20107%20148%20123%20102%20123%22%2F%3E%3Cpolygon%20points%3D%2293%20105%20100%2089%20146%2089%20153%20105%20146%20121%20100%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%2063%20102%2047%20150%2047%20157%2063%20150%2079%20102%2079%22%2F%3E%3Cpolygon%20points%3D%2293%2061%20100%2045%20148%2045%20155%2061%20148%2077%20100%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2295%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2293%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22103%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Crect%20x%3D%22197%22%20y%3D%22169%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22195%22%20y%3D%22167%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22205%22%20y%3D%22187%22%3E%27%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m0%200%20h10%20m24%200%20h10%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m20%20166%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22239%20183%20247%20179%20247%20187%22%2F%3E%3Cpolygon%20points%3D%22239%20183%20231%20179%20231%20187%22%2F%3E%3C%2Fsvg%3E" height="203" width="249"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="Identifier">Identifier:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%22189%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Cpolygon%20points%3D%2251%20151%2058%20135%20106%20135%20113%20151%20106%20167%2058%20167%22%2F%3E%3Cpolygon%20points%3D%2249%20149%2056%20133%20104%20133%20111%20149%20104%20165%2056%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%20107%2058%2091%20104%2091%20111%20107%20104%20123%2058%20123%22%2F%3E%3Cpolygon%20points%3D%2249%20105%2056%2089%20102%2089%20109%20105%20102%20121%2056%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%2063%2058%2047%20106%2047%20113%2063%20106%2079%2058%2079%22%2F%3E%3Cpolygon%20points%3D%2249%2061%2056%2045%20104%2045%20111%2061%20104%2077%2056%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m23%20166%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20159%20179%20159%20187%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20143%20179%20143%20187%22%2F%3E%3C%2Fsvg%3E" height="189" width="161"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LiteralValue">LiteralValue:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22205%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%22106%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%22106%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2221%22%3ELiteralInteger%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3ELiteralString%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m106%200%20h10%20m-146%200%20h20%20m126%200%20h20%20m-166%200%20q10%200%2010%2010%20m146%200%20q0%20-10%2010%20-10%20m-156%2010%20v24%20m146%200%20v-24%20m-146%2024%20q0%2010%2010%2010%20m126%200%20q10%200%2010%20-10%20m-136%2010%20h10%20m96%200%20h10%20m0%200%20h10%20m23%20-44%20h-3%22%2F%3E%3Cpolygon%20points%3D%22195%2017%20203%2013%20203%2021%22%2F%3E%3Cpolygon%20points%3D%22195%2017%20187%2013%20187%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="205" usemap="#LiteralValue.map"><map name="LiteralValue.map"><area shape="rect" coords="49,1,155,33" href="#LiteralInteger" title="LiteralInteger"><area shape="rect" coords="49,45,145,77" href="#LiteralString" title="LiteralString"></map><br>

{%end%}

Elle introduit le nouveau mot-clef `WHERE` qui permet d'écrire une expression du type `champ = value`, ce qui signifie : "renvoie moi le champ qui possède cette valeur".

### Ajouts des tokens

Nous allons rajouter 6 tokens supplémentaires:
- PRIMARY
- KEY
- NOT
- NULL
- WHERE
- =

```rust
enum Token {
    // ...
    /// PRIMARY token
    Primary,
    /// KEY token
    Key,
    /// NOT token
    Not,
    /// NULL token
    Null,
    /// = token
    Equal,
    /// WHERE token
    Where,
    /// Technical token never matched
    Technic
    // ...
}
```

J'en profite pour définir un token technique qui permet d'utiliser l'API Token sans devoir définir quelque chose de précis à reconnaître.

On rajoute les matchers qui vont bien:

```rust
impl Match for Token {
    fn matcher(&self) -> Matcher {
        match self {
            // ...
            Token::Primary => Box::new(matchers::match_pattern("primary")),
            Token::Key => Box::new(matchers::match_pattern("key")),
            Token::Not => Box::new(matchers::match_pattern("not")),
            Token::Null => Box::new(matchers::match_pattern("null")),
            Token::Equal => Box::new(matchers::match_char('=')),
            Token::Where => Box::new(matchers::match_pattern("where")),
            Token::Technic => Box::new(matchers::match_predicate(|_| (false, 0))),
            // ...
        }
    }
}
```

En n'oubliant pas de rajouter les implémentation de `Size` pour ces nouveaux tokens

```rust
impl Size for Token {
    fn size(&self) -> usize {
        match self {
            // ...
            Token::Primary => 7,
            Token::Key => 3,
            Token::Not => 3,
            Token::Null => 4,
            Token::Where => 5,
            // ...
        }
    }
}
```

### Forecaster UntilEnd

Son travail est simple, reconnaître la fin de la slice.

```rust
pub struct UntilEnd;

impl<'a> Forecast<'a, u8, Token> for UntilEnd {
    fn forecast(&self, data: &mut Scanner<'a, u8>) -> crate::parser::Result<ForecastResult<Token>> {
        Ok(ForecastResult::Found {
            end_slice: data.remaining().len(),
            start: Token::Technic,
            end: Token::Technic,
        })
    }
}
```

On consomme jusqu'à la fin, son existence peut sembler absurde, mais il va permettre de construire des API d'alternatives de reconnaissances de pattern bien plus
naturellement.

### Définition des contraintes

Pour parser nos contraintes `PRIMARY KEY` et `NOT NUL`, il faut pouvoir les reconnaître.

Pour se faire on rajoute deux visiteurs: 

`PrimaryKeyConstraint` qui reconnaît l'enchaînement de tokens `PRIMARY` suivi d'un nombre supérieur à 1 d'espace puis le token `KEY`.

```rust
struct PrimaryKeyConstraint;

impl<'a> Visitable<'a, u8> for PrimaryKeyConstraint {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        recognize(Token::Primary, scanner)?;
        scanner.visit::<Whitespaces>()?;
        recognize(Token::Key, scanner)?;
        Ok(PrimaryKeyConstraint)
    }
}
```

On fait de même avec le `NotNullConstraint`

```rust
struct NotNullConstraint;

impl<'a> Visitable<'a, u8> for NotNullConstraint {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        scanner.visit::<OptionalWhitespaces>()?;
        recognize(Token::Not, scanner)?;
        scanner.visit::<Whitespaces>()?;
        recognize(Token::Null, scanner)?;
        Ok(NotNullConstraint)
    }
}
```

On fusionne les visiteurs dans une énumération

```rust
enum ConstraintResult {
    PrimaryKey(PrimaryKeyConstraint),
    NotNull(NotNullConstraint),
}
```

Et son pendant `Constraint` associé à sa glu technique.

```rust
enum Constraint {
    PrimaryKey,
    NotNull,
}

impl From<ConstraintResult> for Constraint {
    fn from(value: ConstraintResult) -> Self {
        match value {
            ConstraintResult::PrimaryKey(_constraint) => Constraint::PrimaryKey,
            ConstraintResult::NotNull(_constraint) => Constraint::NotNull,
        }
    }
}
```

On peut alors en faire un Visteur.

```rust
impl<'a> Visitable<'a, u8> for Constraint {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        Acceptor::new(scanner)
            .try_or(|constraint| Ok(ConstraintResult::PrimaryKey(constraint)))?
            .try_or(|constraint| Ok(ConstraintResult::NotNull(constraint)))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)
            .map(Into::into)
    }
}
```

### Ajout des contraintes sur la définition de la colonne

On rajoute la possibilité d'avoir des contraintes sur une colonne.

```rust
struct ColumnDefinition {
    name: String,
    field: ColumnType,
    // ici 👇
    constraints: Vec<Constraint>,
}
```

Un groupe de contraintes est séparé par un espace blanc.

Lorsque l'on défini des containtes, on peut se retrouver dans deux cas:
- soit c'est un groupe de containtes sur un champ du schéma qui n'est pas le dernier champ du schéma : ce champ se termine par une virgule `,`
- soit le groupe de contraintes est sur le dernier champ du schéma : ce champ se termine par rien du tout 


```rust
impl<'a> Visitable<'a, u8> for ColumnDefinition {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {

        // On reconnaît un groupe qui se termine
        let maybe_constraints = Forcaster::new(scanner)
            // soit par une virgule
            .try_or(UntilToken(Token::Comma))?
            // soit c'est la fin de la slice
            .try_or(UntilEnd)?
            .finish();

        let mut constraints = Vec::new();
        // si un groupe est trouvé
        if let Some(Forecasting { data, .. }) = maybe_constraints {
            let mut constrait_tokenizer = Tokenizer::new(data);
            // si le groupe contient au moins un byte
            if !constrait_tokenizer.remaining().is_empty() {
                // on en fait une liste de contraintes séparées par au moins un blanc
                let constraints_group =
                    constrait_tokenizer.visit::<SeparatedList<Constraint, Whitespaces>>()?;
                constraints = constraints_group.into_iter().collect();
                // on avance le scanner
                scanner.bump_by(data.len());
            }
        }

        // on retourne la colonne avec ses contraintes
        Ok(ColumnDefinition {
            name,
            field,
            constraints,
        })

    }
}
```

### Modification du visiteur Schema

```rust
impl<'a> Schema {
    fn parse(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on visite la liste de colonnes
        let columns_definitions =
            fields_group_tokenizer.visit::<SeparatedList<ColumnDefinition, SeparatorComma>>()?;

        // que l'on transforme en des définitions de colonnes
        let fields =
            columns_definitions
                .into_iter()
                .fold(Vec::new(), |mut fields, column_definition| {
                    fields.push((
                        column_definition.name,
                        crate::data::ColumnDefinition::new(
                            column_definition.field,
                            // 👇 avec les contraintes associées 
                            column_definition.constraints,
                        ),
                    ));
                    fields
                });
    }
}
```

Ce qui donne:

```rust
#[test]
fn test_constraints() {
    let data = b"(id INTEGER NOT NULL PRIMARY KEY, name_1 TEXT(50) NOT NULL)";
    let mut tokenizer = Tokenizer::new(data);
    let schema = tokenizer.visit::<Schema>().expect("failed to parse schema");
    assert_eq!(
        schema,
        Schema::new(
            vec![
                (
                    "id".to_string(),
                    ColumnDefinition::new(
                        ColumnType::Integer,
                        vec![Constraint::NotNull, Constraint::PrimaryKey,]
                    )
                ),
                (
                    "name_1".to_string(),
                    ColumnDefinition::new(ColumnType::Text(50), vec![Constraint::NotNull])
                )
            ],
        )
    )
}
```

Nous sommes désormais capables de parser les contraintes de la tables ! 🤩

### Clef primaire du schéma

Une des contraintes qui le plus important pour nous à l'instant présent est `PRIMARY KEY`, celle ci dispose plusieurs règle de définition.
- au moins un des champs doit-être une clef primaire
- il ne peut pas y avoir plus d'un champ comme clef primaire d'une table

Nous allons matérialiser cela à la fois via des erreurs

```rust
enum ParseError {
    // ...
    PrimaryKeyNotExists,
    NonUniquePrimaryKey(Vec<Vec<String>>),
}
```

et une méthode qui a pour objet de trouver la contrainte unique de clef primaire et renvoyer dans les autres cas.

{%note()%}
La primary key est un `Vec<String>` car il est possible d'associer plus d'un champ pour construire une clef primaire.

On nomme ça des [clefs composites](https://en.wikipedia.org/wiki/Composite_key), si vous voulez prendre de l'avance sur la suite. 😇
{%end%}

```rust
fn parse_primary_key(
    fields: &Vec<(String, crate::data::ColumnDefinition)>,
) -> crate::parser::Result<Vec<String>> {
    let mut primary_keys = Vec::new();
    // on boucle sur les champs du schéma
    for (field_name, column_definition) in fields {
        // si au moins une des contraintes de la colonne est PRIMARY KEY
        if !column_definition
            .constraints
            .iter()
            .filter(|constraint| constraint == &&Constraint::PrimaryKey)
            .collect::<Vec<_>>()
            .is_empty()
        {
            // alors on rajoute le nom de la colonne
            primary_keys.push(vec![field_name.clone()]);
        }
    }

    // si aucune PRIMARY KEY n'est trouvable, le schéma est invalide
    if primary_keys.is_empty() {
        return Err(ParseError::PrimaryKeyNotExists);
    }
    // si plus d'un champ est une PRIMARY KEY, le schéma est également invalide
    if primary_keys.len() > 1 {
        return Err(ParseError::NonUniquePrimaryKey(primary_keys));
    }

    Ok(primary_keys.remove(0))
}
```

On défini un nouveau champ `primary_key` sur notre `Schema`.

```rust
struct Schema {
    pub fields: HashMap<String, ColumnDefinition>,
    pub columns: Vec<String>,
    // ici 👇
    pub primary_key: PrimaryKey,
}
```

Lors du parse on va alors appeler notre méthode `parse_primary_key` depuis notre visiteur de Schéma.

```rust
impl<'a> Schema {
    fn parse(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // ...

        // on récupère la primary key
        let primary_key = parse_primary_key(&fields)?;

        Ok(Schema::new(fields, primary_key))
    }
}
```

Ce qui permet de parser nos schémas

```rust
#[test]
fn test_constraints() {
    let data = b"(id INTEGER NOT NULL PRIMARY KEY, name_1 TEXT(50) NOT NULL)";
    let mut tokenizer = Tokenizer::new(data);
    let schema = tokenizer.visit::<Schema>().expect("failed to parse schema");
    assert_eq!(
        schema,
        Schema::new(
            vec![
                (
                    "id".to_string(),
                    ColumnDefinition::new(
                        ColumnType::Integer,
                        vec![Constraint::NotNull, Constraint::PrimaryKey,]
                    )
                ),
                (
                    "name_1".to_string(),
                    ColumnDefinition::new(ColumnType::Text(50), vec![Constraint::NotNull])
                )
            ],
            vec!["id".to_string()]
        )
    )
}

#[test]
fn test_constraints_non_existent_primary_key() {
    let data = b"(id INTEGER NOT NULL, name_1 TEXT(50) NOT NULL)";
    let mut tokenizer = Tokenizer::new(data);
    let schema = tokenizer.visit::<Schema>();
    assert_eq!(schema, Err(crate::parser::ParseError::PrimaryKeyNotExists))
}

#[test]
fn test_constraints_multiple_primary_key() {
    let data = b"(id INTEGER NOT NULL PRIMARY KEY, name_1 TEXT(50) NOT NULL PRIMARY KEY)";
    let mut tokenizer = Tokenizer::new(data);
    let schema = tokenizer.visit::<Schema>();
    assert_eq!(
        schema,
        Err(crate::parser::ParseError::NonUniquePrimaryKey(vec![
            vec!["id".to_string()],
            vec!["name_1".to_string()]
        ]))
    )
}
```

Nous avons désormais la certitude d'avoir une clef primaire unique dans la définition du schéma de notre table.

On avance ! 🤩

### Clause Where

Notre clause est extrêmement simplifié par rapport à la réalité de SQL (chaque chose en son temps 😅).

On associe un champ et une valeur séparé par un token `=`.

Notre association se matérialise par la structure

```rust
struct WhereClause {
    field: String,
    value: Value,
}
```

Que l'on visite

```rust
impl<'a> Visitable<'a, u8> for WhereClause {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        scanner.visit::<OptionalWhitespaces>()?;
        // doit commencer par WHERE
        recognize(Token::Where, scanner)?;
        // suivi d'un espace
        scanner.visit::<Whitespaces>()?;
        // suivi d'un nom de colonne
        let column = scanner.visit::<Column>()?;
        // on reconnaît le = 
        recognize(Token::Equal, scanner)?;
        // suivi d'un espace
        scanner.visit::<Whitespaces>()?;
        // suivi d'une valeur
        let value = scanner.visit::<Value>()?;
        Ok(Self {
            field: column.0,
            value,
        })
    }
}
```

Ce qui donne 

```rust
#[test]
fn test_where_clause() {
    let mut scanner = Scanner::new(b"WHERE id = 42");
    let where_clause = scanner.visit::<WhereClause>().unwrap();
    assert_eq!(where_clause.field, "id".to_string());
    assert_eq!(where_clause.value, Value::Integer(42));
}
```

### Modification de la commande SELECT

On rajoute alors notre clause where qui peut optionnellement exister

```rust
struct SelectCommand {
    pub table_name: String,
    pub projection: Projection,
    // ici 👇
    pub where_clause: Option<WhereClause>,
}
```

Que l'on peut alors visiter

```rust
impl<'a> Visitable<'a, u8> for SelectCommand {
    fn accept(scanner: &mut Tokenizer<'a>) -> parser::Result<Self> {
        // ...

        
        // le modificateur d'optionalité permet de ne pas contraindre l'existence du WHERE
        let where_clause = scanner.visit::<Optional<WhereClause>>()?.0;

        // on reconnait le point virgule terminal
        recognize(Token::Semicolon, scanner)?;

        Ok(SelectCommand {
            table_name,
            projection,
            where_clause,
        })
    }
}
```

Donnant:

```rust
#[test]
fn test_parse_select_command_with_where_clause() {
    let data = b"SELECT * FROM table WHERE id = 1;";
    let mut tokenizer = super::Tokenizer::new(data);
    let result = tokenizer.visit::<SelectCommand>().expect("failed to parse");
    assert_eq!(
        result,
        SelectCommand {
            table_name: "table".to_string(),
            projection: Projection::Star,
            where_clause: Some(WhereClause {
                field: "id".to_string(),
                value: Value::Integer(1)
            })
        }
    );
}
```

## Indexation des entrées

Maintenant que notre parser est amélioré pour définir la clef primaire de la table, nous allons pouvoir l'utiliser pour remplir notre "annuaire".

Et voici la réponse à la question "Qui est le nom et le numéro de notre annuaire ?".

Notre "nom" sera un tableau de valeur, un tuple en fait

Et notre "numéro" sera l'ID du tuple dans la table.

```rust
type PrimaryIndexes = BTreeMap<Vec<Value>, usize>;
```

Notre "annuaire" sera stocké au niveau de la table

```rust
struct Table {
    schema: Schema,
    row_number: usize,
    pager: Pager,
    // ici 👇
    primary_indexes: BTreeMap<Vec<Value>, usize>,
}

impl Table {
    pub fn new(schema: Schema) -> Self {
        Self {
            pager: Pager::new(schema.size()),
            schema,
            row_number: 0,
            primary_indexes: BTreeMap::new(),
        }
    }
}
```

Lors de l'insertion, nous allons récupérer les valeurs des champs qui constituent la clef primaire de notre table.

```rust
fn get_pk_value(
    row: &HashMap<String, Value>,
    pk_fields: &Vec<String>,
) -> Result<Vec<Value>, InsertionError> {
    let mut pk = vec![];
    for pk_field in pk_fields {
        if let Some(value) = row.get(pk_field) {
            pk.push(value.clone());
        } else {
            return Err(InsertionError::PrimaryKeyNotExists(pk_fields.to_vec()));
        }
    }
    Ok(pk)
}
```

{%note()%}
Si la clef primaire est impossible à trouver, ceci est une erreur, même si dans les faits, cette erreur ne peut pas exister car le schéma vérifie préemptivement le tuple à insérer.
{%end%}


Il est alors possible avant insertion dans la page de venir rajouter notre clef primaire dans l'index primaire.

```rust
impl Table {
    pub fn insert(&mut self, row: &HashMap<String, Value>) -> Result<(), InsertionError> {
        // récupération de la slice de données stockée par la page du tuple
        let page = self.pager.write(self.row_number);

        let mut writer = Cursor::new(page);
        // appel à la sérialisation au travers du schéma
        // la vérification est également faite à ce moment

        let pk_fields = &self.schema.primary_key;
        let pk = get_pk_value(row, pk_fields)?;
        
        // insertion dans l'index primaire de notre tuple 
        // et de l'ID courant d'insertion
        self.primary_indexes.insert(pk, self.row_number);

        self.schema
            .serialize(&mut writer, row)
            .map_err(InsertionError::Serialization)?;
        self.row_number += 1;
        Ok(())
    }
}
```

## Query Engine

Pour simplifier les recherches et les scans de tables et surtout pour éviter que le fichier tables n'enfle démesurément, nous allons mettre la logique
de recherche au sein d'un `QueryEngine`.

Celui-ci prend une référence de `Table` en paramètre.

```rust
pub struct QueryEngine<'a> {
    table: &'a Table,
}

impl<'a> QueryEngine<'a> {
    pub fn new(table: &'a Table) -> Self {
        Self { table }
    }
}
```

On peut alors créer des comportement de recherche différent.

D'abord le full-scan, qui est déplacement du code actuelle de la table vers le QueryEngine

```rust
impl QueryEngine<'_> {
    pub fn full_scan(&self, row_number: usize) -> Result<Vec<Vec<Value>>, SelectError> {
        let mut rows = Vec::with_capacity(row_number);
        for row_number in 0..row_number {
            let page = self
                .table
                .pager
                .read(row_number)
                .ok_or(SelectError::PageNotExist(row_number))?;
            let mut reader = Cursor::new(page);
            rows.push(
                // désérialisation par le schéma
                self.table
                    .schema
                    .deserialize(&mut reader)
                    .map_err(SelectError::Deserialization)?,
            )
        }
        Ok(rows)
    }
}
```

Puis notre recherche par index primaire.

Pour cela nous avons besoin de d'un utilitaire `get_row` qui récupère un tuple par rapport à son ID.

```rust
impl QueryEngine<'_> {
    fn get_row(&self, row_number: usize) -> Result<Vec<Value>, SelectError> {
        let page = self
            .table
            .pager
            .read(row_number)
            .ok_or(SelectError::PageNotExist(row_number))?;
        let mut reader = Cursor::new(page);
        let row = self
            .table
            .schema
            .deserialize(&mut reader)
            .map_err(SelectError::Deserialization)?;
        Ok(row)
    }
}
```

Et une méthode `get_by_pk`, le `pk` signifie "Primary Key".

```rust
impl QueryEngine<'_> {
    pub fn get_by_pk(
        &self,
        values: &Vec<Value>,
        primary_indexes: &PrimaryIndexes,
    ) -> Result<Vec<Vec<Value>>, SelectError> {
        // récupération l'ID du tuple
        let row_number = primary_indexes
            .get(values)
            .ok_or(SelectError::PrimaryKeyNotExists(values.clone()))?;
        // récupération de la page
        let row = self.get_row(*row_number)?;
        Ok(vec![row])
    }
}
```

## Utilisation du Query Engine

Tout étant correctement découpé, il est possible de segmenter nos recherches entre le full scan et la recherche par PK.

```rust
impl Table {
    pub fn select(
        &self,
        where_clause: Option<WhereClause>,
    ) -> Result<Vec<Vec<Value>>, SelectError> {
        // instanciation du Query Engine pour la table
        let engine = QueryEngine::new(self);

        match where_clause {
            // s'il n'y a pas de clause where on scan tout
            None => engine.full_scan(self.row_number),
            // sinon si la clause where concerne la clef primaire
            Some(WhereClause { field, value })
                if self.schema.primary_key == vec![field.clone()] =>
            {
                // on récupère l'entrée désignée
                engine.get_by_pk(&vec![value], &self.primary_indexes)
            }
            _ => Err(SelectError::NotImplemented),
        }
    }
}
```

## Testons !! 

On se créé une table qui possède une clef primaire "id".

```sql
db > CREATE TABLE Users (id INTEGER PRIMARY KEY, name TEXT(20), email TEXT(50));
db > INSERT INTO Users (id, name, email) VALUES (42, 'john.doe', 'john.doe@example.com');
db > INSERT INTO Users (id, name, email) VALUES (666, 'jane.doe', 'jane.doe@example.com');
db > INSERT INTO Users (id, name, email) VALUES (1, 'admin', 'admin@example.com');
```

On full scan la table

```sql
db > SELECT * FROM Users;
[Integer(42), Text("john.doe"), Text("john.doe@example.com")]
[Integer(666), Text("jane.doe"), Text("jane.doe@example.com")]
[Integer(1), Text("admin"), Text("admin@example.com")]
```

On récupère par PK.

```sql
db > SELECT * FROM Users WHERE id = 1;
[Integer(1), Text("admin"), Text("admin@example.com")]
db > SELECT * FROM Users WHERE id = 42;
[Integer(42), Text("john.doe"), Text("john.doe@example.com")]
db > SELECT * FROM Users WHERE id = 666;
[Integer(666), Text("jane.doe"), Text("jane.doe@example.com")]
db >
```

Succès total et absolu !! 😍

## Conclusion

Notre Query Engine a encore plein de problème et notre clef primaire ne supporte pas encore les clefs composites. Mais on a un début de quelque chose. ^^'

Dans la [prochaine partie](/rustqlite-11) nous verrons les expressions qui permettront de faire des recherches plus intéressantes.

Merci de votre lecture ❤️

Vous pouvez trouver le code la partie [ici](https://gitlab.com/blog_example/sqlite-en-rust/-/tree/10-primary-key?ref_type=heads) et le [diff](https://gitlab.com/blog_example/sqlite-en-rust/-/compare/9-pages...10-primary-key) là.
