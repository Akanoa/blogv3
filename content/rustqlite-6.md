+++
title = "Partie 6 : Parser les commandes SQL"
date = 2024-12-12
draft = false
template  = 'post.html'

[taxonomies]
categories = ["Réimplémenter sqlite en Rust"]
tags = ["rust", "sqlite", "system"]

[extra]
lang = "fr"
toc = true
math = true
mermaid = false
biscuit = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Réimplémenter sqlite en Rust : Partie 6" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/rustqlite-6.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Réimplémenter sqlite en Rust : Partie 6" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/rustqlite-6.png" },
    { property = "og:url", content="https://lafor.ge/rustqlite-6" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]

+++


{% detail(title="Les articles de la série") %}
{{ summary(path="content/toc/rustqlite.toml") }}
{% end %}

Bonjour à toutes et tous 😃

Dans la [précédente partie](/rustqlite-5) nous avons bâti un parseur en utilisant le pattern Visitor et nous vons construit la grammaire des commandes supportées.


Aujourdhui, nous allons utiliser les outils qui sont à notre dispositions et peut-être en créer de nouveau pour parser des versions simplifiées de commande SQL:
- CREATE TABLE
- SELECT 
- INSERT 

Il y a un peu de travail, mais il est comparativement à la partie 5 sera bien plus mécanique qu'exploratoire.

C'est parti !

## Modifications des tokens

Avant de nous lancer dans les parser à proprement parler, je vais retifier le tir sur quelques maladresses dans la gestion des tokens et de leur reconnaissance.

Le token Literal String que l'on a créé précédemment possède deux angles morts:
- il ne gère pas les espaces qui seront nécessaire pour certaines valeurs
- il ne gère pas les caractères spéciaux comme les symboles dont '@'

D'un autre côté, dans certain cas on veut interdire ces comportements.

On va donc créé une variante `Identfier` et développer deux matchers:
- un pour toutes les chaînes standards
- un autre seulement pour les identifiants de champs, tables, etc ...

```rust
enum Literal {
    /// number recognized
    Number,
    /// string recognized
    String,
    /// a special case of string without space or special characters accepted
    Identifier,
}
```

On peut en définir les matchers

```rust
fn match_string() -> impl Fn(&[u8]) -> (bool, usize) {
    move |x: &[u8]| -> (bool, usize) {
        if x.is_empty() {
            return (false, 0);
        }
        let mut pos = 0;
        let mut found = false;

        let forbiden_chars = ['(', ')', '\'', ','];

        loop {
            if pos == x.len() {
                return (found, pos);
            }

            if forbiden_chars.contains(&(x[pos] as char)) {
                return (found, pos);
            }

            if x[pos].is_ascii_alphanumeric()
                || x[pos].is_ascii_punctuation()
                || x[pos].is_ascii_whitespace()
            {
                found = true;
                pos += 1;
            } else {
                return (found, pos);
            }
        }
    }
}

fn match_identifier() -> impl Fn(&[u8]) -> (bool, usize) {
    move |x: &[u8]| -> (bool, usize) {
        if x.is_empty() {
            return (false, 0);
        }
        let mut pos = 0;
        let mut found = false;

        loop {
            if pos == x.len() {
                return (found, pos);
            }

            if x[pos].is_ascii_alphanumeric() || x[pos] == b'_' {
                found = true;
                pos += 1;
            } else {
                return (found, pos);
            }
        }
    }
}
```

Pour ensuite les appliquer

```rust
impl Match for Literal {
    fn matcher(&self) -> Matcher {
        match self {
            Literal::Number => Box::new(matchers::match_number()),
            Literal::Identifier => Box::new(matchers::match_identifier()),
            Literal::String => Box::new(matchers::match_string()),
        }
    }
}
```

## Composants supplémentaires

Bien que notre boîte à outils soit déjà bien chargée, certaines commandes vont nous demander des composants que l'on ne possède pas encore.

Mais que l'on peut construire à partir de ce que l'on a déjà.

### Consommer des espaces blancs optionnels

Il nous manque dans notre arsenal, la capacité de consommer les espaces blancs surnuméraires dans nos commandes.

En faire un visiteur est très aisé.

```rust
pub struct OptionalWhitespaces;

impl<'a> Visitable<'a, u8> for OptionalWhitespaces {
    fn accept(scanner: &mut Scanner<'a, u8>) -> parser::Result<Self> {
        // si on est déjà en fin de chaîne plus
        // d'espaces ne sont consommables
        if scanner.remaining().is_empty() {
            return Ok(OptionalWhitespaces);
        }

        // on boucle tant qu'on est en mesure de capturer du blanc
        while Token::Whitespace.recognize(scanner)?.is_some() {
            // si on est déjà en fin de chaîne plus
            // d'espaces ne sont consommables
            if scanner.remaining().is_empty() {
                break;
            }
        }
        Ok(OptionalWhitespaces)
    }
}
```

### Reconnaître un groupe qui se termine par un token précis

Ce cas est extrêment précis mais nécessaire pour la suite.

Lors d'un select la projection est défini par des identifiants séparé par des virgules, on sait que l'on arrive à la fin de la projection quand on atteint le token FROM

```
SELECT id, brand FROM table;
       ^        ^
       début    fin
```

Il faut donc forecast ce groupe comme on l'a fait pour les groupe délimité par des parenthèses.

Comme nos tokens ont des tailles en termes de nombre de bytes différents, il faut connaître pour chacun combien ils prennent de place dans la slice.

On rajoute un trait `Size`, qui a pour but de fournir ce comportement à nos token.

```rust
pub trait Size {
    fn size(&self) -> usize;
}

impl Size for Token {
    fn size(&self) -> usize {
        match self {
            Token::Create => 6,
            Token::Table => 5,
            Token::Insert => 6,
            Token::Into => 4,
            Token::Values => 6,
            Token::Select => 6,
            Token::From => 4,
            Token::Field(field) => field.size(),
            Token::Literal(_literal) => 0,
            _ => 1,
        }
    }
}

impl Size for Field {
    fn size(&self) -> usize {
        match self {
            Field::Integer => 7,
            Field::Text => 4,
        }
    }
}
```

{%note()%}
Je met un 0 à literal pour signifier qu'il n'a pas de taille définie, on aurait pu mettre un Option à la place, mais ça complique l'API
{%end%} 

Pour cela on va se construire un nouvel outil qui prend un token comme paramètre, ce token sera le délimiteur de fin.

```rust
struct UntilToken(Token);
```

On peut alors lui implémenter `Forecast`

```rust
impl<'a> Forecast<'a, u8, Token> for UntilToken {
    fn forecast(&self, data: &mut Scanner<'a, u8>) -> crate::parser::Result<ForecastResult<Token>> {
        let mut tokenizer = Tokenizer::new(data.remaining());

        while !tokenizer.remaining().is_empty() {
            match recognize(self.0, &mut tokenizer) {
                Ok(_element) => {
                    return Ok(ForecastResult::Found {
                        end_slice: tokenizer.cursor() - self.0.size(),
                        start: self.0,
                        end: self.0,
                    });
                }
                Err(_err) => {
                    tokenizer.bump_by(1);
                    continue;
                }
            }
        }

        Ok(ForecastResult::NotFound)
    }
}
```

Que l'on peut utiliser de cette manière.

```rust
#[test]
fn test_until_token() {
    let data = b"id, brand FROM cars";
    let mut tokenizer = Tokenizer::new(data);
    let forecast_result =
        forecast(UntilToken(Token::From), &mut tokenizer).expect("failed to forecast");
    assert_eq!(
        forecast_result,
        Forecasting {
            start: Token::From,
            end: Token::From,
            data: b"id, brand ",
        }
    )
}
```

Bon avec ça je pense que l'on a tout. ^^

On peut parser les commandes. 🎯

## Commande Create Table

{%detail(title="Grammaire de CREATE TABLE")%}

```
CreateTable ::= 'CREATE' 'TABLE' LiteralString OpenParen ColumnDef* CloseParen Semicolon
ColumnDef ::= LiteralString ColumnType | LiteralString ColumnType Comma
ColumnType ::= 'INTEGER' | ColumTypeText
ColumTypeText ::= 'TEXT' OpenParen LiteralInteger CloseParen
LiteralString ::=  [A-Za-z0-9_]*
LiteralInteger ::= [0-9]+
OpenParen ::= '('
CloseParen ::= ')'
Comma ::= ','
Semicolon ::= ';'
```  
<p style="font-size: 14px; font-weight:bold"><a name="CreateTable">CreateTable:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22613%22%20height%3D%2271%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2237%22%20width%3D%2272%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2235%22%20width%3D%2272%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2255%22%3ECREATE%3C%2Ftext%3E%3Crect%20x%3D%22123%22%20y%3D%2237%22%20width%3D%2262%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22121%22%20y%3D%2235%22%20width%3D%2262%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22131%22%20y%3D%2255%22%3ETABLE%3C%2Ftext%3E%3Crect%20x%3D%22205%22%20y%3D%2237%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22203%22%20y%3D%2235%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22213%22%20y%3D%2255%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22321%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22319%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22329%22%20y%3D%2255%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22387%22%20y%3D%223%22%20width%3D%2288%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22385%22%20y%3D%221%22%20width%3D%2288%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22395%22%20y%3D%2221%22%3EColumnDef%3C%2Ftext%3E%3Crect%20x%3D%22515%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22513%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22523%22%20y%3D%2255%22%3E%29%3C%2Ftext%3E%3Crect%20x%3D%22561%22%20y%3D%2237%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22559%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22569%22%20y%3D%2255%22%3E%3B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m0%200%20h10%20m72%200%20h10%20m0%200%20h10%20m62%200%20h10%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m26%200%20h10%20m20%200%20h10%20m0%200%20h98%20m-128%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m108%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-108%200%20h10%20m88%200%20h10%20m20%2034%20h10%20m26%200%20h10%20m0%200%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22603%2051%20611%2047%20611%2055%22%2F%3E%3Cpolygon%20points%3D%22603%2051%20595%2047%20595%2055%22%2F%3E%3C%2Fsvg%3E" height="71" width="613" usemap="#CreateTable.map"><map name="CreateTable.map"><area shape="rect" coords="203,35,299,67" href="#LiteralString" title="LiteralString"><area shape="rect" coords="385,1,473,33" href="#ColumnDef" title="ColumnDef"></map>

<p style="font-size: 14px; font-weight:bold"><a name="ColumnType">ColumnType:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22215%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2280%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2280%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3EINTEGER%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%22116%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%22116%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3EColumTypeText%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m80%200%20h10%20m0%200%20h36%20m-156%200%20h20%20m136%200%20h20%20m-176%200%20q10%200%2010%2010%20m156%200%20q0%20-10%2010%20-10%20m-166%2010%20v24%20m156%200%20v-24%20m-156%2024%20q0%2010%2010%2010%20m136%200%20q10%200%2010%20-10%20m-146%2010%20h10%20m116%200%20h10%20m23%20-44%20h-3%22%2F%3E%3Cpolygon%20points%3D%22205%2017%20213%2013%20213%2021%22%2F%3E%3Cpolygon%20points%3D%22205%2017%20197%2013%20197%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="215" usemap="#ColumnType.map"><map name="ColumnType.map"><area shape="rect" coords="49,45,165,77" href="#ColumTypeText" title="ColumTypeText"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="ColumTypeText">ColumTypeText:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22331%22%20height%3D%2237%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%223%22%20width%3D%2254%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%221%22%20width%3D%2254%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2221%22%3ETEXT%3C%2Ftext%3E%3Crect%20x%3D%22105%22%20y%3D%223%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22103%22%20y%3D%221%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22113%22%20y%3D%2221%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22151%22%20y%3D%223%22%20width%3D%22106%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22149%22%20y%3D%221%22%20width%3D%22106%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22159%22%20y%3D%2221%22%3ELiteralInteger%3C%2Ftext%3E%3Crect%20x%3D%22277%22%20y%3D%223%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22275%22%20y%3D%221%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22285%22%20y%3D%2221%22%3E%29%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m0%200%20h10%20m54%200%20h10%20m0%200%20h10%20m26%200%20h10%20m0%200%20h10%20m106%200%20h10%20m0%200%20h10%20m26%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22321%2017%20329%2013%20329%2021%22%2F%3E%3Cpolygon%20points%3D%22321%2017%20313%2013%20313%2021%22%2F%3E%3C%2Fsvg%3E" height="37" width="331" usemap="#ColumTypeText.map"><map name="ColumTypeText.map"><area shape="rect" coords="149,1,255,33" href="#LiteralInteger" title="LiteralInteger"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%22189%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Cpolygon%20points%3D%2251%20151%2058%20135%20106%20135%20113%20151%20106%20167%2058%20167%22%2F%3E%3Cpolygon%20points%3D%2249%20149%2056%20133%20104%20133%20111%20149%20104%20165%2056%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%20107%2058%2091%20104%2091%20111%20107%20104%20123%2058%20123%22%2F%3E%3Cpolygon%20points%3D%2249%20105%2056%2089%20102%2089%20109%20105%20102%20121%2056%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%2063%2058%2047%20106%2047%20113%2063%20106%2079%2058%2079%22%2F%3E%3Cpolygon%20points%3D%2249%2061%2056%2045%20104%2045%20111%2061%20104%2077%2056%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m23%20166%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20159%20179%20159%20187%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20143%20179%20143%20187%22%2F%3E%3C%2Fsvg%3E" height="189" width="161"><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161">
{%end%}

C'est la plus difficile de toute, on va la faire en premier car elle va permettre de dégager le chemin pour toutes les autres.

Elle est composée de deux partie principales:

- Une sorte de header qui identifie le nom de la table
- La déclaration du schéma

La schéma est un structure de données qui associe des nom de colonnes à leur définition.

### Type de Colonne 

Ces définitions sont typées. 

#### Integer

Ce type de colonne est le plus simple, il est constitué d'un token `Integer`.

```
INTEGER
```

Une reconnaissance de token suffit, mais il faut faire attention à d'éventuels espaces blancs avant et après le token.

On créé alors `IntegerField` pour y accrocher notre Visitor.

```rust
struct IntegerField;
impl<'a> Visitable<'a, u8> for IntegerField {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        scanner.visit::<OptionalWhitespaces>()?;
        recognize(Token::Field(TokenField::Integer), scanner)?;
        scanner.visit::<OptionalWhitespaces>()?;
        Ok(IntegerField)
    }
}
```

#### Text

Un type de colonne textuel est plus complexe car il contient la taille de la chaîne de caractères.

On se retrouve alors avec un token `Text` suivi du token `(`, un token literal integer puis finalement un token `)`.

```
TEXT(50)
```

Cette fois-ci la structure va contenir un champ représentant la taille.

```rust
struct TextField(usize);
```

L'API de parse que l'on a construit, rend alors les opérations quasi naturelles.

```rust
impl<'a> Visitable<'a, u8> for TextField {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie de potentiel blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on reconnaît le token TEXT
        recognize(Token::Field(TokenField::Text), scanner)?;
        // on nettoie de potentiel blancs
        scanner.visit::<OptionalWhitespaces>()?;

        // on reconnaît le token (
        recognize(Token::OpenParen, scanner)?;
        // on nettoie de potentiel blancs
        scanner.visit::<OptionalWhitespaces>()?;

        // on reconnaît le nombre
        let value_token = recognize(Token::Literal(Literal::Number), scanner)?;
        // on peut alors extraire les bytes qui représente ce nombre
        let value_bytes = &value_token.source[value_token.start..value_token.end];
        // les décoder depuis l'utf-8 ou dans le cas présent l'ASCII
        let value_string =
            String::from_utf8(value_bytes.to_vec()).map_err(ParseError::Utf8Error)?;
        // pour finalement parser la chaîne vers un usize
        let value = value_string.parse().map_err(ParseError::ParseIntError)?;

        // on nettoie de potentiel blancs
        scanner.visit::<OptionalWhitespaces>()?;
        recognize(Token::CloseParen, scanner)?;
        // on reconnaît le token (
        scanner.visit::<OptionalWhitespaces>()?;

        Ok(TextField(value))
    }
}
```

#### FieldResult

On vient alors fusionné nos deux variantes de parse dans une énumération.

```rust
enum ColumnTypeResult {
    Integer(IntegerField),
    Text(TextField),
}
```

Et leur pendant d'API publique

```rust
enum ColumnType {
    Integer,
    Text(usize),
}
```

Pour permettre de passer du résultat de l'acceptation à celui du parse on utilise une implémentation du trait `From`.

```rust
impl From<ColumnTypeResult> for ColumnType {
    fn from(value: ColumnTypeResult) -> Self {
        match value {
            ColumnTypeResult::Integer(_) => ColumnType::Integer,
            ColumnTypeResult::Text(TextField(value)) => ColumnType::Text(value),
        }
    }
}
```

On peut alors via l'Acceptor tester successivement les types de champs possibles.

```rust
impl<'a> Visitable<'a, u8> for ColumnType {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        Acceptor::new(scanner)
            .try_or(|field| Ok(ColumnTypeResult::Integer(field)))?
            .try_or(|field| Ok(ColumnTypeResult::Text(field)))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)
            .map(Into::into)
    }
}
```

On a alors un mécanisme qui transforme le contenu de notre Scanner en un `ColumnType`.

### Définition de colonne

La définition de la colonne est un nom et un type de colonne séparé d'un blanc.

```
id INTEGER
nom TEXT(26)
```

On le modélise par une structure.

```rust
struct ColumnDefinition {
    name: String,
    field: ColumnType,
}
```

Qui peut être rendu visitable

```rust
impl<'a> Visitable<'a, u8> for ColumnDefinition {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie de potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;

        // on reconnaît une chaîne de caractères
        let name_tokens = recognize(Token::Literal(Literal::String), scanner)?;
        // on en récupère les bytes
        let name_bytes = &name_tokens.source[name_tokens.start..name_tokens.end];
        // pour décoder le nom du champ
        let name = String::from_utf8(name_bytes.to_vec()).map_err(ParseError::Utf8Error)?;

        // l'espace blancs est obligatoire
        scanner.visit::<Whitespaces>()?;
        // on visite le ColumnType
        let field = scanner.visit::<ColumnType>()?;

        Ok(ColumnDefinition { name, field })
    }
}
```

### Schéma

Un schéma est un enchaînement de définition de colonne séparées par des virgules, entouré de parenthèses.

```
(id INTEGER, nom TEXT(26))
```

```rust
struct Schema {
    pub fields: HashMap<String, ColumnType>,
}
```

On peut alors le visiter

```rust
impl<'a> Schema {
    fn parse(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;

        // on forecast notre groupe délimité par des parenthèse
        let fields_group = forecast(GroupKind::Parenthesis, scanner)?;
        // on enlève les parenthèses périphériques quio font toutes les deux 1 bytes
        let fields_group_bytes = &fields_group.data[1..fields_group.data.len() - 1];

        // on en créé un sous scanner
        let mut fields_group_tokenizer = Tokenizer::new(fields_group_bytes);

        // que l'on visite comme une liste de définition de colonne séparé par des virgules
        let columns_definitions =
            fields_group_tokenizer.visit::<SeparatedList<ColumnDefinition, SeparatorComma>>()?;

        // que l'on transforme en notre map de définition de champs
        let fields = columns_definitions.into_iter().fold(
            HashMap::new(),
            |mut fields, column_definition| {
                fields.insert(column_definition.name, column_definition.field);
                fields
            },
        );
        
        // on n'oublie pas de déplacer le curseur du scanner externe de la taille du groupe
        scanner.bump_by(fields_group.data.len());

        Ok(Schema { fields })
    }
}
```


Ce qui nous donne 

```rust
#[test]
fn test_parse_schema() {
    let data = b"( id integer, name text  ( 50 ) )";
    let mut tokenizer = Tokenizer::new(data);
    let schema = tokenizer.visit::<Schema>().expect("failed to parse schema");
    assert_eq!(
        schema,
        Schema {
            fields: HashMap::from([
                ("id".to_string(), ColumnType::Integer),
                ("name".to_string(), ColumnType::Text(50))
            ])
        }
    );

    let data = b"(id INTEGER, name TEXT(50))";
    let mut tokenizer = Tokenizer::new(data);
    let schema = tokenizer.visit::<Schema>().expect("failed to parse schema");
    assert_eq!(
        schema,
        Schema {
            fields: HashMap::from([
                ("id".to_string(), ColumnType::Integer),
                ("name".to_string(), ColumnType::Text(50))
            ])
        }
    );
}
```

### La commande Create table

On rassemble tout le monde pour créer notre commande CREATE TABLE.

```
CREATE TABLE table1 (id INTEGER, nom TEXT(26));
```

Modélisé en:

```rust
struct CreateTableCommand {
    pub(crate) table_name: String,
    schema: Schema,
}
```

Visiter la commande est un jeu de légos. 😎

```rust
impl<'a> Visitable<'a, u8> for CreateTableCommand {
    fn accept(scanner: &mut Scanner<'a, u8>) -> parser::Result<Self> {
        // on nettoie de potentiel blanc
        scanner.visit::<OptionalWhitespaces>()?;
        // on reconnait le token CREATE
        recognize(Token::Create, scanner)?;
        // on reconnait au moins 1 blanc
        scanner.visit::<Whitespaces>()?;
        // on reconnait le token TABLE
        recognize(Token::Table, scanner)?;
        // on reconnait au moins 1 blanc
        scanner.visit::<Whitespaces>()?;
        
        // on reconnait une literal string représentant le nom de table
        let table_name_tokens = recognize(Token::Literal(Literal::String), scanner)?;
        let table_name_bytes = &table_name_tokens.source[table_name_tokens.start..table_name_tokens.end];
        let table_name = String::from_utf8(table_name_bytes.to_vec()).map_err(ParseError::Utf8Error)?;
        
        // on visite le schéma
        let schema = scanner.visit::<Schema>()?;
        
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on reconnaît le point-virgule final
        recognize(Token::Semicolon, scanner)?;
        Ok(CreateTableCommand { table_name, schema })
    }
}
```

On arrive finalement à notre but:

```rust
#[test]
    fn test_parse_create_table_command() {
        let data = b"create table   users   (id   integer,   name   text(50) );";
        let mut scanner = Scanner::new(data);
        let result = CreateTableCommand::accept(&mut scanner).expect("failed to parse");
        assert_eq!(
            result,
            CreateTableCommand {
                table_name: "users".to_string(),
                schema: Schema {
                    fields: HashMap::from([
                        ("id".to_string(), ColumnType::Integer),
                        ("name".to_string(), ColumnType::Text(50))
                    ])
                }
            }
        );
    }
```

## Command Insert Into

{%detail(title="Grammaire de INSERT INTO")%}

```
InsertInto ::= 'INSERT' 'INTO' LiteralString  OpenParen Column* CloseParen 'VALUES' OpenParen Value*  CloseParen Semicolon
Column ::= LiteralString | LiteralString Comma
Value ::= (LiteralInteger | LiteralString ) | ((LiteralInteger | LiteralString) Comma ) 
LiteralString ::=  "'" [A-Za-z0-9_]* "'"
LiteralInteger ::= [0-9]+
OpenParen ::= '('
CloseParen ::= ')'
Comma ::= ','
Semicolon ::= ';'
```

<p style="font-size: 14px; font-weight:bold"><a name="InsertInto">InsertInto:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22885%22%20height%3D%2271%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2237%22%20width%3D%2270%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2235%22%20width%3D%2270%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2255%22%3EINSERT%3C%2Ftext%3E%3Crect%20x%3D%22121%22%20y%3D%2237%22%20width%3D%2256%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22119%22%20y%3D%2235%22%20width%3D%2256%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22129%22%20y%3D%2255%22%3EINTO%3C%2Ftext%3E%3Crect%20x%3D%22197%22%20y%3D%2237%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22195%22%20y%3D%2235%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22205%22%20y%3D%2255%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22313%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22311%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22321%22%20y%3D%2255%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22379%22%20y%3D%223%22%20width%3D%2268%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22377%22%20y%3D%221%22%20width%3D%2268%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22387%22%20y%3D%2221%22%3EColumn%3C%2Ftext%3E%3Crect%20x%3D%22487%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22485%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22495%22%20y%3D%2255%22%3E%29%3C%2Ftext%3E%3Crect%20x%3D%22533%22%20y%3D%2237%22%20width%3D%2272%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22531%22%20y%3D%2235%22%20width%3D%2272%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22541%22%20y%3D%2255%22%3EVALUES%3C%2Ftext%3E%3Crect%20x%3D%22625%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22623%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22633%22%20y%3D%2255%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22691%22%20y%3D%223%22%20width%3D%2256%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22689%22%20y%3D%221%22%20width%3D%2256%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22699%22%20y%3D%2221%22%3EValue%3C%2Ftext%3E%3Crect%20x%3D%22787%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22785%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22795%22%20y%3D%2255%22%3E%29%3C%2Ftext%3E%3Crect%20x%3D%22833%22%20y%3D%2237%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22831%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22841%22%20y%3D%2255%22%3E%3B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m0%200%20h10%20m70%200%20h10%20m0%200%20h10%20m56%200%20h10%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m26%200%20h10%20m20%200%20h10%20m0%200%20h78%20m-108%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m88%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-88%200%20h10%20m68%200%20h10%20m20%2034%20h10%20m26%200%20h10%20m0%200%20h10%20m72%200%20h10%20m0%200%20h10%20m26%200%20h10%20m20%200%20h10%20m0%200%20h66%20m-96%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m76%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-76%200%20h10%20m56%200%20h10%20m20%2034%20h10%20m26%200%20h10%20m0%200%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22875%2051%20883%2047%20883%2055%22%2F%3E%3Cpolygon%20points%3D%22875%2051%20867%2047%20867%2055%22%2F%3E%3C%2Fsvg%3E" height="71" width="885" usemap="#InsertInto.map"><map name="InsertInto.map"><area shape="rect" coords="195,35,291,67" href="#LiteralString" title="LiteralString"><area shape="rect" coords="377,1,445,33" href="#Column" title="Column"><area shape="rect" coords="689,1,745,33" href="#Value" title="Value"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="Column">Column:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22239%22%20height%3D%2269%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%223%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%221%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2239%22%20y%3D%2221%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22167%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22165%22%20y%3D%2233%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22175%22%20y%3D%2253%22%3E%2C%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m0%200%20h10%20m96%200%20h10%20m20%200%20h10%20m0%200%20h34%20m-64%200%20h20%20m44%200%20h20%20m-84%200%20q10%200%2010%2010%20m64%200%20q0%20-10%2010%20-10%20m-74%2010%20v12%20m64%200%20v-12%20m-64%2012%20q0%2010%2010%2010%20m44%200%20q10%200%2010%20-10%20m-54%2010%20h10%20m24%200%20h10%20m23%20-32%20h-3%22%2F%3E%3Cpolygon%20points%3D%22229%2017%20237%2013%20237%2021%22%2F%3E%3Cpolygon%20points%3D%22229%2017%20221%2013%20221%2021%22%2F%3E%3C%2Fsvg%3E" height="69" width="239" usemap="#Column.map"><map name="Column.map"><area shape="rect" coords="29,1,125,33" href="#LiteralString" title="LiteralString"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="Value">Value:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22289%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%22106%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%22106%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2221%22%3ELiteralInteger%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22217%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22215%22%20y%3D%2233%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22225%22%20y%3D%2253%22%3E%2C%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m106%200%20h10%20m-146%200%20h20%20m126%200%20h20%20m-166%200%20q10%200%2010%2010%20m146%200%20q0%20-10%2010%20-10%20m-156%2010%20v24%20m146%200%20v-24%20m-146%2024%20q0%2010%2010%2010%20m126%200%20q10%200%2010%20-10%20m-136%2010%20h10%20m96%200%20h10%20m0%200%20h10%20m40%20-44%20h10%20m0%200%20h34%20m-64%200%20h20%20m44%200%20h20%20m-84%200%20q10%200%2010%2010%20m64%200%20q0%20-10%2010%20-10%20m-74%2010%20v12%20m64%200%20v-12%20m-64%2012%20q0%2010%2010%2010%20m44%200%20q10%200%2010%20-10%20m-54%2010%20h10%20m24%200%20h10%20m23%20-32%20h-3%22%2F%3E%3Cpolygon%20points%3D%22279%2017%20287%2013%20287%2021%22%2F%3E%3Cpolygon%20points%3D%22279%2017%20271%2013%20271%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="289" usemap="#Value.map"><map name="Value.map"><area shape="rect" coords="49,1,155,33" href="#LiteralInteger" title="LiteralInteger"><area shape="rect" coords="49,45,145,77" href="#LiteralString" title="LiteralString"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22249%22%20height%3D%22203%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%22169%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%22167%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%22187%22%3E%27%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%20151%20102%20135%20150%20135%20157%20151%20150%20167%20102%20167%22%2F%3E%3Cpolygon%20points%3D%2293%20149%20100%20133%20148%20133%20155%20149%20148%20165%20100%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%20107%20102%2091%20148%2091%20155%20107%20148%20123%20102%20123%22%2F%3E%3Cpolygon%20points%3D%2293%20105%20100%2089%20146%2089%20153%20105%20146%20121%20100%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%2063%20102%2047%20150%2047%20157%2063%20150%2079%20102%2079%22%2F%3E%3Cpolygon%20points%3D%2293%2061%20100%2045%20148%2045%20155%2061%20148%2077%20100%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2295%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2293%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22103%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Crect%20x%3D%22197%22%20y%3D%22169%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22195%22%20y%3D%22167%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22205%22%20y%3D%22187%22%3E%27%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m0%200%20h10%20m24%200%20h10%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m20%20166%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22239%20183%20247%20179%20247%20187%22%2F%3E%3Cpolygon%20points%3D%22239%20183%20231%20179%20231%20187%22%2F%3E%3C%2Fsvg%3E" height="203" width="249"><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161">

{%end%}

Elle peut faire peur de prime abord, mais elle est relativement innofensive quand on y réfléchie bien.

Elle est composé de 2 grosses partie.

La définition de la table et celle des valeurs.

### Définition des champs

Commençons par les champs. Un groupe délimité par des parenthèses séparé par des virgules de literals string est précédé par 2 token INSERT,INTO, {table} où {table} est une literal string représentant le nom de la table.

```
INSERT INTO ma_table(id, brand)
```

Et ça rien ce n'est rien qu'on a pas déjà réussi à faire.

Il faut penser à découper astucieusement le travail.

D'abord on reconnaît une `Column`

```rust
struct Column(String);

impl<'a> Visitable<'a, u8> for Column {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;

        // on reconnait la literal string
        let name_tokens = recognize(Token::Literal(Literal::String), scanner)?;
        let name_bytes = &name_tokens.source[name_tokens.start..name_tokens.end];
        let name = String::from_utf8(name_bytes.to_vec()).map_err(ParseError::Utf8Error)?;

        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        Ok(Column(name))
    }
}
```

Puis ensuite, on peut en faire un groupe de `Column` que l'on réduit en un wrapper de `Vec<String>`

```rust
#[derive(Debug, PartialEq)]
pub struct Columns(Vec<String>);

impl<'a> Visitable<'a, u8> for Columns {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on reconnaît le token "("
        recognize(Token::OpenParen, scanner)?;
        // on capture le groupe de nom de colonnes
        let columns_group = scanner.visit::<SeparatedList<Column, SeparatorComma>>()?;
        let colums = columns_group.into_iter();

        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on reconnaît le token ")"
        recognize(Token::CloseParen, scanner)?;
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        
        Ok(Columns(colums.map(|column| column.0).collect()))
    }
}
```

Et voilà ! 

```rust
#[test]
fn test_columns() {
    let data = b"(id, brand)";
    let mut tokenizer = Tokenizer::new(data);
    let result = tokenizer.visit::<Columns>().expect("failed to parse");
    assert_eq!(result.0, vec!["id", "brand"]);
}
```

### Définitions des valeurs

Deux types de valeurs sont acceptées:
- des entiers : 1235, 42, 0
- des string : 'test data', 'email@example.com' 

{%warning()%}
Les chaînes sont entourées par des guillemets simples.
{%end%}


Nous avons donc deux choses à gérer, le type de donnée (INTEGER, TEXT) et le fait que c'est une liste.

Occupons-nous d'abord des entiers.

```rust
struct IntegerValue(i64);

impl<'a> Visitable<'a, u8> for IntegerValue {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on récupère le nombre
        let number_token = recognize(Token::Literal(Literal::Number), scanner)?;
        let number_bytes = &number_token.source[number_token.start..number_token.end];
        let number_string =
            String::from_utf8(number_bytes.to_vec()).map_err(ParseError::Utf8Error)?;
        // que l'on parse vers un i64 
        let number = number_string.parse().map_err(ParseError::ParseIntError)?;
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        Ok(IntegerValue(number))
    }
}
```

On fait de même avec les champs texte

```rust
struct TextValue(String);

impl<'a> Visitable<'a, u8> for TextValue {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on reconnait le début du groupe quoted
        recognize(Token::Quote, scanner)?;

        // on récupère le literal string
        let literal_token = recognize(Token::Literal(Literal::String), scanner)?;
        let literal_bytes = &literal_token.source[literal_token.start..literal_token.end];
        let literal_string =
            String::from_utf8(literal_bytes.to_vec()).map_err(ParseError::Utf8Error)?;

        // on reconnait la fin du groupe quoted
        recognize(Token::Quote, scanner)?;

        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;

        Ok(TextValue(literal_string))
    }
}
```

On réalise une union des variantes

```rust
enum ValueResult {
    Integer(IntegerValue),
    Text(TextValue),
}
```

On créé l'énumération des types de valeurs

```rust
enum Value {
    Integer(i64),
    Text(String),
}
```

On lui rajoute de la glue technique

```rust
impl From<ValueResult> for Value {
    fn from(value: ValueResult) -> Self {
        match value {
            ValueResult::Integer(IntegerValue(value)) => Value::Integer(value),
            ValueResult::Text(TextValue(value)) => Value::Text(value),
        }
    }
}
```

Ce qui permet alors d'en faire un Acceptor et de visiter la `Value`.

```rust
impl<'a> Visitable<'a, u8> for Value {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        Acceptor::new(scanner)
            .try_or(|value| Ok(ValueResult::Integer(value)))?
            .try_or(|value| Ok(ValueResult::Text(value)))?
            .finish()
            .ok_or(UnexpectedToken)
            .map(Into::into)
    }
}
```

Utilisable ainsi.

```rust
#[test]
fn test_integer_value() {
    let data = b"123";
    let mut tokenizer = Tokenizer::new(data);
    let value = IntegerValue::accept(&mut tokenizer).expect("failed to parse");
    assert_eq!(value, IntegerValue(123));
}

#[test]
fn test_text_value() {
    let data = b"'test'";
    let mut tokenizer = Tokenizer::new(data);
    let value = TextValue::accept(&mut tokenizer).expect("failed to parse");
    assert_eq!(value, TextValue("test".to_string()));
}
```

On peut alors accumuler les `Value`.

```rust
struct Values(Vec<Value>);

impl<'a> Visitable<'a, u8> for Values {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on récupère le groupe de valeurs
        let values_group = forecast(GroupKind::Parenthesis, scanner)?;
        let values_group_bytes = &values_group.data[1..values_group.data.len() - 1];
        // on créé le tokenizer intermédiaire
        let mut value_group_tokenizer = Tokenizer::new(values_group_bytes);
        // on reconnait les values
        let values_list = value_group_tokenizer.visit::<SeparatedList<Value, SeparatorComma>>()?;
        let values = values_list.into_iter().collect();
        // on avance le tokenizer externe
        scanner.bump_by(values_group.data.len());
        Ok(Values(values))
    }
}
```

Et ça marche plutôt bien 😎

```rust
#[test]
fn text_values() {
    let data = b"('test1', 120, 'test3')";
    let mut tokenizer = Tokenizer::new(data);
    let values = Values::accept(&mut tokenizer).expect("failed to parse");
    assert_eq!(
        values,
        Values(vec![
            Value::Text("test1".to_string()),
            Value::Integer(120),
            Value::Text("test3".to_string())
        ])
    );
}
```

## Commande Insert Into

On modélise la commande par la structure suivante:

```rust
struct InsertIntoCommand {
    table_name: String,
    fields: HashMap<String, String>
}
```

On en fait un Visiteur

```rust
impl<'a> Visitable<'a, u8> for InsertIntoCommand {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on reconnaît le token INSERT
        recognize(Token::Insert, scanner)?;
        // on reconnait au moins un blanc
        scanner.visit::<Whitespaces>()?;
        // on reconnaît le token INTO
        recognize(Token::Into, scanner)?;
        // on reconnait au moins un blanc
        scanner.visit::<Whitespaces>()?;

        // on reconnait le nom de la table
        let name_tokens = recognize(Token::Literal(Literal::String), scanner)?;
        let name_bytes = name_tokens.source[name_tokens.start..name_tokens.end].to_vec();
        let table_name = String::from_utf8(name_bytes).map_err(ParseError::Utf8Error)?;

        // on visite les noms de colonne
        let columns = scanner.visit::<Columns>()?;

        // on reconnait au moins un blanc
        scanner.visit::<Whitespaces>()?;

        // on reconnaît le token VALUES
        recognize(Token::Values, scanner)?;

        // on nettoie les potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;

        // on reconnaît les values
        let values = scanner.visit::<Values>()?;

        // on zip les couples (colonne, valeur)
        let fields = zip(values.0, columns.0).fold(HashMap::new(), |mut map, (value, column)| {
            map.insert(column, value);
            map
        });

        Ok(InsertIntoCommand { table_name, fields })
    }
}
```

Qui fonctionne ainsi

```rust
#[test]
fn test_insert_into_command() {
    let data = b"INSERT INTO users(id, name, email) VALUES(42, 'user 1', 'email@example.com')";
    let mut tokenizer = Tokenizer::new(data);
    let result = tokenizer
        .visit::<InsertIntoCommand>()
        .expect("failed to parse");
    assert_eq!(result.table_name, "users");
    assert_eq!(result.fields.len(), 3);
    assert_eq!(result.fields.get("id").unwrap(), &Value::Integer(42));
    assert_eq!(
        result.fields.get("name").unwrap(),
        &Value::Text("user 1".to_string())
    );
    assert_eq!(
        result.fields.get("email").unwrap(),
        &Value::Text("email@example.com".to_string())
    );
    assert_eq!(tokenizer.cursor(), 76);
}
```

Succès ! 😊



## Commande Select From

{%detail(title="Grammaire de SELECT")%}

```
Select ::= 'SELECT' Column* 'FROM' LiteralString Semicolon
Column ::= '*' | LiteralString | LiteralString Comma
LiteralString ::=  [A-Za-z0-9_]*
LiteralInteger ::= [0-9]+
OpenParen ::= '('
CloseParen ::= ')'
Comma ::= ','
Semicolon ::= ';'
```

<p style="font-size: 14px; font-weight:bold"><a name="Select">Select:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22497%22%20height%3D%2271%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2237%22%20width%3D%2270%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2235%22%20width%3D%2270%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2255%22%3ESELECT%3C%2Ftext%3E%3Crect%20x%3D%22141%22%20y%3D%223%22%20width%3D%2268%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22139%22%20y%3D%221%22%20width%3D%2268%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22149%22%20y%3D%2221%22%3EColumn%3C%2Ftext%3E%3Crect%20x%3D%22249%22%20y%3D%2237%22%20width%3D%2260%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22247%22%20y%3D%2235%22%20width%3D%2260%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22257%22%20y%3D%2255%22%3EFROM%3C%2Ftext%3E%3Crect%20x%3D%22329%22%20y%3D%2237%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22327%22%20y%3D%2235%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22337%22%20y%3D%2255%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22445%22%20y%3D%2237%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22443%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22453%22%20y%3D%2255%22%3E%3B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m0%200%20h10%20m70%200%20h10%20m20%200%20h10%20m0%200%20h78%20m-108%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m88%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-88%200%20h10%20m68%200%20h10%20m20%2034%20h10%20m60%200%20h10%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22487%2051%20495%2047%20495%2055%22%2F%3E%3Cpolygon%20points%3D%22487%2051%20479%2047%20479%2055%22%2F%3E%3C%2Fsvg%3E" height="71" width="497" usemap="#Select.map"><map name="Select.map"><area shape="rect" coords="139,1,207,33" href="#Column" title="Column"><area shape="rect" coords="327,35,423,67" href="#LiteralString" title="LiteralString"></map>
<p style="font-size: 14px; font-weight:bold"><a name="Column">Column:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22279%22%20height%3D%22113%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E%2A%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22187%22%20y%3D%2279%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22185%22%20y%3D%2277%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22195%22%20y%3D%2297%22%3E%2C%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m28%200%20h10%20m0%200%20h152%20m-220%200%20h20%20m200%200%20h20%20m-240%200%20q10%200%2010%2010%20m220%200%20q0%20-10%2010%20-10%20m-230%2010%20v24%20m220%200%20v-24%20m-220%2024%20q0%2010%2010%2010%20m200%200%20q10%200%2010%20-10%20m-210%2010%20h10%20m96%200%20h10%20m20%200%20h10%20m0%200%20h34%20m-64%200%20h20%20m44%200%20h20%20m-84%200%20q10%200%2010%2010%20m64%200%20q0%20-10%2010%20-10%20m-74%2010%20v12%20m64%200%20v-12%20m-64%2012%20q0%2010%2010%2010%20m44%200%20q10%200%2010%20-10%20m-54%2010%20h10%20m24%200%20h10%20m43%20-76%20h-3%22%2F%3E%3Cpolygon%20points%3D%22269%2017%20277%2013%20277%2021%22%2F%3E%3Cpolygon%20points%3D%22269%2017%20261%2013%20261%2021%22%2F%3E%3C%2Fsvg%3E" height="113" width="279" usemap="#Column.map"><map name="Column.map"><area shape="rect" coords="49,45,145,77" href="#LiteralString" title="LiteralString"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%22189%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Cpolygon%20points%3D%2251%20151%2058%20135%20106%20135%20113%20151%20106%20167%2058%20167%22%2F%3E%3Cpolygon%20points%3D%2249%20149%2056%20133%20104%20133%20111%20149%20104%20165%2056%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%20107%2058%2091%20104%2091%20111%20107%20104%20123%2058%20123%22%2F%3E%3Cpolygon%20points%3D%2249%20105%2056%2089%20102%2089%20109%20105%20102%20121%2056%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%2063%2058%2047%20106%2047%20113%2063%20106%2079%2058%2079%22%2F%3E%3Cpolygon%20points%3D%2249%2061%2056%2045%20104%2045%20111%2061%20104%2077%2056%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m23%20166%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20159%20179%20159%20187%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20143%20179%20143%20187%22%2F%3E%3C%2Fsvg%3E" height="189" width="161"><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161"><br>

{%end%}

La dernière commande que nous allons parser est le Select, elle semblait la plus simple mais elle m'a donné un peu de réflexion à avoir sur la projection.

### Projection

La projection permet de sélectionner les champs qui doivent être renvoyé lors d'un SELECT.

La projection va fonctionner de la même manière que pour les `ColunmType`, il y là aussi deux variantes:
- \* : qui projete tous les champs
- columns : qui liste des champs précis

Occupons-nous de la version '*'.

```rust
struct ProjectionStar;

impl<'a> Visitable<'a, u8> for ProjectionStar {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie de potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on reconnait l'étoile
        recognize(Token::Star, scanner)?;
        // on nettoie de potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        Ok(ProjectionStar)
    }
}
```

Rien de bien exaltant à expliquer.

La version "colonnes" est plus intéressante.

```rust
struct ProjectionColumns(Vec<String>);

impl<'a> Visitable<'a, u8> for ProjectionColumns {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on nettoie de potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        
        // on capture le groupe qui se termine par le token FROM
        let columns_tokens = forecast(UntilToken(Token::From), scanner)?;
        let columns_bytes = columns_tokens.data;
        
        // on crée un tokenizer pour récupérer les noms de colonnes
        let mut columns_tokenizer = Tokenizer::new(columns_bytes);
        // on visite une liste de colonnes
        let columns_list = columns_tokenizer.visit::<SeparatedList<Column, SeparatorComma>>()?;
        let columns = columns_list
            .into_iter()
            .map(|x| x.0)
            .collect::<Vec<String>>();
        
        // on nettoie de potentiels blancs
        scanner.visit::<OptionalWhitespaces>()?;
        // on avance le curseur
        scanner.bump_by(columns_tokens.data.len());
        Ok(ProjectionColumns(columns))
    }
}
```

On fusionne les deux types de projection.

```rust
enum ProjectionResult {
    Star(ProjectionStar),
    Columns(ProjectionColumns),
}
```

On créé également la structure de `Projection`.

```rust
enum Projection {
    Columns(Vec<String>),
    Star,
}
```

Et en faire la glue.

```rust
impl From<ProjectionResult> for Projection {
    fn from(value: ProjectionResult) -> Self {
        match value {
            ProjectionResult::Star(ProjectionStar) => Projection::Star,
            ProjectionResult::Columns(ProjectionColumns(columns)) => Projection::Columns(columns),
        }
    }
}
```

Qui permet finalement d'en créer un visiteur.

```rust
impl<'a> Visitable<'a, u8> for Projection {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        Acceptor::new(scanner)
            .try_or(|projection| Ok(ProjectionResult::Star(projection)))?
            .try_or(|projection| Ok(ProjectionResult::Columns(projection)))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)
            .map(Into::into)
    }
}
```

Qui s'utilise ainsi.

```rust
#[test]
fn test_projections() {
    let data = b"* FROM";
    let mut tokenizer = Tokenizer::new(data);
    let projection = tokenizer.visit::<Projection>().expect("failed to parse");
    assert_eq!(projection, Projection::Star);

    let data = b"id, name FROM";
    let mut tokenizer = Tokenizer::new(data);
    let projection = tokenizer.visit::<Projection>().expect("failed to parse");
    assert_eq!(
        projection,
        Projection::Columns(vec!["id".to_string(), "name".to_string()])
    );
}
```

### La commande Select From

Une fois en possession de la `Projection`, il est possible de mettre en place la commande Select

```rust
struct SelectCommand {
    table_name: String,
    projection: Projection,
}

impl<'a> Visitable<'a, u8> for SelectCommand {
    fn accept(scanner: &mut Tokenizer<'a>) -> parser::Result<Self> {
        // on nettoie les potentiels espaces
        scanner.visit::<OptionalWhitespaces>()?;
        recognize(Token::Select, scanner)?;
        // on reconnait au moins un espace
        scanner.visit::<Whitespaces>()?;
        // on reconnait la projection
        let projection = scanner.visit::<Projection>()?;
        // on reconnait le token FROM
        recognize(Token::From, scanner)?;
        // on reconnait au moins un espace
        scanner.visit::<Whitespaces>()?;

        // on reconnait le nom de la table
        let name_tokens = recognize(Token::Literal(Literal::Identifier), scanner)?;
        let name_bytes = name_tokens.source[name_tokens.start..name_tokens.end].to_vec();
        let table_name = String::from_utf8(name_bytes).map_err(ParseError::Utf8Error)?;

        // on nettoie les potentiels espaces
        scanner.visit::<OptionalWhitespaces>()?;

        // on reconnait le point virgule terminal
        recognize(Token::Semicolon, scanner)?;

        Ok(SelectCommand {
            table_name,
            projection,
        })
    }
}
```

Et ça marche comme sur des roulettes !

```rust
#[test]
fn test_parse_select_command() {
    let data = b"SELECT * FROM table;";
    let mut tokenizer = super::Tokenizer::new(data);
    let result = tokenizer.visit::<SelectCommand>().expect("failed to parse");
    assert_eq!(
        result,
        SelectCommand {
            table_name: "table".to_string(),
            projection: Projection::Star
        }
    );

    let data = b"SELECT id, brand FROM table_2;";
    let mut tokenizer = super::Tokenizer::new(data);
    let result = tokenizer.visit::<SelectCommand>().expect("failed to parse");
    assert_eq!(
        result,
        SelectCommand {
            table_name: "table_2".to_string(),
            projection: Projection::Columns(vec!["id".to_string(), "brand".to_string()])
        }
    )
}
```

## Parser les commandes

Comme tout est étagé et isolé. La cerise sur le gâteau est evidente.

```rust
enum Command {
    CreateTable(CreateTableCommand),
    Select(SelectCommand),
    InsertInto(InsertIntoCommand),
}

impl<'a> Visitable<'a, u8> for Command {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        Acceptor::new(scanner)
            .try_or(|command| Ok(Command::Select(command)))?
            .try_or(|command| Ok(Command::CreateTable(command)))?
            .try_or(|command| Ok(Command::InsertInto(command)))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)
    }
}
```

Et tout se goupille !! 😍

```rust
#[test]
fn test_select() {
    let data = b"SELECT * FROM table;";
    let mut tokenizer = Tokenizer::new(data);
    assert_eq!(
        tokenizer.visit(),
        Ok(Command::Select(SelectCommand {
            table_name: "table".to_string(),
            projection: Projection::Star
        }))
    );

    let data = b"SELECT id, brand FROM table_2;";
    let mut tokenizer = Tokenizer::new(data);
    assert_eq!(
        tokenizer.visit(),
        Ok(Command::Select(SelectCommand {
            table_name: "table_2".to_string(),
            projection: Projection::Columns(vec!["id".to_string(), "brand".to_string()])
        }))
    );
}

#[test]
fn test_insert() {
    let data = b"INSERT INTO Users(id, name, email) VALUES (42, 'user 1', 'email@example.com')";
    let mut tokenizer = Tokenizer::new(data);
    assert_eq!(
        tokenizer.visit(),
        Ok(Command::InsertInto(InsertIntoCommand {
            table_name: "Users".to_string(),
            fields: vec![
                ("id".to_string(), Value::Integer(42)),
                ("name".to_string(), Value::Text("user 1".to_string())),
                (
                    "email".to_string(),
                    Value::Text("email@example.com".to_string())
                )
            ]
            .into_iter()
            .collect()
        }))
    );
}

#[test]
fn test_create() {
    let data = b"CREATE TABLE Users(id INTEGER, name TEXT(50), email TEXT(128));";
    let mut tokenizer = Tokenizer::new(data);
    assert_eq!(
        tokenizer.visit(),
        Ok(Command::CreateTable(CreateTableCommand {
            table_name: "Users".to_string(),
            schema: Schema {
                fields: HashMap::from([
                    ("id".to_string(), ColumnType::Integer),
                    ("name".to_string(), ColumnType::Text(50)),
                    ("email".to_string(), ColumnType::Text(128)),
                ]),
            }
        }))
    )
}
```

## Conclusion

Ok ! La parenthèse "parser" est enfin terminée ! Il n'est pas parfait, il a plein de problèmes, mais il va permettre de pouvoir attaquer la création des schémas
sur nos tables et donc pouvoir nous débarasser des stuctures User et Car et tous les inconvénients qu'elles comportent.

Dans la [prochaine partie](/rustqlite-7) on généralisera les données stockées !

Merci de votre lecture ❤️

Vous pouvez trouver le code la partie [ici](https://gitlab.com/blog_example/sqlite-en-rust/-/tree/6-parser-command-sql?ref_type=heads) et le [diff](https://gitlab.com/blog_example/sqlite-en-rust/-/compare/5-parser...6-parser-command-sql) là.
