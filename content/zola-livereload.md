+++
title = "Utiliser le livereload de Zola dans Gitpod"
date = 2022-08-11
draft = false

[taxonomies]
categories = ["Autre"]
tags = ["tools"]

[extra]
lang = "fr"
toc = true
show_comment = true
math = false
mermaid = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120
metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Utiliser le livereload de Zola dans Gitpod" },
    { name = "twitter:image", content="https://i.ibb.co/bNHkTtX/zola.png" },
]
+++

Bonjour à toutes et tous 😁

Une fois n'est pas coutume, un très court article.

Si vous me suivez sur [twitter](https://twitter.com/_Akanoa_), vous avez sûrement remarqué que je parle beaucoup de [Zola](getzola.org).

Je suis en train de réécrire mon blog avec, vous êtes peut-être d'ailleurs sur le nouveau blog en ce moment. ^^

Le problème est que Zola est très opinated, et une des positions qu'il a pris c'est le livereload c'est en localhost, point barre ! 👺

Zola injecte une balise de livereload  qui expose un livereload en http:///:1024 et il est impossible de configurer quoique ce soit.

```html
<script src="/livereload.js?port=1024&amp;mindelay=10"></script>
```

Or Gitpod expose chacun des port sortant du container sous la forme d'une URL ayant la nomencalture suivante:

```
https://<port>-<namespace>-<project>-<internal-domain>.gitpod.io
```

Exemple:

```
https://1024-akanoa-blogv2-jr01424va03.ws-eu67.gitpod.io
```

Toute l'astuce va être de devoir modifier l'URL du livereload en:

```html
<script src="https://1024-akanoa-blogv2-jr01424va03.ws-eu67.gitpod.io?port=443&amp;mindelay=10"></script>
```

Le problème c'est que l'on a pas la main sur l'injection de la balise de script.

Heureusement, il existe une solution! 

Je me suis inspiré de cette [issue](https://github.com/getzola/zola/issues/1560).

Bon, on peut configurer le host du livereload dynamiquement, c'est cool ! 😀

Maintenant comment fait-on pour récupérer l'URL du port ?

Et bien ici aussi, il y a une solution !

Gitpod expose une commande:

```
gp url 1024
```

Ceci va nous donner la valeur de l'URL d'exposition du port 1024.

Bon on avance, maintenant comment, faire pour que Zola comprennent que le port 1024 on en veut pas?

Et bien, il faut se rendre dans le `master template` de votre Zola.

Et la toute, toute fin, rajouter ceci

```html
<html>
    <head>
    </head>
    <body>
        <script>
            window.LiveReloadOptions = {
            https: true,
            host: '1024-akanoa-blogv2-jr01424va03.ws-eu67.gitpod.io',
            port: 443,
            };
        </script>
    </body>
</html>
```

Avec ça déjà ça devrait marcher pour le livereload. (changez le domaine avec votre retour de la command `gp url 1024`)

Pour l'instant c'est statique, mais on va le rendre dynamique.

Pour cela un peu de bash

```bash
export LIVERELOAD_HOST=$(gp url 1024)
export LIVERELOAD_HOST=${LIVERELOAD_HOST:8}
```

Ces deux commandes récupère l'URL et le transforme en domaine en retirant le "https://" qui est devant.

Maintenant nous allons automatiser la manip:

Dans le fichier `.gitpod.yml`, rajoutez ou modifiez votre `init task`

```yml
tasks:
  - command: > 
      export LIVERELOAD_HOST=$(gp url 1024) &&
      export LIVERELOAD_HOST=${LIVERELOAD_HOST:8} &&
      zola serve --base-url /
ports:
  - port: 1024
    onOpen: ignore
```

On ignore aussi la preview du 1024, c'est du websocket, il n'y a rien a preview ^^

Ensuite on revient dans `master template` de votre Zola.

```html
<html>
    <head>
    </head>
    <body>
        {% set livereload = get_env(name="LIVERELOAD_HOST", default="") -%}
        {% if livereload != "" -%}
        <script>
            window.LiveReloadOptions = {
            https: true,
            host: '{{ livereload }}',
            port: 443,
            };
        </script>
        {% endif -%}
    </body>
</html>
```

On définit aussi plusieurs choses:

1. On récupère le domaine du port 1024 depuis la variable `LIVERELOAD_HOST`
2. On vérifie que cette variable existe et n'est pas vide, cela permet en prod de ne pas afficher cette balise de script
3. On template

Et voilà, c'est fini, à vous les joies du livereload dans Gitpod avec Zola. 🎉

C'était très court, mais si ça peut aider du monde ^^

Je vous remercie de votre lecture et à la prochaine 😀