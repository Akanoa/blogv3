#!/usr/bin/env bash

NAME=$(gum input --placeholder "Titre de l'article")
URL=$(gum input --placeholder "Nom du fichier")
TITLE=$(gum input --placeholder "Titre de l'article sur twitter")
THUMBNAIL=$(gum input --placeholder "Nom de l'image de présentation" )
echo -n "Introduction de l'article"
INTRO=$(gum input --placeholder "Message d'introduction" --value "Bonjour à toutes et à tous 😀" )
CATEGORIES=$(gum choose Rust SysAdmin Réseau "Rust par le métal" --limit 1)
DATE=$(date +%Y-%m-%d)


NAME=$NAME TITLE=$TITLE URL=$URL INTRO=$INTRO CATEGORIES=$CATEGORIES DATE=$DATE THUMBNAIL=$THUMBNAIL envsubst < ./utils/template.md > /tmp/post.md

gum pager < /tmp/post.md

gum confirm "Est ce correct ?" && mv /tmp/post.md content/$URL.md
