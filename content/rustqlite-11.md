+++
title = "Partie 11 : Expressions Logiques SQL"
date = 2024-12-21
draft = false
template  = 'post.html'

[taxonomies]
categories = ["Réimplémenter sqlite en Rust"]
tags = ["rust", "sqlite", "system"]

[extra]
lang = "fr"
toc = true
math = true
mermaid = false
biscuit = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Réimplémenter sqlite en Rust : Partie 11" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/rustqlite-11.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Réimplémenter sqlite en Rust : Partie 11" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/rustqlite-11.png" },
    { property = "og:url", content="https://lafor.ge/rustqlite-11" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]

+++


{% detail(title="Les articles de la série") %}
{{ summary(path="content/toc/rustqlite.toml") }}
{% end %}

Bonjour à toutes et tous 😃

L'introduction du concept de [clef primaire](/rustqlite-10) permet de récupérer une entrée dans la base de données sans devoir scanner tous les éléments un par un jusqu'à
trouver le bon.

C'est pas mal du tout, mais un peu limité.

Généralement les requêtes sont un peu plus complexes et utiles.

Si l'on reprend la métaphore de notre annuaire téléphonique, c'est comme si pour trouver quelqu'un, vous n'avez que deux choix: soit vous chercher dans toutes les pages à la
recherche du bon nom soit vous connaissez déjà le numéro de page et c'est nettement plus simple. Ça c'est le fonctionnement de l'index primaire et du full-scan.

Maintenant, imaginons que l'on veuille récupérer les numéros de tous les hommes habitant dans la ville&nbsp;"X".

Dans la vraie vie, pour ceux qui n'ont jamais vu un, si si ça doit exister maintenant 😅. Les entrées sont décomposé en section pour chaque ville et trié par ordre alphabétique.

Nous pouvons modéliser notre annuaire au travers d'une table `PhoneBook`.

```sql
CREATE TABLE PhoneBook(name TEXT(50) PRIMARY KEY, city TEXT(50), phone_number TEXT(50), gender TEXT(1));
```

Notre requête dans l'annuaire ne peut pas être par clef primaire.

Il faut connaître les noms des personnes pour les trouver.

```sql
SELECT * FROM PhoneBook WHERE name='Alexandre Adams'
SELECT * FROM PhoneBook WHERE name='Amandine Agostini'
SELECT * FROM PhoneBook WHERE name='Arthur Almeida'
SELECT * FROM PhoneBook WHERE name='Anaïs Aubert'
...
```

Ce n'est pas très pratique... 😑

La vraie requête que l'on veut c'est:

```sql
SELECT * FROM PhoneBook WHERE city = 'X' AND gender = 'H';
```

Cette partie de la requête, nous allons l'appeler "expression".

```sql
city = 'X' AND gender = 'H'
```

Je vous propose de mettre en place le parse de cette expression dans cette nouvelle partie de notre épopée.

## Grammaire

Avant de partir en bataille avec notre parser, nous allons en définir les contours.

{%detail(title="Expression logique")%}
```
Expression ::= ColumnExpression | Expression LogicalOperator Expression
ColumnExpression ::= Column BinaryOperator LiteralValue
LogicalOperator ::= 'AND' | 'OR'
BinaryOperator ::= '=' | '!=' | '>' | '>=' | '<' | '<='
Column ::= [A-Za-z_]+
LiteralValue ::= LiteralString | LiteralInteger
LiteralString ::=  "'" [^']+ "'"
LiteralInteger ::= [0-9]+
```

<p style="font-size: 14px; font-weight:bold"><a name="Experession">Expression:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22439%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%22136%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%22136%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2221%22%3EColumnExpression%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2290%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2290%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3EExpression%3C%2Ftext%3E%3Crect%20x%3D%22161%22%20y%3D%2247%22%20width%3D%22120%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22159%22%20y%3D%2245%22%20width%3D%22120%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22169%22%20y%3D%2265%22%3ELogicalOperator%3C%2Ftext%3E%3Crect%20x%3D%22301%22%20y%3D%2247%22%20width%3D%2290%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22299%22%20y%3D%2245%22%20width%3D%2290%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22309%22%20y%3D%2265%22%3EExpression%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m136%200%20h10%20m0%200%20h204%20m-380%200%20h20%20m360%200%20h20%20m-400%200%20q10%200%2010%2010%20m380%200%20q0%20-10%2010%20-10%20m-390%2010%20v24%20m380%200%20v-24%20m-380%2024%20q0%2010%2010%2010%20m360%200%20q10%200%2010%20-10%20m-370%2010%20h10%20m90%200%20h10%20m0%200%20h10%20m120%200%20h10%20m0%200%20h10%20m90%200%20h10%20m23%20-44%20h-3%22%2F%3E%3Cpolygon%20points%3D%22429%2017%20437%2013%20437%2021%22%2F%3E%3Cpolygon%20points%3D%22429%2017%20421%2013%20421%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="439" usemap="#Experession.map"><map name="Experession.map"><area shape="rect" coords="49,1,185,33" href="#ColumnExpression" title="ColumnExpression"><area shape="rect" coords="49,45,139,77" href="#Expression" title="Expression"><area shape="rect" coords="159,45,279,77" href="#LogicalOperator" title="LogicalOperator"><area shape="rect" coords="299,45,389,77" href="#Expression" title="Expression"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="ColumnExpression">ColumnExpression:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22377%22%20height%3D%2237%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%223%22%20width%3D%2268%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%221%22%20width%3D%2268%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2239%22%20y%3D%2221%22%3EColumn%3C%2Ftext%3E%3Crect%20x%3D%22119%22%20y%3D%223%22%20width%3D%22116%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22117%22%20y%3D%221%22%20width%3D%22116%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22127%22%20y%3D%2221%22%3EBinaryOperator%3C%2Ftext%3E%3Crect%20x%3D%22255%22%20y%3D%223%22%20width%3D%2294%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22253%22%20y%3D%221%22%20width%3D%2294%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22263%22%20y%3D%2221%22%3ELiteralValue%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m0%200%20h10%20m68%200%20h10%20m0%200%20h10%20m116%200%20h10%20m0%200%20h10%20m94%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22367%2017%20375%2013%20375%2021%22%2F%3E%3Cpolygon%20points%3D%22367%2017%20359%2013%20359%2021%22%2F%3E%3C%2Fsvg%3E" height="37" width="377" usemap="#ColumnExpression.map"><map name="ColumnExpression.map"><area shape="rect" coords="29,1,97,33" href="#Column" title="Column"><area shape="rect" coords="117,1,233,33" href="#BinaryOperator" title="BinaryOperator"><area shape="rect" coords="253,1,347,33" href="#LiteralValue" title="LiteralValue"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LogicalOperator">LogicalOperator:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22147%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2248%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2248%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3EAND%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2240%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2240%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2265%22%3EOR%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m48%200%20h10%20m-88%200%20h20%20m68%200%20h20%20m-108%200%20q10%200%2010%2010%20m88%200%20q0%20-10%2010%20-10%20m-98%2010%20v24%20m88%200%20v-24%20m-88%2024%20q0%2010%2010%2010%20m68%200%20q10%200%2010%20-10%20m-78%2010%20h10%20m40%200%20h10%20m0%200%20h8%20m23%20-44%20h-3%22%2F%3E%3Cpolygon%20points%3D%22137%2017%20145%2013%20145%2021%22%2F%3E%3Cpolygon%20points%3D%22137%2017%20129%2013%20129%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="147"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="BinaryOperator">BinaryOperator:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22139%22%20height%3D%22257%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2230%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2230%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E%3D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2234%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2234%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2265%22%3E%21%3D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2291%22%20width%3D%2230%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2289%22%20width%3D%2230%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22109%22%3E%26gt%3B%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22135%22%20width%3D%2240%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22133%22%20width%3D%2240%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22153%22%3E%26gt%3B%3D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22179%22%20width%3D%2230%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22177%22%20width%3D%2230%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22197%22%3E%26lt%3B%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%22223%22%20width%3D%2240%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%22221%22%20width%3D%2240%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%22241%22%3E%26lt%3B%3D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m30%200%20h10%20m0%200%20h10%20m-80%200%20h20%20m60%200%20h20%20m-100%200%20q10%200%2010%2010%20m80%200%20q0%20-10%2010%20-10%20m-90%2010%20v24%20m80%200%20v-24%20m-80%2024%20q0%2010%2010%2010%20m60%200%20q10%200%2010%20-10%20m-70%2010%20h10%20m34%200%20h10%20m0%200%20h6%20m-70%20-10%20v20%20m80%200%20v-20%20m-80%2020%20v24%20m80%200%20v-24%20m-80%2024%20q0%2010%2010%2010%20m60%200%20q10%200%2010%20-10%20m-70%2010%20h10%20m30%200%20h10%20m0%200%20h10%20m-70%20-10%20v20%20m80%200%20v-20%20m-80%2020%20v24%20m80%200%20v-24%20m-80%2024%20q0%2010%2010%2010%20m60%200%20q10%200%2010%20-10%20m-70%2010%20h10%20m40%200%20h10%20m-70%20-10%20v20%20m80%200%20v-20%20m-80%2020%20v24%20m80%200%20v-24%20m-80%2024%20q0%2010%2010%2010%20m60%200%20q10%200%2010%20-10%20m-70%2010%20h10%20m30%200%20h10%20m0%200%20h10%20m-70%20-10%20v20%20m80%200%20v-20%20m-80%2020%20v24%20m80%200%20v-24%20m-80%2024%20q0%2010%2010%2010%20m60%200%20q10%200%2010%20-10%20m-70%2010%20h10%20m40%200%20h10%20m23%20-220%20h-3%22%2F%3E%3Cpolygon%20points%3D%22129%2017%20137%2013%20137%2021%22%2F%3E%3Cpolygon%20points%3D%22129%2017%20121%2013%20121%2021%22%2F%3E%3C%2Fsvg%3E" height="257" width="139"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="Column">Column:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22201%22%20height%3D%22141%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2271%2035%2078%2019%20126%2019%20133%2035%20126%2051%2078%2051%22%2F%3E%3Cpolygon%20points%3D%2269%2033%2076%2017%20124%2017%20131%2033%20124%2049%2076%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2284%22%20y%3D%2237%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2271%2079%2078%2063%20124%2063%20131%2079%20124%2095%2078%2095%22%2F%3E%3Cpolygon%20points%3D%2269%2077%2076%2061%20122%2061%20129%2077%20122%2093%2076%2093%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2284%22%20y%3D%2281%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Crect%20x%3D%2271%22%20y%3D%22107%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2269%22%20y%3D%22105%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2279%22%20y%3D%22125%22%3E_%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m40%200%20h10%20m62%200%20h10%20m-102%200%20h20%20m82%200%20h20%20m-122%200%20q10%200%2010%2010%20m102%200%20q0%20-10%2010%20-10%20m-112%2010%20v24%20m102%200%20v-24%20m-102%2024%20q0%2010%2010%2010%20m82%200%20q10%200%2010%20-10%20m-92%2010%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%20-10%20v20%20m102%200%20v-20%20m-102%2020%20v24%20m102%200%20v-24%20m-102%2024%20q0%2010%2010%2010%20m82%200%20q10%200%2010%20-10%20m-92%2010%20h10%20m28%200%20h10%20m0%200%20h34%20m-122%20-88%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m122%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-122%200%20h10%20m0%200%20h112%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22191%2033%20199%2029%20199%2037%22%2F%3E%3Cpolygon%20points%3D%22191%2033%20183%2029%20183%2037%22%2F%3E%3C%2Fsvg%3E" height="141" width="201"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LiteralValue">LiteralValue:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22205%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2221%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%22106%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%22106%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3ELiteralInteger%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m96%200%20h10%20m0%200%20h10%20m-146%200%20h20%20m126%200%20h20%20m-166%200%20q10%200%2010%2010%20m146%200%20q0%20-10%2010%20-10%20m-156%2010%20v24%20m146%200%20v-24%20m-146%2024%20q0%2010%2010%2010%20m126%200%20q10%200%2010%20-10%20m-136%2010%20h10%20m106%200%20h10%20m23%20-44%20h-3%22%2F%3E%3Cpolygon%20points%3D%22195%2017%20203%2013%20203%2021%22%2F%3E%3Cpolygon%20points%3D%22195%2017%20187%2013%20187%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="205" usemap="#LiteralValue.map"><map name="LiteralValue.map"><area shape="rect" coords="49,1,145,33" href="#LiteralString" title="LiteralString"><area shape="rect" coords="49,45,155,77" href="#LiteralInteger" title="LiteralInteger"></map><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22241%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2219%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2217%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2237%22%3E%27%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%2035%20102%2019%20142%2019%20149%2035%20142%2051%20102%2051%22%2F%3E%3Cpolygon%20points%3D%2293%2033%20100%2017%20140%2017%20147%2033%20140%2049%20100%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%2237%22%3E%5B%5E%27%5D%3C%2Ftext%3E%3Crect%20x%3D%22189%22%20y%3D%2219%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22187%22%20y%3D%2217%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22197%22%20y%3D%2237%22%3E%27%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m0%200%20h10%20m24%200%20h10%20m20%200%20h10%20m54%200%20h10%20m-94%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m74%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-74%200%20h10%20m0%200%20h64%20m20%2032%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22231%2033%20239%2029%20239%2037%22%2F%3E%3Cpolygon%20points%3D%22231%2033%20223%2029%20223%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="241"><br>
      <p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161"><br>

{%end%}

Notre grammaire se complexifie. Nous avons tout une série de nouveaux tokens à rajouter.

- `AND`
- `OR`
- `!=`
- `=`
- `>`
- `<`
- `>=`
- `<=`

Nous avons introduit également le concept d'expression de colonne `ColumnExpression`.

On associe un identifiant de colonne et une valeur comparé par un opérateur binaire `BinaryOperator`.

```sql
id > 12
name != 'Max'
```

Mais nous avons également la possibilité de d'associer deux `ColumnExpression` via un `LogicalOperator`.

Et on peut le faire autant que l'on veut.

```sql
id > 12 AND name != 'Max' OR data = 'true'
```

## Ajout des tokens

On rajoute nos différents tokens

```rust
enum Operator {
    /// AND token
    And,
    /// OR token
    Or,
    /// > token
    GreaterThan,
    /// < token
    LessThan,
    /// >= token
    GreaterThanOrEqual,
    /// <= token
    LessThanOrEqual,
    /// != token
    Different,
    /// = token
    Equal,
}


enum Token {
    // ...
    Operator(Operator),
    // ...
}
```

On y associe un Matcher

```rust
impl Match for Operator {
    fn matcher(&self) -> Matcher {
        match self {
            Operator::And => Box::new(matchers::match_pattern("and")),
            Operator::Or => Box::new(matchers::match_pattern("or")),
            Operator::GreaterThan => Box::new(matchers::match_pattern(">")),
            Operator::LessThan => Box::new(matchers::match_pattern("<")),
            Operator::GreaterThanOrEqual => Box::new(matchers::match_pattern(">=")),
            Operator::LessThanOrEqual => Box::new(matchers::match_pattern("<=")),
            Operator::Different => Box::new(matchers::match_pattern("!=")),
            Operator::Equal => Box::new(matchers::match_pattern("=")),
        }
    }
}

impl Match for Token {
    fn matcher(&self) -> Matcher {
        match self {
            // ...
            Token::Operator(operator) => operator.matcher(),
            // ...
        }
    }
}
```

Ainsi que la taille des tokens

```rust
impl Size for Operator {
    fn size(&self) -> usize {
        match self {
            Operator::And => 3,
            Operator::Or => 2,
            Operator::GreaterThan => 1,
            Operator::LessThan => 1,
            Operator::GreaterThanOrEqual => 2,
            Operator::LessThanOrEqual => 2,
            Operator::Different => 2,
            Operator::Equal => 1,
        }
    }
}


impl Size for Token {
    fn size(&self) -> usize {
        match self {
            // ...
            Token::Operator(operator) => operator.size(),
            // ...
        }
    }
}
```

## Parsing de l'expression

Comme la grammaire le laisse supposer, parser une Expression est plus complexe que parser les embryon de commandes que nous avons déjà dans notre arsenal.

La principal difficulté étant qu'une Expression est récursivement une Expression.

Mais avant de nous attaquer au plat de resistance et de l'avoir sur l'estomac par indigestion. Nous allons décomposer le problèmes en sous-problèmes et recomposer
le puzzle.

### Recognizer

On avait déjà un Acceptor qui permettait d'avoir des alternatives de visiteurs, le Forecaster qui donnait le choix de forecast de groupe de token dans le futur.

Il nous manquait la possiblité de choisir une collection de tokens à reconnaître.

Avec le `Recognizer` c'est chose faite !

```rust
impl<'a, 'b, T, U> Recognizer<'a, 'b, T, U> {
    pub fn new(scanner: &'b mut Scanner<'a, T>) -> Self {
        Recognizer {
            data: None,
            scanner,
        }
    }

    pub fn try_or<R: Recognizable<'a, T, U>>(
        mut self,
        element: R,
    ) -> Result<Recognizer<'a, 'b, T, U>> {
        // Propagate result
        if self.data.is_some() {
            return Ok(self);
        }
        // Or apply current recognizer
        if let Some(found) = element.recognize(self.scanner)? {
            self.data = Some(found);
        }
        Ok(self)
    }

    pub fn finish(self) -> Option<U> {
        self.data
    }
}
```

Même principe que pour les deux autres, on lui fourni une liste de choses à reconnaître et il tente de reconnaître le token, s'il n'y arrive pas, il passe au suivant, sinon il s'arrête et propage le token reconnu.

En sorti de méthode `finish` on obtient alors une Option du token.

### Binary Operator

La première chose que l'on va vouloir reconnaître c'est nos opérateurs binaires. 

- `!=`
- `=`
- `>`
- `<`
- `>=`
- `<=`

Nous avons besoin d'une énumération pour modéliser ces opérateurs

```rust
enum BinaryOperator {
    GreaterThanOrEqual,
    LessThanOrEqual,
    GreaterThan,
    LessThan,
    Equal,
    Different,
}
```

Ainsi que d'un TryFrom qui va permettre de passer du token reconnu à l'opérateur.

```rust
impl TryFrom<Token> for BinaryOperator {
    type Error = ParseError;
    fn try_from(value: Token) -> Result<Self, Self::Error> {
        match value {
            Token::Operator(Operator::GreaterThanOrEqual) => Ok(BinaryOperator::GreaterThanOrEqual),
            Token::Operator(Operator::LessThanOrEqual) => Ok(BinaryOperator::LessThanOrEqual),
            Token::Operator(Operator::GreaterThan) => Ok(BinaryOperator::GreaterThan),
            Token::Operator(Operator::LessThan) => Ok(BinaryOperator::LessThan),
            Token::Operator(Operator::Equal) => Ok(BinaryOperator::Equal),
            Token::Operator(Operator::Different) => Ok(BinaryOperator::Different),
            _ => Err(ParseError::UnexpectedToken),
        }
    }
}
```

On peut alors reconnaître notre opérateur.

```rust
#[test]
fn test_binary_operator() {
    let data = b"> 12";
    let mut scanner = Scanner::new(data);
    let result: BinaryOperator = Recognizer::new(&mut scanner)
        // reconnaissance de l'opérateur 👇
        .try_or(Token::Operator(Operator::GreaterThan))
        .expect("Unable to parse")
        .finish()
        .expect("No token recognize")
        .element
        .try_into()
        .expect("Unable to convert");
    assert_eq!(result, BinaryOperator::GreaterThan);
}
```

### ColumnExpression

On peut désormais représenter notre ColumnExpression

```rust
struct ColumnExpression {
    column: Column,
    operator: BinaryOperator,
    value: Value,
}

impl ColumnExpression {
    pub fn new(column: Column, operator: BinaryOperator, value: Value) -> Self {
        Self {
            column,
            operator,
            value,
        }
    }
}
```

Que l'on peut alors visiter.

```rust
impl<'a> Visitable<'a, u8> for ColumnExpression {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // reconnaissance de l'identifiant de colonne
        let column = Column::accept(scanner)?;
        // nettoyage d'eventuel d'espace blanc
        OptionalWhitespaces::accept(scanner)?;

        // reconnaissance de l'opérateur binaire
        let operator = Recognizer::new(scanner)
            .try_or(Token::Operator(Operator::GreaterThanOrEqual))?
            .try_or(Token::Operator(Operator::GreaterThan))?
            .try_or(Token::Operator(Operator::LessThanOrEqual))?
            .try_or(Token::Operator(Operator::LessThan))?
            .try_or(Token::Operator(Operator::Different))?
            .try_or(Token::Operator(Operator::Equal))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)?
            .element
            .try_into()?;

        // nettoyage d'eventuel d'espace blanc
        OptionalWhitespaces::accept(scanner)?;
        // reconnaissance de la valeur
        let value = Value::accept(scanner)?;
        Ok(ColumnExpression::new(column, operator, value))
    }
}
```

Ce qui donne:

```rust
#[test]
fn test_column_expression() {
    let data = b"id = 12";
    let mut scanner = Scanner::new(data);
    let result = scanner.visit();
    assert_eq!(
        result,
        Ok(ColumnExpression {
            column: Column("id".to_string()),
            operator: BinaryOperator::Equal,
            value: Value::Integer(12)
        })
    );
}
```

Parfait! On peut maintenant parser une ColumnExpression 🤩

### Opérateur logique

On fait de même pour les tokens:
- AND
- OR

```rust
enum LogicalOperator {
    Or,
    And,
}

impl TryFrom<Token> for LogicalOperator {
    type Error = ParseError;

    fn try_from(value: Token) -> Result<Self, Self::Error> {
        match value {
            Token::Operator(Operator::And) => Ok(LogicalOperator::And),
            Token::Operator(Operator::Or) => Ok(LogicalOperator::Or),
            _ => Err(ParseError::UnexpectedToken),
        }
    }
}
```

Permettant de reconnaître les opérateur logique.

```rust
#[test]
fn test_logical_operator() {
    let data = b"AND toto";
    let mut scanner = Scanner::new(data);
    let result: LogicalOperator = Recognizer::new(&mut scanner)
        .try_or(Token::Operator(Operator::And))
        .expect("Unable to parse")
        .finish()
        .expect("No token recognize")
        .element
        .try_into()
        .expect("Unable to convert");
    assert_eq!(result, LogicalOperator::And);
}
```

### Expression

Notre expression est une suite de ColumnExpression séparé par de LogicalOperator.

Lorsque l'on a une expression du genre:

```sql
id > 12 AND name = 'toto'
```

Cela donne une expression du genre

```
ColumnExpression LogicalOperator ColumnExpression
```

Mais si on a quelque chose commen

```sql
id > 12 AND name = 'toto' AND gender = 'M'
```

Cela donne l'expression suivante

```
ColumnExpression LogicalOperator ColumnExpression LogicalOperator ColumnExpression
```

Sauf que l'opérateur logique n'a que deux paramètres:
- un à gauche `lhs`
- un à droite `rhs`

Si on redécoupe

```
ColumnExpression LogicalOperator [ ColumnExpression LogicalOperator ColumnExpression ]
```

On réduit encore

```
ColumnExpression LogicalOperator LogicalExpression
```

Et si l'on veut que le système soit totalement récursif, notre expression doit se réduire en

```
LogicalExpression
```

On en déduit deux choses.

Nous avons besoin d'une énumération qui permette de faire une union entre la ColumnExpression et la LogicalExpression

```rust
enum Expression {
    Logical(LogicalExpression),
    Column(ColumnExpression),
}
```

On la visite

```rust
impl<'a> Visitable<'a, u8> for Expression {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        Acceptor::new(scanner)
            .try_or(|value| Ok(Expression::Logical(value)))?
            .try_or(|value| Ok(Expression::Column(value)))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)
    }
}
```

### LogicalExpression

On peut alors en définir le LogicalExpression

```rust
struct LogicalExpression {
    lhs: Box<Expression>,
    operator: LogicalOperator,
    rhs: Box<Expression>,
}
```

{%note()%}
Le boxing `Box<Expression>` est obligatoire car Expression peut contenir une LogicalExpression qui contient lui même une Expression, etc ...

Cela conduit à une taille infinie.

Ça la stack n'aime pas du tout, la heap n'a pas ce problème.
{%end%}

On en défini également un constructeur

```rust
impl LogicalExpression {
    fn new(lhs: Expression, operator: LogicalOperator, rhs: Expression) -> Self {
        Self {
            lhs: Box::new(lhs),
            operator,
            rhs: Box::new(rhs),
        }
    }
}
```

Et finalement un visiteur

```rust
impl<'a> Visitable<'a, u8> for LogicalExpression {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        // on forecast la fin d'un groupe d'expression car si l'on tente
        // de visiter une Expression sans avoir au préalable délimité
        // le contenu du scanner, nous allons éternellement 
        // parser le même morceau de LogicalExpression
        // ce qui fait exploser la stack !
        // Tout lhs fini nécessairement par AND ou OR
        let lhs_group = Forcaster::new(scanner)
            .try_or(UntilToken(Token::Operator(Operator::And)))?
            .try_or(UntilToken(Token::Operator(Operator::Or)))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)?;

        let mut lhs_scanner = Scanner::new(lhs_group.data);
        // on visite l'Expression du lhs
        let lhs = lhs_scanner.visit()?;
        // on avance le curseur du scanner principal
        scanner.bump_by(lhs_group.data.len());
        
        // on nettoie d'éventuel blancs
        scanner.visit::<OptionalWhitespaces>()?;

        // on reconnait l'opérateur logique
        let operator = Recognizer::new(scanner)
            .try_or(Token::Operator(Operator::And))?
            .try_or(Token::Operator(Operator::Or))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)?
            .element
            .try_into()?;
        // on reconnaît au moins un blanc après l'opérateur logique
        scanner.visit::<Whitespaces>()?;
        // on visite l'expression suivante
        let rhs = scanner.visit()?;
        Ok(LogicalExpression::new(lhs, operator, rhs))
    }
}
```

## Test de parse

On peut finalement parser nos Expressions

### Expression simple

D'abord une simple comparaison

```rust
#[test]
fn test_logical_expression_simple() {
    let data = b"id > 12";
    let mut scanner = Scanner::new(data);
    let result = scanner.visit();
    assert_eq!(
        result,
        Ok(Expression::Column(ColumnExpression::new(
            Column("id".to_string()),
            BinaryOperator::GreaterThan,
            Value::Integer(12),
        )))
    );
}
```

### Expression logique

Puis une utilisant un connecteur logique

```rust
#[test]
fn test_logical_expression_or() {
    let data = b"id <= 12 OR name != 'toto'";
    let mut scanner = Scanner::new(data);
    let result = scanner.visit();

    let c1 = ColumnExpression::new(
        Column("id".to_string()),
        BinaryOperator::LessThanOrEqual,
        Value::Integer(12),
    );

    let c2 = ColumnExpression::new(
        Column("name".to_string()),
        BinaryOperator::Different,
        Value::Text("toto".to_string()),
    );

    let l1 = LogicalExpression::new(
        Expression::Column(c1),
        LogicalOperator::Or,
        Expression::Column(c2),
    );

    assert_eq!(result, Ok(Expression::Logical(l1)))
}
```

### Expression logiques composites

Puis un chaînage de plusieurs expressions

```rust
#[test]
fn test_logical_expression_or_and() {
    let data = b"id <= 12 OR name != 'toto' AND gender = 'male'";
    let mut scanner = Scanner::new(data);
    let result = scanner.visit();

    let c1 = ColumnExpression::new(
        Column("id".to_string()),
        BinaryOperator::LessThanOrEqual,
        Value::Integer(12),
    );

    let c2 = ColumnExpression::new(
        Column("name".to_string()),
        BinaryOperator::Different,
        Value::Text("toto".to_string()),
    );

    let c3 = ColumnExpression::new(
        Column("gender".to_string()),
        BinaryOperator::Equal,
        Value::Text("male".to_string()),
    );

    let l1 = LogicalExpression::new(
        Expression::Column(c1),
        LogicalOperator::Or,
        Expression::Column(c2),
    );

    let l2 = LogicalExpression::new(
        Expression::Logical(l1),
        LogicalOperator::And,
        Expression::Column(c3),
    );

    assert_eq!(result, Ok(Expression::Logical(l2)))
}
```

Et voilà ! 🤩

Nous sommes capables de parser des expressions logiques SQL. 😍😍😍

Pas toutes biensûr, mais c'est un début.

En tout cas ça sera suffisant pour la suite de mes explications.

## Conclusion

Oui, c'était encore un épisode de parsing, mais que voulez vous, il faut bien des outils pour bâtir des choses, et le parsing est nécessaire pour nous ouvir
le champ des possibles. 😎

Dans la [prochaine partie](/rustqlite-12) nous utiliserons nos expressions pour requêter notre base données sur autre chose que la clef primaire.

Merci de votre lecture ❤️

Vous pouvez trouver le code la partie [ici](https://gitlab.com/blog_example/sqlite-en-rust/-/tree/11-expression) et le [diff](https://gitlab.com/blog_example/sqlite-en-rust/-/compare/10-primary-key...11-expression) là.
