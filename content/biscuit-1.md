+++
title = "En route vers Biscuit (Partie 1)"
date = 2022-08-25
draft = false

[taxonomies]
categories = ["Biscuit"]
tags = ["sécurité", "biscuit"]

[extra]
lang = "fr"
toc = true
show_comment = true
math = false
mermaid = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120
metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="En route vers Biscuit (Partie 1)" },
    { name = "twitter:image", content="https://i.ibb.co/tMNrvmQ/twitter.jpg" },
]
+++

Bonjour à toutes et tous 😀

Pour ceux qui suivent ce blog, vous savez que j’aime expliquer des trucs. 😅

<!-- more -->


J’ai découvert un nouveau jouet qui me permet d’aborder plein de sujets, comme la cryptographie asymétrique, les chaînes de confiance
et les langages logiques.

Aujourd’hui, je vous propose ainsi de découvrir un nouvel outil appelé **Biscuit**. 🥠

Il s’agit d’un token d’autorisation et d’authentification pouvant être atténué sans avoir besoin de secret partagé.

Si ce que j’ai écrit juste au-dessus est de l’hébreu pour vous, l’article qui va suivre va tenter de vous donner toutes les 
bases pour être à même de la comprendre.

Si vous voulez directement entrer dans le vif du sujet, un second article sera disponible, [ici](/biscuit-2).

Pour ceux/celles qui sont resté(e)s, je vous propose une rétrospective de la gestion des données utilisateur éphémères.

On parlera d’abord, du concept des sessions et on exposera leur limites.

Puis, nous analyserons les tokens d’authentification sans état.

Bon, c’est parti pour un très, très, long voyage. 😁

## Authentification avec état

Lorsqu’il est venu à l’idée de personnaliser le contenu des utilisateurs. Il a fallu être capable de
savoir qui était en train de visiter le site, pour pouvoir lui afficher le contenu qui lui correspond.

Une autre volonté était de permettre à l’utilisateur de posséder un compte sur le site. Cela permettant à l’utilisateur de
s’engager sur les contenus, et de pouvoir plus facilement le connaître, lui et ses habitudes. (Oui le côté Sombre de la Force est partout) 

Mais qui dit compte utilisateur, dit authentification. Qui dit authentification, dit mire de login.

Pour la première connexion de sa session de visite, il ne verra pas d’inconvénient à se connecter avec son mot de passe. Mais s’il doit le 
faire à chaque fois, cela risque d’être plutôt agaçant pour lui et nous risquons potentiellement de ne jamais le revoir.

C’est pour cette raison que deux concepts ont vu le jour, ceci travaillant de pair :

- les cookies
- les sessions

### Sessions

L’idée est simple pour savegarder les données d’un utilisateur, côté serveur, nous lui ouvrons une session. Il s’agit soit d’un emplacement dans la mémoire du serveur, soit d’un fichier qui va contenir toutes les informations nécessaires.

Par exemple lorsque l’utilisateur est bien connecté et qu’il n’est pas nécessaire de 
lui redemander son mot de passe d’une page à l’autre.

Nous pouvons représenter nos sessions sous la forme d’un tableau ayant pour clef les ID de session et comme valeur les données que l’on veut stocker dans la session. 

![schéma représentant des sessions sur le serveur](/assets/images/biscuit-1/session1.png)

Côté client, nous allons également sauvegarder des informations sur le navigateur de l’utilisateur, il s’agit d’un petit fichier qui se nomme un cookie.

<div align="center">
    <img src="/assets/images/biscuit-1/cookie1.png" alt="un cookie informatique" style="align-items: center;" width="25%"/>
</div>

Ce cookie peut contenir des informations.

<div align="center">
    <img src="/assets/images/biscuit-1/cookie2.png" alt="un cookie informatique avec des données" style="align-items: center;" width="50%"/>
</div>

&nbsp;

{% tip(header="Information supplémentaire") %}
Si vous voulez plus de détails sur ce concept je vous conseille la vidéo [d’Hubert Sablonnière](https://twitter.com/hsablonniere) sur le [sujet](https://www.youtube.com/watch?v=MKQ8gUGdKGs).
{% end %}

Si c’est la première visite, l’on demande à l’utilisateur de se connecter.

![session inexistante et cookie innexistant](/assets/images/biscuit-1/session2.png)

Si l’authentification est un succès. Nous créons une session sur le serveur et l’on vient générer un cookie avec l’ID de session nouvellement créé.

![création du cookie et de la session](/assets/images/biscuit-1/session3.png)

Ce cookie est alors transmis à l’utilisateur par retour de requête.

Lorsque l’utilisateur réalise une requête vers une ressource privilégiée sur le serveur, l’on vient lire l’ID de cette session, si celle-ci existe et est valide, on laisse passer l’utilisateur, sinon on lui demande de s’authentifier.

![requête avec cookie](/assets/images/biscuit-1/session4.png)

Ainsi, nous avons réglé le problème, merci au revoir ! 😛

Sauf que…

### Scissions de sessions

… Non ❌

Dans une infrastructure même vétuste, lorsque le trafic augmente, une seule machine ne suffit pas, on entre alors dans le monde merveilleux de l’équilibrage de charge. 🤩

Sur le papier cela semble simple, on met une machine qui sert d’agent de la circulation et qui répartit les requêtes sur 2 ou plus serveurs.

Cet agent de circulation est appelé un **load balancer**.

![load balancer](/assets/images/biscuit-1/lb.png)

C’est à ce moment précis que les choses se gâtent. Rappelez-vous les sessions sont des fichiers sur le serveur ou des cases dans sa mémoire. Ce qui signifie que 
ce qui se passe sur le serveur 1 est totalement inconnu au serveur 2 et vice-versa.

Lorsque la première requête arrive, pas de problème. On est dans l’état précédent, la requête va être routée vers le serveur 1 par exemple, une session va être 
créée et un cookie déposé.

![load balancer redirige vers serveur 1](/assets/images/biscuit-1/lb2.png)

Et maintenant à 50/50, la prochaine requête arrivera soit sur le serveur 1, soit sur le serveur 2.

Si c’est le serveur 1, pas de problème, l’utilisateur précédemment connecté, le reste.

Par contre, si c’est le serveur 2 qui reçoit la requête, il va lire le cookie, voir que la session n’existe pas. Demander à l’utilisateur de se connecter. 

![load balancer redirige vers serveur 2](/assets/images/biscuit-1/lb3.png)

Si celui-ci s’exécute, une nouvelle session est construite sur le serveur 2, et le cookie est réécrit avec le numéro de session sur le serveur 2.

![session créé sur le serveur 2](/assets/images/biscuit-1/lb4.png)

La situation est désormais inverse. Si la requête tombe sur le serveur 1, celui-ci déconnectera l’utilisateur et ainsi de suite…

![session créé sur le serveur 2](/assets/images/biscuit-1/lb5.png)

L’utilisateur a à présent 50% de chance de se faire déconnecter à chaque requête ! 😫

Bon il faut faire quelque chose, on ne peut pas laisser la situation ainsi.

Pour cela nous allons partager les sessions.

### Sessions partagées

L’idée est de remplacer des sessions stockées localement par des sessions sur le réseau.

Cela peut être, soit via un montage NFS (ce n’est pas bien, ne faites pas ça 😛) soit via une base de données qui va contenir nos différentes sessions.

La base la plus utilisée pour ce genre d’usage est Redis, mais il en existe plein d’autres.

Les différents serveurs sont alors connectés à cette base de données et l’interrogent pour récupérer la session indexée par l’ID contenu dans le cookie de requête.

![sessions partagées](/assets/images/biscuit-1/shared.png)

Et maintenant grâce à ces sessions déportées, on peut augmenter ou diminuer le nombre de serveurs, sans jamais risquer de déconnecter un utilisateur. 😎

{% tip (header="En Bonus") %}
Redis est fait pour travailler en cluster, ce qui signifie que si votre nombre de serveurs explose, vous pouvez rajouter des nœuds sur votre cluster Redis
pour accueillir la charge supplémentaire de requêtes de sessions.
{% end %}

Le souci de cela, c’est la complexité de l’infrastructure, on a une collection de frontaux, une collection de serveurs et un cluster de sessions.

Ce serait intéressant de pouvoir de se passer d’état tout court. C’est ce que l’on va voir tout de suite. 😀

## Authentification sans état

Si vous vous rappelez, les cookies sont des fichiers locaux au navigateur de l’utilisateur et générés par le serveur.

Pourquoi ne pas utiliser ce concept pour y stocker la session de l’utilisateur ? 😺

### Cookie

Reprenons notre situation de tout à l’heure, l’utilisateur se connecte à un serveur, le serveur vérifie les cookies qui lui sont fournis.

S’il n’y a pas de cookies ou que le cookie n’a pas d’information valide. Le serveur génère un cookie.

Au lieu d’y stocker seulement l’ID de la session, nous allons y coller toute la session précédemment stockée soit sur le serveur soit sur la BDD de sessions.


<div align="center">
    <img src="/assets/images/biscuit-1/cookie3.png" alt="un cookie avec des données de session" style="align-items: center;" width="50%"/>
</div>

Lorsque l’utilisateur revient avec son cookie, le serveur vient lire le cookie et y récupère la session de l’utilisateur.

Ici notre session définit les droits de l’utilisateur, ainsi que son user ID.

<div align="center">
    <img src="/assets/images/biscuit-1/cookie4.png" alt="cas d'une connexion légitime" style="align-items: center;" width="100%"/>
</div>

Comme la session est stockée dans le cookie et non sur un serveur en particulier, le cookie peut alors être traité par n’importe quel serveur.

On en revient donc à la situation de la session partagée. 😀

Sauf que là, encore… Non ❌

Je vous présente Jaba, Jaba est un pirate, son plaisir dans la vie, c’est de devenir administrateur sur une plateforme.

<div align="center">
    <img src="/assets/images/biscuit-1/pirate.png" alt="un pirate voulant devenir admin" style="align-items: center;" width="25%"/>
</div>

Les cookies sont, je le rappelle, des fichiers locaux au navigateur, Jaba n’a pas besoin d’avoir accès au serveur.

Et il ne va pas se priver de le faire. 😈

<div align="center">
    <img src="/assets/images/biscuit-1/cookie5.png" alt="cas d'une connexion illégitime" style="align-items: center;" width="100%"/>
</div>

Le voici administrateur !! 😭

Nous allons devoir mettre des bâtons dans les roues de Jaba !

### Signatures Numériques

Pour cela nous allons introduire un concept supplémentaire, celui-ci se nomme la signature numérique. Elle consiste à valider les données qui transitent. En clair, seul
le serveur doit avoir la capacité de le faire.

Il existe deux manières de signer des données

- la signature par secret
- la signature par clef asymétrique

#### Signature par secret

Voici un secret, il s’agit d’un grand nombre qui doit comme son nom l’indique, doit rester… eh bien… secret. 🔐

<div align="center">
    <img src="/assets/images/biscuit-1/secret.png" alt="un secret" style="align-items: center;" width="25%"/>
</div>

On peut utiliser ce secret pour venir signer nos données.

<div align="center">
    <img src="/assets/images/biscuit-1/signature1.png" alt="signature par secret" style="align-items: center;" width="100%"/>
</div>

Puis lorsque l’on a le besoin, nous pouvons vérifier avec ce même secret la validité des données.

<div align="center">
    <img src="/assets/images/biscuit-1/signature2.png" alt="signature validée" style="align-items: center;" width="100%"/>
</div>

Si les données sont altérées, alors la signature ne correspondra pas et la vérification lèvera une erreur.

<div align="center">
    <img src="/assets/images/biscuit-1/signature3.png" alt="signature invalide car données altérée" style="align-items: center;" width="100%"/>
</div>

Mais, si la signature et les données sont modifiables par l’utilisateur et qu’il fournit une signature compatible avec les données

Comment fait-on pour être certain que les données ne sont pas altérées ?

Si la signature ne correspond pas avec le secret, la punition sera la même.

<div align="center">
    <img src="/assets/images/biscuit-1/signature4.png" alt="signature invalide" style="align-items: center;" width="100%"/>
</div>

Ces deux mécanismes imposent que seul le serveur est à même de pouvoir modifier les données, ou en tout cas si celle-ci ou la signature est altérée. Le serveur
s’en rendra compte tout de suite.

{% detail(title="Code python") %}
```python
import hashlib, hmac, binascii

def hmac_sha256(secret, message):
  return binascii.hexlify(hmac.new(secret, message, hashlib.sha256).digest())

def sign_message(secret, message):
    signature = hmac_sha256(secret, message)
    return {'message': message, 'signature': signature}

def verify(message, secret):
    computed_signature = hmac_sha256(secret, message['message'])
    if hmac_sha256(secret, computed_signature) == hmac_sha256(secret, message['signature']):
        print("La signature est valide")
    else:
        print("La signature n'est pas valide")

# On génère des secret
secret = b"12345"
pirate_secret = b"6789"

# On génère des messages à signer
message = b"Mon message"
pirate_message = bytes("Message altéré", encoding="utf-8")

# La signature corespond au message
genuine_message = sign_message(secret, message)
# On altère le message
altered_message = genuine_message.copy()
altered_message['message'] = pirate_message
# On altère la signature
altered_signature = sign_message(pirate_secret, pirate_message)

verify(genuine_message, secret)
verify(altered_message, secret)
verify(altered_signature, secret)
```
{% end %}

Le code est disponible [ici](https://replit.com/@Akanoa/hmac#main.py).

Celui-ci renvoie.

```
La signature est valide
La signature n'est pas valide
La signature n'est pas valide
```

On voit bien ici que seul une correspondance du triplet (secret, données non falsifiées, signature valide), permet au système de valider les données reçues.

Le souci est que pour vérifier, il faut obligatoirement partager le secret.

C’est pour cette raison qu’un autre type de signature a été mis en place, il s’agit des clefs asymétriques.

#### Signature par clefs asymétriques

Même principe ici nous allons générer un secret. Celui-ci va porter un nom différent, il s’agit d’une **clef privée**.

<div align="center">
    <img src="/assets/images/biscuit-1/private.png" alt="clef privée" style="align-items: center;" width="50%"/>
</div>

Contrairement au secret qui était relativement libre de choix, la clef privée respecte des règles mathématiques.

Elle est donc généralement créée par un algorithme.

Cette clef privée a la capacité de générer un pendant, celui-ci se nomme **clef publique**.

<div align="center">
    <img src="/assets/images/biscuit-1/signature9.png" alt="dérivation de la clef publique" style="align-items: center;" width="100%"/>
</div>

Les mathématiques nous certifient (jusqu’à preuve du contraire) qu’il est impossible de retrouver la 
clef privée en connaissant la clef publique.

Le couple de clefs privée/publique est appelée un *Keypair* en cryptographie, j’utiliserai en conséquence ce terme dans la suite.

<div align="center">
    <img src="/assets/images/biscuit-1/keypair.png" alt="une paire de clef asymétrique" style="align-items: center;" width="50%"/>
</div>

Le mécanisme de signature reste le même que dans le cadre d’un secret. Nous utilisons la clef privée pour signer nos données.

<div align="center">
    <img src="/assets/images/biscuit-1/signature5.png" alt="signature asymétrique" style="align-items: center;" width="100%"/>
</div>

Par contre, la vérification est réalisée via la clef publique et non la clef privée.

<div align="center">
    <img src="/assets/images/biscuit-1/signature6.png" alt="vérification réussite" style="align-items: center;" width="100%"/>
</div>

Ici de même, si les données sont altérées

<div align="center">
    <img src="/assets/images/biscuit-1/signature7.png" alt="vérification échouées car les données sont altérées" style="align-items: center;" width="100%"/>
</div>

La vérification échouera.

Ainsi que lorsque la signature n’a pas été générée par la clef privée d’origine.

<div align="center">
    <img src="/assets/images/biscuit-1/signature8.png" alt="vérification échouées car la signature est altérée" style="align-items: center;" width="100%"/>
</div>

Voici un petit morceau de python pour vous expliquer le fonctionnement.

On installe, la dépendance :

```
pip install ed25519
```
{% detail(title="Code python") %}
```python
import ed25519

def verify(public_key, signature, message):
  try:
    public_key.verify(signature, message, encoding='hex')
    print("La signature est valide.")
  except:
    print("La signature a été altérée")

# On génère la paire de clef
private_key, public_key = ed25519.create_keypair()
print("Clef privée originelle:", private_key.to_ascii(encoding='hex'))
print("Clef publique originelle", public_key.to_ascii(encoding='hex'))

## On génère également une paire de clef pirates
pirate_private_key, pirate_public_key = ed25519.create_keypair()
print("Clef privée pirate:", privKey.to_ascii(encoding='hex'))
print("Clef publique pirate", pubKey.to_ascii(encoding='hex'))

## On signe un message avec la clef privée originel 
message = b'Voici mon message'
signature = private_key.sign(message, encoding='hex')
print("Signature :", signature)

## On fait de même mais avec une clef privée différente 
pirate_message = b'Message altéré'
pirate_signature = pirate_private_key.sign(pirate_message, encoding='hex')
print("Signature pirate :", pirate_signature)

### Cas nominal
verify(public_key, signature, message)

### Les données ont été altérées
verify(public_key, signature, pirate_message)

### Les données et la signature ont été altérées
verify(public_key, pirate_signature, pirate_message)
```
{% end %}

Le code [ici](https://replit.com/@Akanoa/ed25519#main.py).

```
La signature est valide.
La signature a été altérée.
La signature a été altérée.
```

On voit ici encore une fois que seul une correspondance du triplet (clef publique, données non falsifiées, signature valide), permet au système de valider les données reçues.


#### Exemple d’utilisation

Prenons un exemple un peu plus concret qui va démontrer la plus grande flexibilité des clefs asymétriques par rapport aux secrets.

Imaginons que vous ayez une `Entreprise A`, celle-ci possède les informations des utilisateurs, ainsi que leurs identifiants de connexion.

Une `Entreprise B` veut pouvoir accepter les utilisateurs de l’`Entreprise A`, les utilisateurs n’ont aucune envie de créer un compte dans l’`Entreprise B`.

L’`Entreprise B` a confiance dans les tokens qui sont délivrés par l’`Entreprise A`. 

Elle souhaite pouvoir les vérifier. 

On ne voudrait pas qu’un Jaba maléfique falsifie le token, si ?… 😈

Par contre l’`Entreprise A` n’a aucune confiance en l’`Entreprise B`, en tout cas pas suffisamment pour lui laisser signer des tokens en se faisant passer pour l’`Entreprise A`.

Avec un secret ce ne serait pas possible, mais avec une paire asymétrique, ce n’est pas un problème.

L’`Entreprise A` peut en toute confiance partager sa clef publique avec l’`Entreprise B`, celle-ci ne pourra que vérifier des signatures avec.

Les choses se passent ainsi :

1. L’utilisateur s’identifie sur `Entreprise A`
2. `Entreprise A` vérifie les identifiants
3. `Entreprise A` génère un token qu’il signe avec sa clef privée
4. L’utilisateur reçoit le token signé de l’`Entreprise A`.
5. L’utilisateur transmet le token à l’`Entreprise B`
6. L’`Entreprise B` vérifie le token avec la clef publique de l’`Entreprise A`
7. Si le token est valide, l’utilisateur est connecté sur l’`Entreprise B`

<div align="center">
    <img src="/assets/images/biscuit-1/saml.png" alt="exemple d'utilisation des clefs asymétriques" style="align-items: center;" width="100%"/>
</div>


L’avantage de cette technique par rapport au secret, est que la clef publique peut librement être envoyée dans la nature, car il est impossible de retrouver la clef privée à
partir de la clef publique et que seule la clef publique est nécessaire à la vérification d’une signature. 😎

La clef privée n’a donc besoin d’être baladée qu’aux endroits nécessitant la signature.

Maintenant que vous avez les bases de la cryptographie, nous pouvons passer au token cryptographique.

### Token

Le Token cryptographique est la réunion de données et d’une signature numérique de celles-ci.

<div align="center">
    <img src="/assets/images/biscuit-1/token1.png" alt="token cryptographique" style="align-items: center;" width="100%"/>
</div>

L’idée est de pouvoir stocker des informations dans un cookie sans risque de les voir falsifier au retour de celui-ci.

<div align="center">
    <img src="/assets/images/biscuit-1/token2.png" alt="exemple de token" style="align-items: center;" width="50%"/>
</div>

Si le token demeure inchangé, la signature sera validée.

<div align="center">
    <img src="/assets/images/biscuit-1/token3.png" alt="token non-falsifié" style="align-items: center;" width="100%"/>
</div>

De ce fait, si Jaba a pour idée de modifier les informations de notre Token, alors le serveur s’en rendra compte immédiatement, car les données ne correspondront plus avec
la signature.

<div align="center">
    <img src="/assets/images/biscuit-1/token4.png" alt="token falsifié" style="align-items: center;" width="100%"/>
</div>

Si Jaba tente de modifier la signature, le serveur s’en rendra également compte, parce que la signature ne correspondra pas au secret ou à la clef privée qui a signé le Token.

<div align="center">
    <img src="/assets/images/biscuit-1/token5.png" alt="signature falsifiée" style="align-items: center;" width="100%"/>
</div>

Et là, Jaba est bien embêté, il ne peut plus se faire passer pour un administrateur de la plateforme. 😛

Les Tokens c’est bien, le souci, c’est que ce n’est pas standardisé et donc chacun doit mettre en place sa propre logique de vérification et de création des tokens.

### JWT

Le JWT est une initiative de standardisation des tokens.

JWT est l’abréviation de JSON Web Token, comme son nom l’indique : il s’agit d’un JSON, mais comme son nom ne l’indique pas il contient par ailleurs une signature numérique.

{% alert() %}
Je ne traiterai que du JWS, si vous voulez en apprendre plus sur le JWE, cet [article est parfait](https://medium.facilelogin.com/jwt-jws-and-jwe-for-not-so-dummies-b63310d201a3)
{% end %}

Le JWT est composé de 3 parties :

- un entête qui définit quel type de signature est utilisé (secret ou clefs asymétriques) au format JSON
- les données en elles-mêmes au format JSON
- la signature

{% warning(header="Attention!") %}
Lorsque vous créz des JWT, attention au choix de l’algorithme de signature, celui-ci vous est libre. Mais mal choisi, il peut occasionner des problèmes de sécurité.
{% end %}

Ces différentes informations sont alors encodées en base64 séparé par des points.

Ce qui donne par exemple :

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.          // en-tête
eyJ1c2VyIjoxLCJyaWdodCI6WyJyZWFkIl19.          // données
tqwenw0ShYJdia5zfeDiUvKNmD0mSf6pAkzHJ67-nFs    // signature
```

Le format des données est lui-même normalisé, les champs du JSON de données sont appelés des *claims*.

Il y a des champs standards qui permettent aux bibliothèques d’agir dans certaines circonstances :

Le champ `exp` par exemple permet de forcer l’expiration d’un JWT.

Il en existe plein d’autres, que vous pouvez retrouver [ici](https://www.rfc-editor.org/rfc/rfc7519#section-4.1).

En plus de ces champs normalisés, vous pouvez en rajouter autant que vous le voulez.

Un petit code d’exemple en utilisant un secret :


```python
import jose
from jose import jwt
import base64
import json

def verify(token, secret):
    try:
        jwt.decode(token, secret, algorithms=['HS256'])
        print("Signature valide")
    except jose.exceptions.JWTError:
        print("Signature invalide")
        

def create_token(data, secret):
    return jwt.encode(data, secret, algorithm='HS256')


message = {
    "user" : 666,
    "rights": ["read"]
}

pirate_message = {
    "user" : 1,
    "rights": ["read", "admin"]
}

secret = "12345"
pirate_secret = "6789"

# On créé un token valide
token = create_token(message, secret)

# On altère les données sans modifier la signature
token_parts = token.split(".")
pirate_message_encoded = json.dumps(
                pirate_message,
                separators=(",", ":"),
            ).encode("utf-8")

# On modifie la signature pour la faire correspondre aux données
token_with_altered_signature = create_token(pirate_message, pirate_secret)


token_parts[1] = base64.urlsafe_b64encode(pirate_message_encoded).replace(b"=", b"").decode("utf-8") 
pirate_token = ".".join(token_parts)

verify(token, secret)
verify(pirate_token, secret)
verify(token_with_altered_signature, secret)
```


Ce code nous renvoie :

```
Signature valide
Signature invalide
Signature invalide
```

Le code est disponible [ici](https://replit.com/@Akanoa/jwt#main.py).

Le souci du JWT est qu’il est impossible d’atténuer les droits.

Prenons l’exemple suivant, dans le JWT on indique l’ID de l’utilisateur, par définition celui-ci peut alors permettre d’agir sur toutes les ressources appartenant à cet utilisateur.

Si nous voulons déléguer des droits à un service, mais ne restreindre qu’à un certain périmètre de droits.

Exemple, uniquement en lecture sur les données qui se trouve dans le *bucket* films.

> Comment peut-on atténuer les droits du JWT ?

Pour cela l’on doit régénérer un autre JWT qui possède cette atténuation de droits.

<div align="center">
    <img src="/assets/images/biscuit-1/jwt1.png" alt="atténuation d'un jwt" style="align-items: center;" width="100%"/>
</div>

Généralement on met en place un système d’authentification centralisé qui se charge de conserver les secrets et les clefs privées. Son rôle est de vérifier les tokens ou
de distribuer les clefs publiques.

<div align="center">
    <img src="/assets/images/biscuit-1/jwt2.png" alt="validation d'un jwt" style="align-items: center;" width="100%"/>
</div>

Lorsque le service 1 veut faire atténuer un JWT, il transmet le nouveau payload, et reçoit un nouveau JWT. Et le transmet au service 2.

<div align="center">
    <img src="/assets/images/biscuit-1/jwt3.png" alt="demande de JWT" style="align-items: center;" width="100%"/>
</div>

Le service 2, fait alors une nouvelle demande de vérification sur ce nouveau token.

<div align="center">
    <img src="/assets/images/biscuit-1/jwt4.png" alt="vérification du JWT atténué" style="align-items: center;" width="100%"/>
</div>

{% note(title="") %}
Ici j’ai représenté le processus de vérification via un secret. Mais on aurait tout aussi bien pu le faire via une clef publique.

La vérification aurait été réalisée par Service 1, Service 2; et non le Service Auth.
{% end %}

Par contre, rien ne dit que ce token a vraiment “moins” de droits que l’autre.

Le JWT ne certifie qu’une seule chose : les données n’ont pas été falsifiées. 

C’est pour cette raison qu’un nouveau système est entré dans la course.

Il s’agit du Macaron !

### Macaron

Le Macaron est projet issu des [laboratoires de Google](https://research.google/pubs/pub41892/).

Son idée est de créer une chaîne de confiance de blocs qui s’inter-vérifient comme le fait une chaîne de certificats SSL par exemple.

Un macaron est composé de champs normalisés

<div align="center">
    <img src="/assets/images/biscuit-1/macaron1.png" alt="un macaron" style="align-items: center;" width="100%"/>
</div>

- location : est une simple chaîne, elle est stockée seulement à titre indicatif
- identifier : est un champ libre permettant de définir quel secret a été utilisé pour générer le macaron
- cid : est un caveat, c’est l’équivalent des *claims* de JWT, il peut y en avoir de 0 à plusieurs, il est fortement recommandé d’en mettre au moins un
sinon ce macaron est le joker ultime.
- signature : la signature cryptographique du macaron, nous allons voir en détails comment celle-ci est créée

#### Signature

Le macaron est également un token et donc signé.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron2.png" alt="signature d'un macaron" style="align-items: center;" width="100%"/>
</div>

Étant donné que nous avons ici encore une signature numérique, Jaba ne pourra pas falsifier les caveats (les droits), donc de ce point de vue on est bons également.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron3.png" alt="rejet d'un macaron falsifié" style="align-items: center;" width="100%"/>
</div>

La méthode de validation d’un macaron est standardisée.

#### Vérification des caveats

Pour chaque droit que l’on désire valider, on vient faire un ”string exact” sur chaque caveat.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron4.png" alt="vérification d'un caveat" style="align-items: center;" width="100%"/>
</div>

Si la signature et les caveats sont validés alors le macaron est validé.

Nous avons ici une méthode standardisée de vérification des droits en plus de la vérification de l’exactitude de ceux-ci.

Par contre, pour des droits qui nécessitent des opérations plus complexes qu’un “string equal”, il faut développer un parser qui vient analyser le caveat.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron5.png" alt="vérification d'un caveat plus complexe" style="align-items: center;" width="100%"/>
</div>

Maintenant voyons comment les macarons respectent les deux conditions de gestion : chaîne de confiance d’atténuation de droits et de validation de ces chaînes.

#### Chaîne de confiance

Le macaron a opté pour le choix du secret au lieu des clefs cryptographiques. 

On réalise la signature via une fonction appelée HMAC. Elle certifie qu’il est impossible de remonter au secret depuis la signature.

C’est la même qu’[ici](/biscuit-1/#signature-par-secret).

<div align="center">
    <img src="/assets/images/biscuit-1/macaron6.png" alt="fonction HMAC" style="align-items: center;" width="100%"/>
</div>

Par contre, cela signifie que le secret qui signe le macaron doit être distribué pour permettre la vérification du macaron.

On choisit un secret. De préférence ce secret doit être unique au macaron.

On définit aussi un **identifier**, celui-ci est un indice permettant de déterminer quel secret doit-être utilisé lors de la vérification.

On réalise alors la signature de couple (secret, identifier)

<div align="center">
    <img src="/assets/images/biscuit-1/macaron7.png" alt="signature initiale" style="align-items: center;" width="100%"/>
</div>

Prenons un macaron avec les caveats suivants :

```
cid user = 666
cid time < 2022-09-01
```

On utilise le premier caveat ainsi que la signature 0 pour générer une signature 1.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron8.png" alt="signature caveat 1" style="align-items: center;" width="100%"/>
</div>

Puis l’on fait de même avec le deuxième caveat et la signature 1

<div align="center">
    <img src="/assets/images/biscuit-1/macaron9.png" alt="signature caveat 9" style="align-items: center;" width="100%"/>
</div>

On peut poursuivre ce mécanisme de chaîne avec autant de caveats que l’on veut

<div align="center">
    <img src="/assets/images/biscuit-1/macaron10.png" alt="signature caveat n" style="align-items: center;" width="100%"/>
</div>

#### Vérification de la chaîne de confiance

Pour vérifier le macaron, il est nécessaire de reconstituer la chaîne de signatures en partant du secret.

On vient successivement recalculer toutes les signatures en passant en revue chaque caveat.

La fonction HMAC étant stable, pour un secret donné et un caveat donné, la signature résultante sera toujours la même.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron11.png" alt="vérification de la chaîne de confiance" style="align-items: center;" width="100%"/>
</div>

Si en bout de chaîne, la signature calculée est égale à la signature du macaron.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron12.png" alt="vérification de la chaîne de confiance" style="align-items: center;" width="100%"/>
</div>

Alors le macaron, n’a pas été falsifié.

Lorsque le macaron est transmis à l’utilisateur, il lui est totalement impossible de modifier les caveats.

{% warning(title="") %}
L’unique champ qui peut être considéré comme provenant de l’Autorité de signature est **l’identifier**. En effet même le premier caveat
peut-être considéré comme une atténuation.

Il est impossible de déterminer à postériori si le caveat est issu du macaron originel ou de son atténuation.
{% end %}

Car une évolution du caveat imposerait de connaître le secret.

Simulons rapidement avec un peu de code.

{% detail(title="Code python") %}
```python
import hashlib, hmac, binascii

def hmac_sha256(secret, message):
  return binascii.hexlify(hmac.new(secret, message, hashlib.sha256).digest())


class Macaron:
    def __init__(self, secret, identifier):
        self.caveats = []
        self.identifier = identifier
        self.signature = hmac_sha256(bytes(secret, encoding="utf-8"), bytes(identifier, encoding="utf-8"))

    def add_caveat(self, caveat):
        self.signature = hmac_sha256(self.signature, caveat.encode("utf-8"))
        self.caveats.append(caveat)

    def verify(self, secret):
        
        signature = hmac_sha256(bytes(secret, encoding="utf-8"), bytes(self.identifier, encoding="utf-8"))
        
        for caveat in self.caveats:
            signature = hmac_sha256(signature, caveat.encode("utf-8"))

        if self.signature == signature:
            print("Macaron valide")
        else:
            print("Macaron invalide")

    def __str__(self):
        data = f"Macaron\n \nidentifier: {self.identifier}\n---\ncaveats\n---\n"
        for caveat in self.caveats:
            data += f"cid {caveat}\n"
        data += f"\nsignature: {self.signature}\n\n###\n"
        return data

secret = "12345"

macaron = Macaron(secret, "simple")
print(macaron)
macaron.add_caveat("user = 666")
print(macaron)
macaron.add_caveat("time < 2022-09-01")
print(macaron)
```
{% end %}

Le code est disponible [ici](https://replit.com/@Akanoa/macaron#main.py).

À chaque ajout d’un caveat

La signature évolue

{% detail(title="Code python") %}
```
Macaron
 
identifier: simple
---
caveats
---

signature: b'07f6ce323d25ab7cd0af1527e1b850532eedb0f665ed48281c09542dc39b5627'

###

Macaron
 
identifier: simple
---
caveats
---
cid user = 666

signature: b'de18c7cc78ff83eaecbab893b0127d9dde26f3961828509f8e30af73627be838'

###

Macaron
 
identifier: simple
---
caveats
---
cid user = 666
cid time < 2022-09-01

signature: b'9f148339a6baca2bffade97f92eadbf7713a24e7ab7d2e2bd3d1332de28c9a8e'

###
```
{% end %}

Si l’on tente de falsifier un caveat, alors la signature calculée va différer.

```python
macaron.verify(secret)

# on falsifie un caveat
macaron.caveats[0] = "user = 1"

macaron.verify(secret)
```

Ce macaron falsifié vaut :

```
Macaron
 
identifier: simple
---
caveats
---
cid user = 1
cid time < 2022-09-01

signature: b'9f148339a6baca2bffade97f92eadbf7713a24e7ab7d2e2bd3d1332de28c9a8e'

###
```

Bien que les signatures soient identiques. Le premier sera validé, mais pas le second.

```
Macaron valide
Macaron invalide
```

Or, une fois que le macaron est créé, les seules informations que l’on connait de l’extérieur du macaron sont :
- l’identifier
- les caveats
- la signature

Reprenons, notre bon Jaba qui tente désespérément de devenir admin.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron13.png" alt="falsification de macaron" style="align-items: center;" width="100%"/>
</div>

De fait de la propriété de la fonction **HMAC**, il est impossible depuis la signature du macaron de revenir à la signature précédente qui a signé le dernier caveat du macaron.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron14.png" alt="hmac reverse caveat 2" style="align-items: center;" width="100%"/>
</div>

Comme notre caveat concernant l’utilisateur est le premier, cela signifie que l’on doit remonter 2 fois la signature. Les probabilités d’y arriver sont quasi nulles.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron15.png" alt="hmac reverse caveat 1" style="align-items: center;" width="100%"/>
</div>

De même que de reconstruire le macaron en connaissant seulement les caveats et pas le secret.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron16.png" alt="hmac secret" style="align-items: center;" width="100%"/>
</div>

Un secret différent créera une signature différente.

Un macaron peut donc être considéré comme immuable une fois créé.

Il peut ainsi être partagé avec un tiers qui peut lui-même rajouter ses caveats, sans pour autant pouvoir supprimer ou modifier les caveats précédents.

Voyons justement ce processus de rajout de caveats.

#### Atténuation

L’atténuation est l’ajout de nouveaux caveats dans le but de créer un macaron avec moins de droits (à comprendre au minimum les caveats précédents).

Reprenons l’exemple de tout à l’heure : comment atténuer le macaron pour n’accepter que les appels sur le *bucket* films.

Il suffit de rajouter un caveat.

```
cid path begins_with /bucket/films/
```
Un peu de code pour tester tout cela.

On définit nos `Verifiers`.

Le premier fait une comparaison classique de chaîne de caractères.  

```python
class StringEqual:
    def __init__(self, caveat):
        self.caveat = caveat
        
    def verify(self, caveat):
        return self.caveat == caveat
```

Le second vérifie qu’un certain pattern est respecté, ici un ”starts_with”

```python
class BeginWith:
    def __init__(self, path, pattern):
        self.pattern = pattern
        self.path = path
        
    def verify(self, caveat):
        regex = r"(.*)\s+begins_with\s+(.*)"
        matches = re.findall(regex, caveat)
        if matches is None:
            return False
        path = matches[0][0]
        pattern = matches[0][1]
        if path != self.path:
            return False

        return self.pattern.startswith(pattern)
```

Puis, on crée un objet `Verifier` qui prend un secret et le macaron en entrée :

```python
class Verifier:
    def __init__(self, secret, macaron):
        self.secret = secret
        self.verifiers = []
        self.macaron = macaron

    def verify(self):
        
        for caveat in self.macaron.caveats:
            verified = False
            for verifier in self.verifiers:
                if verifier.verify(caveat):
                    verified = True
                    break
                else:
                    continue
            if not verified:
                print("Macaron invalide")
                return

        self.macaron.verify(self.secret)
```

Son rôle va être d’itérer sur chaque caveat et de vérifier la signature.

On va par exemple dire que l’utilisateur 666 souhaite accéder au film “Ratatouille” (très bien d’ailleurs, je le recommande ^^ ).

Nous allons vérifier que le caveat concernant le champ ”path” commence bien par ”/bucket/films/ratatouille”. On connait le chemin étant donné que c’est la
ressource qui tente d’être accédée. 

Le code est disponible [ici](https://replit.com/@Akanoa/macaron2#main.py).

Testons sans atténuation :

```python
# le macaron provient de la requête
macaron = Macaron(secret, "simple")
macaron.add_caveat("user = 666")

verifier_success = Verifier(secret, macaron)
verifier_success.satisfy_exact("user = 666")

verifier_fail = Verifier(secret, macaron)
verifier_fail.satisfy_exact("user = 667")

verifier_success.verify()
verifier_fail.verify()
```

Ce qui donne le résultat suivant :

```
Macaron valide
Macaron invalide
```

Maintenant atténuons ses droits :

```python
macaron.add_caveat("path begins_with /bucket/films/")
```

Vérifions la restriction.

```python
# la ressource que l'on tente d'atteindre
ressource = "/bucket/films/ratatouille"

verifier_right_path = Verifier(secret, macaron)
verifier_right_path.satisfy_exact("user = 666")
verifier_right_path.satisfy_general(BeginWith("path", ressource))

verifier_right_path.verify()
```

```
Macaron valide ✅
```

```python
# la ressource que l'on tente d'atteindre
ressource = "/bucket/compta/facture_12"

verifier_right_path = Verifier(secret, macaron)
verifier_right_path.satisfy_exact("user = 666")
verifier_right_path.satisfy_general(BeginWith("path", ressource))

verifier_right_path.verify()
```

```
Macaron invalide ❌
```

Le macaron est donc atténuable sans avoir besoin de connaître le secret. On appelle ça de l’**atténuation offline**.

Par contre, pour vérifier ce macaron atténué, il faut connaître le secret.

Si le macaron a été fourni à un tiers, cela impose d’exposer une API d’authentification qui se chargera de valider le macaron.

Prenons l’atténuation suivante

<div align="center">
    <img src="/assets/images/biscuit-1/macaron17.png" alt="atténuation d'un macaron" style="align-items: center;" width="100%"/>
</div>

Le service 1, reçoit un macaron, le fait valider par l’API d’Auth.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron18.png" alt="vérification du macaron" style="align-items: center;" width="100%"/>
</div>

Puis le transmet au service 2.

Celui-ci, lui-même le fait vérifier.

<div align="center">
    <img src="/assets/images/biscuit-1/macaron19.png" alt="vérification d'un macaron atténué" style="align-items: center;" width="100%"/>
</div>

L’atténuation est offline, mais pas sa vérification, le secret reste centralisé et donc doit soit être distribué, soit nécessite un service d’authentification  tiers.

Biscuit est une proposition qui vise à la fois régler les problèmes du JWT et ceux des Macarons.

### Biscuit

Je sais que vous êtes là pour Biscuit, mais c’est encore vraiment trop complexe pour que ça puisse tenir ici.

On verra dans la partie 2, l’anatomie d’un Biscuit ainsi que les mécanismes qui permettront à la fois de le sécuriser et de l’atténuer.

La suite sera [là](/biscuit-2).

## Conclusion

Si vous êtes arrivé jusqu’ici, je vous en félicite ! 😄

Un petit récapitulatif de ce que l’on a vu.

Les sessions côté serveur sont bien, mais exigent de conserver un état et donc une persistance.

Lorsque les infrastructures grandissent, le besoin de passer sur des mécanismes d’authentification sans état peut se faire sentir.

Les cookies ne pouvant pas être fournis sans sécurité, le mécanisme de signature a été inventé, ce qui a fait apparaître le standard JWT.

Macaron par la suite, a été une tentative de rendre ces tokens immuables et atténuables.

Mais en introduisant 2 problèmes, le premier est la nécessité d’une centralisation de la vérification et le second l’absence de normalisation de la
vérification du Macaron.

|                         Critères                          | Cookie | Token | JWT | Macaron | Biscuit |
|:---------------------------------------------------------:|:------:|:-----:|:---:|:-------:|:-------:|
|                         Sécurisé                          |   ❌    |   ✅   |  ✅  |    ✅    |    ❔    |
|                     Payload normalisé                     |   ❌    |   ❌   |  ❌  |    ❌    |    ❔    |
|                  Vérification normalisée                  |   ❌    |   ❌   |  ❌  |    ❌    |    ❔    |
|                         Immuable                          |   ❌    |   ❌   |  ❌  |    ✅    |    ❔    |
|                    Atténuation offline                    |   ❌    |   ❌   |  ❌  |    ❌    |    ❔    |
|                 Vérifié par clef publique                 |   ❌    |   ✅   |  ✅  |    ❌    |    ❔    |
| Peut contenir des données non utiles pour la vérification |   ✅    |   ✅   |  ✅  |    ❌    |    ❔    |

Biscuit a pour but de mettre une ✅ sur chaque ligne.

Un grand merci à [Geoffroy Couprie](https://twitter.com/gcouprie) et [Clément Delafargue](https://twitter.com/clementd), qui ont bien voulu prendre de leur temps pour m'expliquer les concepts que je ne comprenais pas et effectuer la relecture attentive de cet article.

Ils sont accessoirement derrière Biscuit. ^^

Je vous remercie encore une fois de n’avoir lu et je vous donne rendez-vous dans la [partie 2](/biscuit-2). ❤️
