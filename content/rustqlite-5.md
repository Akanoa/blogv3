+++
title = "Partie 5 : Parser les commandes, création de la boîte à outils"
date = 2024-12-08
draft = false
template  = 'post.html'

[taxonomies]
categories = ["Réimplémenter sqlite en Rust"]
tags = ["rust", "sqlite", "system"]

[extra]
lang = "fr"
toc = true
math = true
mermaid = false
biscuit = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Réimplémenter sqlite en Rust : Partie 5" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/rustqlite-5.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Réimplémenter sqlite en Rust : Partie 5" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/rustqlite-5.png" },
    { property = "og:url", content="https://lafor.ge/rustqlite-5" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]

+++


{% detail(title="Les articles de la série") %}
{{ summary(path="content/toc/rustqlite.toml") }}
{% end %}

Bonjour à toutes et tous 😃

Dans la [précédente partie](/rustqlite-4) nous avons rajouté la possibilité de stocker des type de données différents via l'introduction du concept de table.

```
db > create user
Table created successfully
db > insert user 1 name email@example.com
Record inserted successfully
db > insert user 2 name2 email2@example.com
Record inserted successfully
db > create car
Table created successfully
db > select car
db > insert car XXX Volvo
Record inserted successfully
db > insert car YYY Renault
Record inserted successfully
db > select user
User(User { id: 1, username: "name", email: "email@example.com" })
User(User { id: 2, username: "name2", email: "email2@example.com" })
db > select car
Car(Car { id: "XXX", brand: "Volvo" })
Car(Car { id: "YYY", brand: "Renault" })
```

Bien que fonctionnel, ce design a deux gros problèmes:
- les schémas de données sont fixes
- rajouter un nouveau type de données nécessite :
    - d'écrire le parser
    - définir un nouveau type de table
    - implémenter la sérialisation de la données


C'est pas souple pour un sou. 😑

Je vous propose dans cette partie d'introduire une nouvelle notation qui va nous permettre de faire ce que l'on a besoin.

Pour cela nous allons avoir besoin d'un nouveau parser bien plus efficace et versatile.

C'est ce que je vous propose de construire aujourd'hui.

{%note()%}
Je suis conscient que la marche de complexité est grande entre cette partie et la précédente.

Si vous ne comprenez pas tout malgré mes explications, ce n'est pas grave, nous utiliseront le travail de cette partie comme une lib.

Vous n'aurez qu'à la considérer comme une boîte noire. 😃
{%end%}

## Cahier des charges

Avant de nous lancer à corps perdu dans le code. Definissons les contours de l'API désirée.

Nous voulons que l'utilisateur à la création de la table puisse annoncer le schéma associé.

```sql
CREATE TABLE User (
    id INTEGER,     -- un nombre
    name TEXT(50), -- chaîne de caractèrees sur 50 bytes
    email TEXT(25) -- chaîne de caractèrees sur 25 bytes
);

CREATE TABLE Car (
    id TEXT(128),
    brand TEXT(50),
);
```

Nous voulons également que lors de l'insertion, l'utilisateur définisse le mapping entre les champs et leurs valeurs

```sql
INSERT INTO User (id, name, email) 
VALUES(42, 'name1', 'email1@example.com');

INSERT INTO Car (id, brand) 
VALUES('XXX', 'Volvo');
```

Et finalement nous désirons sélectionner les champs renvoyés (on dit projeté) depuis la table. L'`*` signifie tous les champs.

```sql
SELECT id, name FROM User;
SELECT * FROM User;
```

### Tokenization

Notre oeil est capable en connaissant la syntaxe SQL d'interpréter ce que la commande va ou doit faire. Mais la machine n'a pas d'yeux.

Elle manipule des bytes et donc tout doit rentrer dans se mode pensé.

Une machine pour interpréter une commande va d'abord tokenizer une chaîne de caratères, puis interpréter cet emchaînement de tokens dans ce que l'on appelle
une *grammaire*.

Le `INSERT INTO User` a une signification, `INSERT` tout seul ou `INTO` tout seul ne veut rien dire, par contre `INSERT INTO table` signifie "insérer dans la table 'table'".

Comme en français, en fait, vous me direz, et vous avez raison. ^^

Laissez moi vous montrer comment la machine voit


```sql
CREATE TABLE User (id INTEGER, name TEXT(50), email TEXT(25));
```

- token "CREATE" : mot clef réservé
- token Whitespace : espace blanc
- token "TABLE"  : mot clef réservé
- token Whitespace : espace blanc
- literal "User" : enchaînement de char jusqu'à un espace blanc ou un parenthèse ouvrante
- token Whitespace : espace blanc 
- token "(" : parenthèse ouvrante
- literal "id"
- token Whitespace : espace blanc 
- token "INTEGER"
- token "," : séparateur réservé
- token Whitespace
- literal "name"
- token Whitespace
- token "TEXT"
- token "("
- literal 50 : un nombre
- token ")"
- token ","
- token Whitespace
- literal "name"
- token Whitespace
- token "TEXT"
- token "("
- literal 25 : un nombre
- token ")"
- token ")"
- token ";"

Si on sérialise ça donne

```
CREATE, WHITESPACE, TABLE, WHITESPACE, "User", WHITESPACE, OPEN_PAREN, "id", WHITESPACE, INTEGER, COMMA, WHITESPACE, "name", WHITESPACE, TEXT, OPEN_PAREN, 50, CLOSE_PAREN, COMMA, WHITESPACE, "email", WHITESPACE, TEXT, OPEN_PAREN, 25, CLOSE_PAREN
```

Mais ça ce n'est que le début, il faut ensuite regrouper ce qui le doit.

{%note()%}
Si j'écris : 
- WHITESPACE* ça veut dire 0 ou plus espace blancs 
- WHITESPACE+ ça veut dire au moins un espace blanc
{%end%}

- CREATE, WHITESPACE+, TABLE, WHITESPACE+, "User" : définition de la commande et du nom de la table à créer
- WHITESPACE*, OPEN_PAREN : début de groupe de définition du schéma
- "id", WHITESPACE+, NUMBER : définition du champ "id"
- COMMA, WHITESPACE* : séparateur de champ
- "name", WHITESPACE+, TEXT, OPEN_PAREN, 50, CLOSE_PAREN : définition du champ "name"
- COMMA, WHITESPACE* : séparateur de champ
- "email", WHITESPACE+, TEXT, OPEN_PAREN, 25, CLOSE_PAREN : définition du champ "email"
- WHITESPACE*, CLOSE_PAREN
- WHITESPACE*, SEMICOLON

On a ici une subdivision et une factorisation à opérer.

Le type de la colonne devient alors une entité à part entière {column_def}

- NUMBER
- TEXT, OPEN_PAREN, 50, CLOSE_PAREN
- TEXT, OPEN_PAREN, 25, CLOSE_PAREN

Si on nettoie pour ne garder que les espaces essentiel et que l'on utilise notre factorisation, cela donne

```
CREATE, WHITESPACE, TABLE, WHITESPACE, {table name}, OPEN_PAREN, [{column_name}, WHITESPACE, {column_def}, COMMA ]+, CLOSE_PAREN, SEMICOLON
```

{%note()%}
le `[...]+` signifie que le groupe se répète au moins une fois.
{%end%}

Vous la sentez la difficulté du problème auquel on s'attèle? 😅

Sans une rigueur à tout épreuve, nous allons aller très vite dans le mur !

### Tokens

La première chose à faire est de faire l'inventaire exhaustif des tokens.

Les mot clefs déjà:

- CREATE
- TABLE
- INSERT
- INTO
- VALUES
- SELECT
- FROM

La ponctuation

- OPEN_PAREN
- CLOSE_PAREN
- COMMA
- SEMICOLON
- QUOTE
- WHITESPACE
- STAR

Les types de champs

- INTEGER
- TEXT

Puis les literals, les tokens qui sont dynamiques

- LITERAL_INTEGER : tout ce qui se parse vers du i64
- LITERAL_STRING : tout le reste qui ne peut pas être interprété

### Grammaire

Pour définir les grammaire, à comprendre l'enchaînement des tokens.

Je vous propose d'utiliser une syntaxe appelée l'[BNF](https://en.wikipedia.org/wiki/Syntax_diagram).

Et un [site](https://rr.red-dove.com) qui permet de les visualiser.

{%detail(title="Grammaire de CREATE TABLE")%}

```
CreateTable ::= 'CREATE' 'TABLE' LiteralString OpenParen ColumnDef* CloseParen Semicolon
ColumnDef ::= LiteralString ColumnType | LiteralString ColumnType Comma
ColumnType ::= 'INTEGER' | ColumTypeText
ColumTypeText ::= 'TEXT' OpenParen LiteralInteger CloseParen
LiteralString ::=  [A-Za-z0-9_]*
LiteralInteger ::= [0-9]+
OpenParen ::= '('
CloseParen ::= ')'
Comma ::= ','
Semicolon ::= ';'
```  
<p style="font-size: 14px; font-weight:bold"><a name="CreateTable">CreateTable:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22613%22%20height%3D%2271%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2237%22%20width%3D%2272%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2235%22%20width%3D%2272%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2255%22%3ECREATE%3C%2Ftext%3E%3Crect%20x%3D%22123%22%20y%3D%2237%22%20width%3D%2262%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22121%22%20y%3D%2235%22%20width%3D%2262%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22131%22%20y%3D%2255%22%3ETABLE%3C%2Ftext%3E%3Crect%20x%3D%22205%22%20y%3D%2237%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22203%22%20y%3D%2235%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22213%22%20y%3D%2255%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22321%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22319%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22329%22%20y%3D%2255%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22387%22%20y%3D%223%22%20width%3D%2288%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22385%22%20y%3D%221%22%20width%3D%2288%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22395%22%20y%3D%2221%22%3EColumnDef%3C%2Ftext%3E%3Crect%20x%3D%22515%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22513%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22523%22%20y%3D%2255%22%3E%29%3C%2Ftext%3E%3Crect%20x%3D%22561%22%20y%3D%2237%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22559%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22569%22%20y%3D%2255%22%3E%3B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m0%200%20h10%20m72%200%20h10%20m0%200%20h10%20m62%200%20h10%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m26%200%20h10%20m20%200%20h10%20m0%200%20h98%20m-128%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m108%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-108%200%20h10%20m88%200%20h10%20m20%2034%20h10%20m26%200%20h10%20m0%200%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22603%2051%20611%2047%20611%2055%22%2F%3E%3Cpolygon%20points%3D%22603%2051%20595%2047%20595%2055%22%2F%3E%3C%2Fsvg%3E" height="71" width="613" usemap="#CreateTable.map"><map name="CreateTable.map"><area shape="rect" coords="203,35,299,67" href="#LiteralString" title="LiteralString"><area shape="rect" coords="385,1,473,33" href="#ColumnDef" title="ColumnDef"></map>

<p style="font-size: 14px; font-weight:bold"><a name="ColumnType">ColumnType:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22215%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2280%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2280%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3EINTEGER%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%22116%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%22116%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3EColumTypeText%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m80%200%20h10%20m0%200%20h36%20m-156%200%20h20%20m136%200%20h20%20m-176%200%20q10%200%2010%2010%20m156%200%20q0%20-10%2010%20-10%20m-166%2010%20v24%20m156%200%20v-24%20m-156%2024%20q0%2010%2010%2010%20m136%200%20q10%200%2010%20-10%20m-146%2010%20h10%20m116%200%20h10%20m23%20-44%20h-3%22%2F%3E%3Cpolygon%20points%3D%22205%2017%20213%2013%20213%2021%22%2F%3E%3Cpolygon%20points%3D%22205%2017%20197%2013%20197%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="215" usemap="#ColumnType.map"><map name="ColumnType.map"><area shape="rect" coords="49,45,165,77" href="#ColumTypeText" title="ColumTypeText"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="ColumTypeText">ColumTypeText:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22331%22%20height%3D%2237%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%223%22%20width%3D%2254%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%221%22%20width%3D%2254%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2221%22%3ETEXT%3C%2Ftext%3E%3Crect%20x%3D%22105%22%20y%3D%223%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22103%22%20y%3D%221%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22113%22%20y%3D%2221%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22151%22%20y%3D%223%22%20width%3D%22106%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22149%22%20y%3D%221%22%20width%3D%22106%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22159%22%20y%3D%2221%22%3ELiteralInteger%3C%2Ftext%3E%3Crect%20x%3D%22277%22%20y%3D%223%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22275%22%20y%3D%221%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22285%22%20y%3D%2221%22%3E%29%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m0%200%20h10%20m54%200%20h10%20m0%200%20h10%20m26%200%20h10%20m0%200%20h10%20m106%200%20h10%20m0%200%20h10%20m26%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22321%2017%20329%2013%20329%2021%22%2F%3E%3Cpolygon%20points%3D%22321%2017%20313%2013%20313%2021%22%2F%3E%3C%2Fsvg%3E" height="37" width="331" usemap="#ColumTypeText.map"><map name="ColumTypeText.map"><area shape="rect" coords="149,1,255,33" href="#LiteralInteger" title="LiteralInteger"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%22189%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Cpolygon%20points%3D%2251%20151%2058%20135%20106%20135%20113%20151%20106%20167%2058%20167%22%2F%3E%3Cpolygon%20points%3D%2249%20149%2056%20133%20104%20133%20111%20149%20104%20165%2056%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%20107%2058%2091%20104%2091%20111%20107%20104%20123%2058%20123%22%2F%3E%3Cpolygon%20points%3D%2249%20105%2056%2089%20102%2089%20109%20105%20102%20121%2056%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%2063%2058%2047%20106%2047%20113%2063%20106%2079%2058%2079%22%2F%3E%3Cpolygon%20points%3D%2249%2061%2056%2045%20104%2045%20111%2061%20104%2077%2056%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m23%20166%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20159%20179%20159%20187%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20143%20179%20143%20187%22%2F%3E%3C%2Fsvg%3E" height="189" width="161"><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161">
{%end%}

{%detail(title="Grammaire de SELECT")%}

```
Select ::= 'SELECT' Column* 'FROM' LiteralString Semicolon
Column ::= '*' | LiteralString | LiteralString Comma
LiteralString ::=  [A-Za-z0-9_]*
LiteralInteger ::= [0-9]+
OpenParen ::= '('
CloseParen ::= ')'
Comma ::= ','
Semicolon ::= ';'
```

<p style="font-size: 14px; font-weight:bold"><a name="Select">Select:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22497%22%20height%3D%2271%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2237%22%20width%3D%2270%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2235%22%20width%3D%2270%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2255%22%3ESELECT%3C%2Ftext%3E%3Crect%20x%3D%22141%22%20y%3D%223%22%20width%3D%2268%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22139%22%20y%3D%221%22%20width%3D%2268%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22149%22%20y%3D%2221%22%3EColumn%3C%2Ftext%3E%3Crect%20x%3D%22249%22%20y%3D%2237%22%20width%3D%2260%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22247%22%20y%3D%2235%22%20width%3D%2260%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22257%22%20y%3D%2255%22%3EFROM%3C%2Ftext%3E%3Crect%20x%3D%22329%22%20y%3D%2237%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22327%22%20y%3D%2235%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22337%22%20y%3D%2255%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22445%22%20y%3D%2237%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22443%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22453%22%20y%3D%2255%22%3E%3B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m0%200%20h10%20m70%200%20h10%20m20%200%20h10%20m0%200%20h78%20m-108%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m88%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-88%200%20h10%20m68%200%20h10%20m20%2034%20h10%20m60%200%20h10%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22487%2051%20495%2047%20495%2055%22%2F%3E%3Cpolygon%20points%3D%22487%2051%20479%2047%20479%2055%22%2F%3E%3C%2Fsvg%3E" height="71" width="497" usemap="#Select.map"><map name="Select.map"><area shape="rect" coords="139,1,207,33" href="#Column" title="Column"><area shape="rect" coords="327,35,423,67" href="#LiteralString" title="LiteralString"></map>
<p style="font-size: 14px; font-weight:bold"><a name="Column">Column:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22279%22%20height%3D%22113%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E%2A%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22187%22%20y%3D%2279%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22185%22%20y%3D%2277%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22195%22%20y%3D%2297%22%3E%2C%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m28%200%20h10%20m0%200%20h152%20m-220%200%20h20%20m200%200%20h20%20m-240%200%20q10%200%2010%2010%20m220%200%20q0%20-10%2010%20-10%20m-230%2010%20v24%20m220%200%20v-24%20m-220%2024%20q0%2010%2010%2010%20m200%200%20q10%200%2010%20-10%20m-210%2010%20h10%20m96%200%20h10%20m20%200%20h10%20m0%200%20h34%20m-64%200%20h20%20m44%200%20h20%20m-84%200%20q10%200%2010%2010%20m64%200%20q0%20-10%2010%20-10%20m-74%2010%20v12%20m64%200%20v-12%20m-64%2012%20q0%2010%2010%2010%20m44%200%20q10%200%2010%20-10%20m-54%2010%20h10%20m24%200%20h10%20m43%20-76%20h-3%22%2F%3E%3Cpolygon%20points%3D%22269%2017%20277%2013%20277%2021%22%2F%3E%3Cpolygon%20points%3D%22269%2017%20261%2013%20261%2021%22%2F%3E%3C%2Fsvg%3E" height="113" width="279" usemap="#Column.map"><map name="Column.map"><area shape="rect" coords="49,45,145,77" href="#LiteralString" title="LiteralString"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%22189%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Cpolygon%20points%3D%2251%20151%2058%20135%20106%20135%20113%20151%20106%20167%2058%20167%22%2F%3E%3Cpolygon%20points%3D%2249%20149%2056%20133%20104%20133%20111%20149%20104%20165%2056%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%20107%2058%2091%20104%2091%20111%20107%20104%20123%2058%20123%22%2F%3E%3Cpolygon%20points%3D%2249%20105%2056%2089%20102%2089%20109%20105%20102%20121%2056%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2251%2063%2058%2047%20106%2047%20113%2063%20106%2079%2058%2079%22%2F%3E%3Cpolygon%20points%3D%2249%2061%2056%2045%20104%2045%20111%2061%20104%2077%2056%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2259%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m23%20166%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20159%20179%20159%20187%22%2F%3E%3Cpolygon%20points%3D%22151%20183%20143%20179%20143%20187%22%2F%3E%3C%2Fsvg%3E" height="189" width="161"><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161"><br>

{%end%}

{%detail(title="Grammaire de INSERT INTO")%}

```
InsertInto ::= 'INSERT' 'INTO' LiteralString  OpenParen Column* CloseParen 'VALUES' OpenParen Value*  CloseParen Semicolon
Column ::= LiteralString | LiteralString Comma
Value ::= (LiteralInteger | LiteralString ) | ((LiteralInteger | LiteralString) Comma ) 
LiteralString ::=  "'" [A-Za-z0-9_]* "'"
LiteralInteger ::= [0-9]+
OpenParen ::= '('
CloseParen ::= ')'
Comma ::= ','
Semicolon ::= ';'
```

<p style="font-size: 14px; font-weight:bold"><a name="InsertInto">InsertInto:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22885%22%20height%3D%2271%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2051%201%2047%201%2055%22%2F%3E%3Cpolygon%20points%3D%2217%2051%209%2047%209%2055%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%2237%22%20width%3D%2270%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%2235%22%20width%3D%2270%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%2255%22%3EINSERT%3C%2Ftext%3E%3Crect%20x%3D%22121%22%20y%3D%2237%22%20width%3D%2256%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22119%22%20y%3D%2235%22%20width%3D%2256%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22129%22%20y%3D%2255%22%3EINTO%3C%2Ftext%3E%3Crect%20x%3D%22197%22%20y%3D%2237%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22195%22%20y%3D%2235%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22205%22%20y%3D%2255%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22313%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22311%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22321%22%20y%3D%2255%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22379%22%20y%3D%223%22%20width%3D%2268%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22377%22%20y%3D%221%22%20width%3D%2268%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22387%22%20y%3D%2221%22%3EColumn%3C%2Ftext%3E%3Crect%20x%3D%22487%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22485%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22495%22%20y%3D%2255%22%3E%29%3C%2Ftext%3E%3Crect%20x%3D%22533%22%20y%3D%2237%22%20width%3D%2272%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22531%22%20y%3D%2235%22%20width%3D%2272%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22541%22%20y%3D%2255%22%3EVALUES%3C%2Ftext%3E%3Crect%20x%3D%22625%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22623%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22633%22%20y%3D%2255%22%3E%28%3C%2Ftext%3E%3Crect%20x%3D%22691%22%20y%3D%223%22%20width%3D%2256%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%22689%22%20y%3D%221%22%20width%3D%2256%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%22699%22%20y%3D%2221%22%3EValue%3C%2Ftext%3E%3Crect%20x%3D%22787%22%20y%3D%2237%22%20width%3D%2226%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22785%22%20y%3D%2235%22%20width%3D%2226%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22795%22%20y%3D%2255%22%3E%29%3C%2Ftext%3E%3Crect%20x%3D%22833%22%20y%3D%2237%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22831%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22841%22%20y%3D%2255%22%3E%3B%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2051%20h2%20m0%200%20h10%20m70%200%20h10%20m0%200%20h10%20m56%200%20h10%20m0%200%20h10%20m96%200%20h10%20m0%200%20h10%20m26%200%20h10%20m20%200%20h10%20m0%200%20h78%20m-108%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m88%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-88%200%20h10%20m68%200%20h10%20m20%2034%20h10%20m26%200%20h10%20m0%200%20h10%20m72%200%20h10%20m0%200%20h10%20m26%200%20h10%20m20%200%20h10%20m0%200%20h66%20m-96%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m76%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-76%200%20h10%20m56%200%20h10%20m20%2034%20h10%20m26%200%20h10%20m0%200%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22875%2051%20883%2047%20883%2055%22%2F%3E%3Cpolygon%20points%3D%22875%2051%20867%2047%20867%2055%22%2F%3E%3C%2Fsvg%3E" height="71" width="885" usemap="#InsertInto.map"><map name="InsertInto.map"><area shape="rect" coords="195,35,291,67" href="#LiteralString" title="LiteralString"><area shape="rect" coords="377,1,445,33" href="#Column" title="Column"><area shape="rect" coords="689,1,745,33" href="#Value" title="Value"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="Column">Column:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22239%22%20height%3D%2269%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%223%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%221%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2239%22%20y%3D%2221%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22167%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22165%22%20y%3D%2233%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22175%22%20y%3D%2253%22%3E%2C%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m0%200%20h10%20m96%200%20h10%20m20%200%20h10%20m0%200%20h34%20m-64%200%20h20%20m44%200%20h20%20m-84%200%20q10%200%2010%2010%20m64%200%20q0%20-10%2010%20-10%20m-74%2010%20v12%20m64%200%20v-12%20m-64%2012%20q0%2010%2010%2010%20m44%200%20q10%200%2010%20-10%20m-54%2010%20h10%20m24%200%20h10%20m23%20-32%20h-3%22%2F%3E%3Cpolygon%20points%3D%22229%2017%20237%2013%20237%2021%22%2F%3E%3Cpolygon%20points%3D%22229%2017%20221%2013%20221%2021%22%2F%3E%3C%2Fsvg%3E" height="69" width="239" usemap="#Column.map"><map name="Column.map"><area shape="rect" coords="29,1,125,33" href="#LiteralString" title="LiteralString"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="Value">Value:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22289%22%20height%3D%2281%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2017%201%2013%201%2021%22%2F%3E%3Cpolygon%20points%3D%2217%2017%209%2013%209%2021%22%2F%3E%3Crect%20x%3D%2251%22%20y%3D%223%22%20width%3D%22106%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%221%22%20width%3D%22106%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2221%22%3ELiteralInteger%3C%2Ftext%3E%3Crect%20x%3D%2251%22%20y%3D%2247%22%20width%3D%2296%22%20height%3D%2232%22%2F%3E%3Crect%20x%3D%2249%22%20y%3D%2245%22%20width%3D%2296%22%20height%3D%2232%22%20class%3D%22nonterminal%22%2F%3E%3Ctext%20class%3D%22nonterminal%22%20x%3D%2259%22%20y%3D%2265%22%3ELiteralString%3C%2Ftext%3E%3Crect%20x%3D%22217%22%20y%3D%2235%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22215%22%20y%3D%2233%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22225%22%20y%3D%2253%22%3E%2C%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2017%20h2%20m20%200%20h10%20m106%200%20h10%20m-146%200%20h20%20m126%200%20h20%20m-166%200%20q10%200%2010%2010%20m146%200%20q0%20-10%2010%20-10%20m-156%2010%20v24%20m146%200%20v-24%20m-146%2024%20q0%2010%2010%2010%20m126%200%20q10%200%2010%20-10%20m-136%2010%20h10%20m96%200%20h10%20m0%200%20h10%20m40%20-44%20h10%20m0%200%20h34%20m-64%200%20h20%20m44%200%20h20%20m-84%200%20q10%200%2010%2010%20m64%200%20q0%20-10%2010%20-10%20m-74%2010%20v12%20m64%200%20v-12%20m-64%2012%20q0%2010%2010%2010%20m44%200%20q10%200%2010%20-10%20m-54%2010%20h10%20m24%200%20h10%20m23%20-32%20h-3%22%2F%3E%3Cpolygon%20points%3D%22279%2017%20287%2013%20287%2021%22%2F%3E%3Cpolygon%20points%3D%22279%2017%20271%2013%20271%2021%22%2F%3E%3C%2Fsvg%3E" height="81" width="289" usemap="#Value.map"><map name="Value.map"><area shape="rect" coords="49,1,155,33" href="#LiteralInteger" title="LiteralInteger"><area shape="rect" coords="49,45,145,77" href="#LiteralString" title="LiteralString"></map><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralString">LiteralString:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22249%22%20height%3D%22203%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%20183%201%20179%201%20187%22%2F%3E%3Cpolygon%20points%3D%2217%20183%209%20179%209%20187%22%2F%3E%3Crect%20x%3D%2231%22%20y%3D%22169%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2229%22%20y%3D%22167%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%2239%22%20y%3D%22187%22%3E%27%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%20151%20102%20135%20150%20135%20157%20151%20150%20167%20102%20167%22%2F%3E%3Cpolygon%20points%3D%2293%20149%20100%20133%20148%20133%20155%20149%20148%20165%20100%20165%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%22153%22%3E%5BA-Z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%20107%20102%2091%20148%2091%20155%20107%20148%20123%20102%20123%22%2F%3E%3Cpolygon%20points%3D%2293%20105%20100%2089%20146%2089%20153%20105%20146%20121%20100%20121%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%22109%22%3E%5Ba-z%5D%3C%2Ftext%3E%3Cpolygon%20points%3D%2295%2063%20102%2047%20150%2047%20157%2063%20150%2079%20102%2079%22%2F%3E%3Cpolygon%20points%3D%2293%2061%20100%2045%20148%2045%20155%2061%20148%2077%20100%2077%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%22108%22%20y%3D%2265%22%3E%5B0-9%5D%3C%2Ftext%3E%3Crect%20x%3D%2295%22%20y%3D%223%22%20width%3D%2228%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%2293%22%20y%3D%221%22%20width%3D%2228%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22103%22%20y%3D%2221%22%3E_%3C%2Ftext%3E%3Crect%20x%3D%22197%22%20y%3D%22169%22%20width%3D%2224%22%20height%3D%2232%22%20rx%3D%2210%22%2F%3E%3Crect%20x%3D%22195%22%20y%3D%22167%22%20width%3D%2224%22%20height%3D%2232%22%20class%3D%22terminal%22%20rx%3D%2210%22%2F%3E%3Ctext%20class%3D%22terminal%22%20x%3D%22205%22%20y%3D%22187%22%3E%27%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%20183%20h2%20m0%200%20h10%20m24%200%20h10%20m20%200%20h10%20m0%200%20h72%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-14%20q0%20-10%2010%20-10%20m82%2034%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-14%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m60%200%20h10%20m0%200%20h2%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m62%200%20h10%20m-92%2010%20l0%20-44%20q0%20-10%2010%20-10%20m92%2054%20l0%20-44%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m28%200%20h10%20m0%200%20h34%20m20%20166%20h10%20m24%200%20h10%20m3%200%20h-3%22%2F%3E%3Cpolygon%20points%3D%22239%20183%20247%20179%20247%20187%22%2F%3E%3Cpolygon%20points%3D%22239%20183%20231%20179%20231%20187%22%2F%3E%3C%2Fsvg%3E" height="203" width="249"><br>
<p style="font-size: 14px; font-weight:bold"><a name="LiteralInteger">LiteralInteger:</a></p><img style="margin: 0" border="0" src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22161%22%20height%3D%2253%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%40namespace%20%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3B%20.line%20%7Bfill%3A%20none%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20.bold-line%20%7Bstroke%3A%20%23141000%3B%20shape-rendering%3A%20crispEdges%3B%20stroke-width%3A%202%3B%7D%20.thin-line%20%7Bstroke%3A%20%231F1800%3B%20shape-rendering%3A%20crispEdges%7D%20.filled%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20none%3B%7D%20text.terminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%23141000%3B%20font-weight%3A%20bold%3B%20%7D%20text.nonterminal%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231A1400%3B%20font-weight%3A%20normal%3B%20%7D%20text.regexp%20%7Bfont-family%3A%20Verdana%2C%20Sans-serif%3B%20font-size%3A%2012px%3B%20fill%3A%20%231F1800%3B%20font-weight%3A%20normal%3B%20%7D%20rect%2C%20circle%2C%20polygon%20%7Bfill%3A%20%23332900%3B%20stroke%3A%20%23332900%3B%7D%20rect.terminal%20%7Bfill%3A%20%23FFDB4D%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.nonterminal%20%7Bfill%3A%20%23FFEC9E%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%20rect.text%20%7Bfill%3A%20none%3B%20stroke%3A%20none%3B%7D%20polygon.regexp%20%7Bfill%3A%20%23FFF4C7%3B%20stroke%3A%20%23332900%3B%20stroke-width%3A%201%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cpolygon%20points%3D%229%2033%201%2029%201%2037%22%2F%3E%3Cpolygon%20points%3D%2217%2033%209%2029%209%2037%22%2F%3E%3Cpolygon%20points%3D%2251%2035%2058%2019%20106%2019%20113%2035%20106%2051%2058%2051%22%2F%3E%3Cpolygon%20points%3D%2249%2033%2056%2017%20104%2017%20111%2033%20104%2049%2056%2049%22%20class%3D%22regexp%22%2F%3E%3Ctext%20class%3D%22regexp%22%20x%3D%2264%22%20y%3D%2237%22%3E%5B0-9%5D%3C%2Ftext%3E%3Cpath%20class%3D%22line%22%20d%3D%22m17%2033%20h2%20m20%200%20h10%20m62%200%20h10%20m-102%200%20l20%200%20m-1%200%20q-9%200%20-9%20-10%20l0%20-12%20q0%20-10%2010%20-10%20m82%2032%20l20%200%20m-20%200%20q10%200%2010%20-10%20l0%20-12%20q0%20-10%20-10%20-10%20m-82%200%20h10%20m0%200%20h72%20m23%2032%20h-3%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20159%2029%20159%2037%22%2F%3E%3Cpolygon%20points%3D%22151%2033%20143%2029%20143%2037%22%2F%3E%3C%2Fsvg%3E" height="53" width="161">

{%end%}

## Scanner

Je vous est dit que nous humain, nous balayons la commande de nos yeux de gauche à droite, et nous interprétons la série de lettres, chiffres, symboles comme des mots, des groupes ou des nombres.

Notre ordinateur va devoir faire pareil. 

L'opération de balayage se nomme en anglais un *scan*.

A l'état initial le curseur est à la position 0.

```
CREATE TABLE User (id INTEGER, name TEXT(50), email TEXT(25));
^
```

On peut alors le faire avancer de 1, pour consommer un caractère, ici le 'C'

```
CREATE TABLE User (id INTEGER, name TEXT(50), email TEXT(25));
 ^
```

Ou manger plus d'un caractère. Ici on consomme le 'CREATE'.

```
CREATE TABLE User (id INTEGER, name TEXT(50), email TEXT(25));
      ^
```

Nous allons bâtir ce **Scanner**.

Il a beaucoup de point de commun avec le [Cursor](https://doc.rust-lang.org/std/io/struct.Cursor.html) qui permet lui aussi de balayer une chaîne de caractères.

{%note()%}
Notre Scanner sera un wrapper autours de ce Cursor, mais on ne peut pas l'utiliser tel quel car il n'est pas possible d'accrocher des comportements sur une structure qui ne 
nous appartient pas.
{%end%}

Nous allons le rendre générique pour pouvoir l'utiliser sur autre chose que des chaîne de caractères plus tard. D'où le `T`.

```rust
struct Scanner<'a, T: Sized> {
    cursor: Cursor<&'a [T]>,
}
```

Puis on lui accroche des comportements

```rust
impl<'a, T> Scanner<'a, T> {
    /// Create a new Scanner
    pub fn new(input: &'a [T]) -> Scanner<'a, T> {
        Self {
            cursor: Cursor::new(input),
        }
    }

    /// Return the data remaining after the cursor position
    pub fn remaining(&self) -> &'a [T] {
        &self.cursor.get_ref()[self.cursor.position() as usize..]
    }

    /// Return the cursor position in the slice
    pub fn cursor(&self) -> usize {
        self.cursor.position() as usize
    }

    /// Jump the cursor at position can be after or before
    pub fn jump(&mut self, position: usize) {
        self.cursor.set_position(position as u64)
    }

    /// Move the cursor to N position to the right
    pub fn bump_by(&mut self, size: usize) {
        self.cursor.set_position((self.cursor() + size) as u64)
    }

    /// Return all data wrapped by the cursor
    pub fn data(&self) -> &'a [T] {
        self.cursor.get_ref()
    }
}
```

## Erreurs

Pour rendre notre gestion d'erreurs, on se créé notre propre énumération.

```rust
type Result<T> = std::result::Result<T, ParseError>;

#[derive(Debug)]
enum ParseError {
    UnexpectedEOF,
    UnexpectedToken,
}
```

## Visitor

Tout notre fonctionnement de parse va se reposer sur un patron de développement bien connu qui se nomme le [*Pattern Visitor*](https://refactoring.guru/design-patterns/visitor).

Celui-ci permet de définir des cahiers des charges qui indique si l'enchaînement des tokens est valides ou pas sur n'importe quelle stucture de données qui peuvent enchâsser n'importe quelle autre structures de données et ça à profondeur infinie et même récursive.

```rust
trait Visitable<'a, T>: Sized {
    fn accept(scanner: &mut Scanner<'a, T>) -> parser::Result<Self>;
}
```

Ce trait paye pas de mine, mais il est incroyablement puissant! Lorsque l'on bâtira les structures de plus haut niveau, vous verrez ses pouvoirs. ^^

## Tokens

Dans la partie des cahiers des charges nous avons fait l'inventaire des tokens que l'on veut reconnaître.

```rust
enum Token {
    /// whitespace character 0x20 ascii char
    Whitespace,
    /// opening parenthesis "(" 0x28
    OpenParen,
    /// closing parenthesis ")" 0x29
    CloseParen,
    /// comma "," 0x2c
    Comma,
    /// semicolon ";" 0x3b
    Semicolon,
    /// semicolon "*" 0x2a
    Star,
    /// simple quote "'" 0x27
    Quote,
    /// CREATE token
    Create,
    /// TABLE token
    Table,
    /// INSERT token
    Insert,
    /// VALUES token
    Values,
    /// INTO token
    Into,
    /// SELECT token
    Select,
    /// FROM token
    From,
    /// Column type
    Field(Field),
    /// Literal value
    Literal(Literal),
    Wildcard,
}

pub enum Literal {
    /// number recognized
    Number,
    /// string recognized
    String,
}

pub enum Field {
    /// INTEGER token
    Integer,
    /// TEXT token
    Text,
}
```

### Matchers

C'est beau d'avoir une énumération mais comment on passe de notre chaîne de caractères à l'énumération ?

Et bien réfléchissez à comment vous vous lisez. Vous regardez la lettre ou le symbole et vous le comparez à votre base de données stockée quelque part dans votre cerveau.

> Ah ça c'est une virgule! ça c'est un 'a' ! etc ...

```rust
/// On regarde le premier caractère de la chaîne et on le compare
/// avec ce que l'on a besoin de vérifier
fn match_char(input: &str, c: char) -> bool {
    input.as_bytes()[0] == c as u8
}

#[test]
fn test_match_char() {
    assert!(match_char("toto", 't'));
    assert!(!match_char("yoto", 't'));
}
```

Vérifier un token complet comme 'CREATE' est la même chose, mais non plus sur le premier caractère mais sur le pattern complet.

```rust
fn match_pattern(input: &str, pattern: &str) -> bool {
    input[..pattern.len()].as_bytes().to_ascii_lowercase()
        == pattern.as_bytes().to_ascii_lowercase()
}

#[test]
fn test_match_pattern() {
    assert!(match_pattern("create table", "create"));
    assert!(!match_pattern("creata table", "create"));
}
```

Mais on peut faire des choses plus intéressante, comme récupérer un nombre.

```rust
fn match_number(input: &str) -> bool {
    let mut pos = 0;
    let mut found = false;
    let input = input.as_bytes();
    loop {
        if pos == input.len() {
            return found;
        }
        if input[pos].is_ascii_digit() {
            found = true;
            pos += 1;
        } else {
            return found;
        }
    }
}

#[test]
fn test_match_number() {
    assert!(match_number("125titi"));
    assert!(!match_number("titi"))
}
```

Alors c'est intéressant de savoir que c'est un nombre, mais est-ce qu'il fait 1 caractère, 2, 3, 4, plus ?

On sait pas en l'état.

Modifions le retour de la méthode pour renvoyer la position en plus du résultat.

```rust
fn match_number(input: &str) -> (bool, usize) {
    let mut pos = 0;
    let mut found = false;
    let input = input.as_bytes();
    loop {
        if pos == input.len() {
            return (found, pos);
        }
        if input[pos].is_ascii_digit() {
            found = true;
            pos += 1;
        } else {
            return (found, pos);
        }
    }
}

#[test]
fn test_match_number() {
    assert_eq!(match_number("125titi"), (true, 3));
    assert_eq!(match_number("titi"), (false, 0));
}
```

Et à partir de là, il est possible de transformer des caractères ici "125" en un nombre 125.

```rust
let data = "125titi";
let (_, size) = match_number(data);
assert_eq!(data[..size].parse::<i64>(), Ok(125_i64));
```

On peut alors transformer les autres matchers pour également renvoyer la position.

```rust
fn match_char(input: &str, c: char) -> (bool, usize) {
    if input.as_bytes()[0] == c as u8 {
        return (true, 1);
    }
    (false, 0)
}

fn match_pattern(input: &str, pattern: &str) -> (bool, usize) {
    if input[..pattern.len()].as_bytes().to_ascii_lowercase()
        == pattern.as_bytes().to_ascii_lowercase()
    {
        return (true, pattern.len());
    }
    (false, 0)
}
```

Dans ces deux cas on peut alors maintenant déplacer le curseur du bon nombre de caractères en cas de succès.

Une fois que l'on a compris ça, on peut match tout ce qu'on veut, si on dit que une string c'est tout les alphanumeric et '_' alors le matcher est

```rust
fn match_string(input: &str) -> (bool, usize) {
    let mut pos = 0;
    let mut found = false;
    let input = input.as_bytes();
    loop {
        if pos == input.len() {
            return (found, pos);
        }
        if input[pos].is_ascii_alphanumeric() || input[pos] == b'_' {
            found = true;
            pos += 1;
        } else {
            return (found, pos);
        }
    }
}
```

## Reconnaissance des tokens

On a nos token, on des matcher qui nous permettent à partir d'une slice de savoir si les N premiers caractères sont corrects ou non.

Pour cela on introduit un nouveau trait

```rust
trait Recognizable<'a, T, U> {
    fn recognize(self, scanner: &mut Scanner<'a, T>) -> Result<Option<U>>;
}
```

- T : le type de données de la slice du curseur du Scanner
- U : ce que l'on tente de reconnaître

### Tokenizer

Notre Scanner est conçu pour être le plus versatile possible.

Le T peut être fixé comme on l'entend, ici par exemple nous allons le définir à `u8` parce que nous allons uniquement manipuler des bytes.

```rust
type Tokenizer<'a> = Scanner<'a, u8>;
```

### At

Nous n'avons pas encore tout nos protagonistes, laissez moi vous présenter `At`. 😀

Il va avoir pour travail de conserver la place de donnée recherchée dans l'ensemble de la slice.

```rust
fn match_pattern(input: &[u8], pattern: &str) -> (bool, usize) {
    if input[..pattern.len()].to_ascii_lowercase() == pattern.as_bytes().to_ascii_lowercase() {
        return (true, pattern.len());
    }
    (false, 0)
}

#[test]
fn test_at() {
    let source = b"create table users (id integer, name text);";
    let cursor = 7;
    let (_, size) = match_pattern(&source[cursor..], "table");
    let result = At::new(cursor, cursor + size, Token::Table, source);

    assert_eq!(
        String::from_utf8_lossy(&result.source[result.start..result.end]),
        "table"
    );
}
```

Grâce à ce mécanisme, il est possible de conserver une trace de ce l'on a récupéré.

Pour un token ce n'est pas très utile, mais on peut aussi l'utiliser avec le `match_number`

Et là tout de suite l'intérêt devient évident!

```rust
fn match_number(input: &[u8]) -> (bool, usize) {
    let mut pos = 0;
    let mut found = false;
    loop {
        if pos == input.len() {
            return (found, pos);
        }
        if input[pos].is_ascii_digit() {
            found = true;
            pos += 1;
        } else {
            return (found, pos);
        }
    }
}

#[test]
fn test_number() {
    let source = b"name text(50);";
    let cursor = 10;
    let (_, size) = match_number(&source[cursor..]);
    let result = At::new(
        cursor,
        cursor + size,
        Token::Literal(Literal::Number),
        source,
    );

    assert_eq!(
        String::from_utf8_lossy(&result.source[result.start..result.end]),
        "50"
    );
}
```

### Matchers

Les matcher que nous avons créé son bien mais manque de versalité.

Je voudrais les balader dans le code et ne les utiliser que lorsque c'est utile en lazy exécution.

Et il existe un pattern de développement parfait pour cela qui se nomme le [currying](https://fr.wikipedia.org/wiki/Curryfication).

C'est basiquement une fonction qui en renvoit une autre.

En rust, cela passe par les closure et le `impl Fn`, j'ai expliqué toutes ces subtilités dans un [précédent article](/closure).

Curryons le match_char, il devient.

```rust
pub fn match_char(pattern: char) -> impl Fn(&[u8]) -> (bool, usize) {
    move |x: &[u8]| (x[0] == pattern as u8, 1)
}
```

On peut alors l'utiliser ainsi

```rust
#[test]
fn test_match_char_curry() {
    let match_c = super::match_char('c');
    assert_eq!(match_c("c".as_bytes()), (true, 1));
    assert_eq!(match_c("t".as_bytes()), (false, 1));
}
```

On voit que l'on retarde l'application du match.

Voici les matchers en mode curried

{%detail(title="Matchers curried")%}
```rust
pub fn match_char(pattern: char) -> impl Fn(&[u8]) -> (bool, usize) {
    move |x: &[u8]| (x[0] == pattern as u8, 1)
}

pub fn match_pattern(pattern: &str) -> impl Fn(&[u8]) -> (bool, usize) + use<'_> {
    move |x: &[u8]| -> (bool, usize) {
        if pattern.is_empty() {
            return (false, 0);
        }
        if pattern.len() > x.len() {
            return (false, 0);
        }
        if pattern.as_bytes().to_ascii_lowercase() == x[..pattern.len()].to_ascii_lowercase() {
            return (true, pattern.len());
        }
        (false, 0)
    }
}

pub fn match_predicate<F>(predicate: F) -> impl Fn(&[u8]) -> (bool, usize)
where
    F: Fn(&[u8]) -> (bool, usize),
{
    move |x: &[u8]| predicate(x)
}

pub fn match_number() -> impl Fn(&[u8]) -> (bool, usize) {
    move |x: &[u8]| -> (bool, usize) {
        if x.is_empty() {
            return (false, 0);
        }

        let mut pos = 0;
        let mut found = false;
        loop {
            if pos == x.len() {
                return (found, pos);
            }
            if x[pos].is_ascii_digit() {
                found = true;
                pos += 1;
            } else {
                return (found, pos);
            }
        }
    }
}

pub fn match_string() -> impl Fn(&[u8]) -> (bool, usize) {
    move |x: &[u8]| -> (bool, usize) {
        if x.is_empty() {
            return (false, 0);
        }
        let mut pos = 0;
        let mut found = false;

        loop {
            if pos == x.len() {
                return (found, pos);
            }
            if x[pos].is_ascii_alphanumeric() || x[pos] == b'_' {
                found = true;
                pos += 1;
            } else {
                return (found, pos);
            }
        }
    }
}
```
{%end%}

### Utiliser le Tokenizer pour reconnaître les tokens

On accroche à notre Tokenizer un fonction qui prend un 'matcher' en paramètre

```rust
impl Tokenizer<'_> {
    pub fn recognize<F>(&mut self, matcher: F) -> parser::Result<(bool, usize)>
    where
        F: Fn(&[u8]) -> (bool, usize),
    {
        if self.remaining().is_empty() {
            return Err(ParseError::UnexpectedEOF);
        }

        let data = self.remaining();
        Ok(matcher(data))
    }
}
```

### Matcher les tokens

On y est presque !

On introduit un nouveau trait `Match` et un type `Matcher`.

```rust
pub type Matcher = Box<dyn Fn(&[u8]) -> (bool, usize)>;

pub trait Match {
    fn matcher(&self) -> Matcher;
}
```

Le Box est obligatoire car deux closures différentes sont deux types de structures différentes or dans la suite nous avons un `match` qui force la cohérence des types. 

Que l'on applique à `Token`.

```rust
impl Match for Token {
    fn matcher(&self) -> Matcher {
        match self {
            Token::Whitespace => Box::new(matchers::match_predicate(|x| {
                (x[0].is_ascii_whitespace(), 1)
            })),
            Token::OpenParen => Box::new(matchers::match_char('(')),
            Token::CloseParen => Box::new(matchers::match_char(')')),
            Token::Comma => Box::new(matchers::match_char(',')),
            Token::Create => Box::new(matchers::match_pattern("create")),
            Token::Table => Box::new(matchers::match_pattern("table")),
            Token::Insert => Box::new(matchers::match_pattern("insert")),
            Token::Into => Box::new(matchers::match_pattern("into")),
            Token::Values => Box::new(matchers::match_pattern("values")),
            Token::Select => Box::new(matchers::match_pattern("select")),
            Token::From => Box::new(matchers::match_pattern("from")),
            Token::Semicolon => Box::new(matchers::match_char(';')),
            Token::Star => Box::new(matchers::match_char('*')),
            Token::Quote => Box::new(matchers::match_char('\'')),
            Token::Field(field) => field.matcher(),
            Token::Literal(literal) => literal.matcher(),
        }
    }
}

impl Match for Literal {
    fn matcher(&self) -> Matcher {
        match self {
            Literal::Number => Box::new(matchers::match_number()),
            Literal::String => Box::new(matchers::match_string()),
        }
    }
}

impl Match for Field {
    fn matcher(&self) -> Matcher {
        match self {
            Field::Text => Box::new(matchers::match_pattern("Text")),
            Field::Integer => Box::new(matchers::match_pattern("Number")),
        }
    }
}
```

Finalement nous pouvons vérouiller le type de `At<'a, T, U>` en `At<'a, u8, Token>`. Car nous ne manipulerons que des bytes qui deviennent des Token.

```rust
type TokenOutput<'a> = At<'a, Token, u8>;
```

Pour enfin implémenter `Recognizable` sur `Token` !!

```rust
impl<'a> Recognizable<'a, u8, TokenOutput<'a>> for Token {
    fn recognize(self, tokenizer: &mut Tokenizer<'a>) -> parser::Result<Option<TokenOutput<'a>>> {
        // l'appel à matcher() est possible car Token implémente le trait Match
        // l'appel à recognize est possible car Tokenizer possède cette méthode (pas le Scanner)
        let (result, size) = tokenizer.recognize(self.matcher())?;
        if !result {
            return Ok(None);
        }
        let pos = tokenizer.cursor();
        // mais Tokenizer est un scanner et donc possède toutes les méthodes
        // du Scanner
        if !tokenizer.remaining().is_empty() {
            // comme faire avancer le curseur du nombre de bytes consommer par le Token
            tokenizer.bump_by(size);
        }
        // Finalement on renvoie le At
        Ok(Some(At::new(pos, pos + size, self, tokenizer.data())))
    }
}
```

Et on utilise cela ainsi

```rust
#[test]
fn test_recognize_token() {
    let data = b"create table user (id number, name text);";
    let mut tokenizer = Tokenizer::new(data);
    assert!(Token::Create
        .recognize(&mut tokenizer)
        .expect("Unable to recognize CREATE token")
        .is_some());

    let data = b"creata table user (id number, name text);";
    let mut tokenizer = Tokenizer::new(data);
    assert!(Token::Create
        .recognize(&mut tokenizer)
        .expect("Unable to recognize CREATE token")
        .is_none());
}
```

On peut finalement se faire une petite fonction utilitaire.

```rust
pub fn recognize<'a, U, R: Recognizable<'a, u8, U>>(
    element: R,
    scanner: &mut Tokenizer<'a>,
) -> Result<U> {
    element
        .recognize(scanner)?
        .ok_or(ParseError::UnexpectedToken)
}
```

Qui s'utilise ainsi

```rust
#[test]
fn test_recognize() {
    let data = b"create table user (id number, name text);";
    let mut tokenizer = Tokenizer::new(data);
    assert!(recognize(Token::Create, &mut tokenizer).is_ok());
    assert!(recognize(Token::Whitespace, &mut tokenizer).is_ok());
    assert!(recognize(Token::Table, &mut tokenizer).is_ok());
}
```

On voit bien que l'on se déplace dans la slice et que l'on match les tokens au fur et à mesure. 😍

## Composants

Maintenant que nous avons des tokens, nous allons pouvoir les assembler.

Pour y arriver nous allons ajouter un comportement à notre Scanner

```rust
impl<'a, T> Scanner<'a, T> {
    pub fn visit<U: Visitable<'a, T>>(&mut self) -> parser::Result<U> {
        U::accept(self)
    }
}
```

Tout objet U qui implémente Visitable est maintenant capable de visiter le Scanner, ce pattern s'appelle une [blanket implementation](/blanket-impl).

### Whitespaces

Pour mette en pratique, prenons l'exemple le plus simple: capturer plus d'un espace blanc.

On se créé une structure `Whitespaces`.

```rust
pub struct Whitespaces;
```

Pas besoin de champ dans cette structure, ce n'est qu'une accroche pour y appliquer le Visitor.

```rust
impl<'a> Visitable<'a, u8> for Whitespaces {
    fn accept(scanner: &mut Scanner<'a, u8>) -> parser::Result<Self> {
        let mut counter = 0;
        while Token::Whitespace.recognize(scanner)?.is_some() {
            counter += 1;
        }
        if counter == 0 {
            return Err(ParseError::UnexpectedToken);
        }
        Ok(Whitespaces)
    }
}
```

On peut alors consommer nos espaces blancs depuis le Tokenizer qui est un Scanner et donc qui possède la méthode "visit".

```rust
#[test]
fn test_whitespaces() {
    let data = b"    data";
    let mut tokenizer = Tokenizer::new(data);
    tokenizer
        .visit::<Whitespaces>()
        .expect("failed to parse whitespaces");
    assert_eq!(tokenizer.remaining(), b"data");
}

#[test]
fn test_whitespaces_fail() {
    let data = b"data";
    let mut tokenizer = Tokenizer::new(data);
    assert!(tokenizer.visit::<Whitespaces>().is_err());
}
```

C'est le niveau 1 de la puissance du Visitor, mais on peut l'utiliser pour faire des choses bien plus complexe.

### Assembler des Visitor

On va faire un example un artificiel.

Par exemple on peut définir un Visiteur pour l'echaînement de tokens [CREATE,TABLE].

```rust
pub struct CreateTablePrefix;

impl<'a> Visitable<'a, u8> for CreateTablePrefix {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        recognize(Token::Create, scanner)?;
        scanner.visit::<Whitespaces>()?;
        recognize(Token::Table, scanner)?;
        Ok(CreateTablePrefix)
    }
}


#[test]
fn test_create_table_prefix() {
    let input = b"CREATE   TABLE  User";
    let mut scanner = Scanner::new(input);
    assert!(CreateTablePrefix::accept(&mut scanner).is_ok());
    assert_eq!(scanner.remaining(), b"  User");
}
```

On y mélange à la fois du Recognizable et du Visitable.

Et puis on peut aussi avoir des composants qui ont plus de logique, comme pour capturer le nom de la table.

```rust
pub struct Table(String);

impl<'a> Visitable<'a, u8> for Table {
    fn accept(scanner: &mut Scanner<'a, u8>) -> parser::Result<Self> {
        let tokens = recognize(Token::Literal(Literal::String), scanner)?;
        let data = String::from_utf8_lossy(&tokens.source[tokens.start..tokens.end]);
        Ok(Table(data.into()))
    }
}

// petit utilitaire pour récupérer le contenu
impl From<Table> for String {
    fn from(table: Table) -> String {
        table.0
    }
}
```

Et alors on peut même esquisser le travail du [prochain article](/rustqlite-6) de la série. En capturant le nom de la table à créer.

```rust
struct CreateTableCommand {
    table_name: String,
}

impl<'a> Visitable<'a, u8> for CreateTableCommand {
    fn accept(scanner: &mut Scanner<'a, u8>) -> parser::Result<Self> {
        // on visite les tokens CREATE TABLE
        scanner.visit::<CreateTablePrefix>()?;
        // suivi d'autant d'espace blancs que l'on veut, mais au moins un
        scanner.visit::<Whitespaces>()?;
        // finalement on récupère le literal string qui représente le nom de la table
        let table_name = scanner.visit::<Table>()?.into();
        // et on retourne la commande
        Ok(CreateTableCommand { table_name })
    }
}

#[test]
fn test_parse_create_table_command() {
    let data = b"create table   users   (   id   number,   name   text(50) )";
    let mut scanner = Scanner::new(data);
    let result = CreateTableCommand::accept(&mut scanner).expect("failed to parse");
    assert_eq!(
        result,
        CreateTableCommand {
            table_name: "users".to_string()
        }
    );
}
```

J'espère que vous commencez à entrevoir la puissance du pattern de développement que l'on a choisi ? 😊

Tout est composable, et donc on peut assembler des morceaux relativement simple et bâtir des cathédrales de complexité pour parser des choses
qui de prime abord semblent insurmontables.

## Groupes

Un autre problème dans l'art du parsing est d'avoir la capacité de comprendre le concept de groupe.

Si on prend la commande de création de table, le groupe est représenté par un ensemble de caractères délimité par "(" et ")".

```
create table   users   (   id   number,   name   text(50) );
                       ^                                  ^
                       début                              fin
```

Nous devons trouver un moyen de faire comprendre à la machine cette notion.

### Forecasting

Savoir si on commence un groupe c'est facile, on match un token ici le "(".

Mais par contre trouver la fin du groupe est plus tricky.

Il faut littéralement voir le futur du parse, voir au-delà du curseur du Scanner.

On défini un trait pour réprésenter ce comportement de recherche.

```rust
trait Forecast<'a, T, S> {
    fn forecast(&self, data: &mut Scanner<'a, T>) -> parser::Result<ForecastResult<S>>;
}
```

Et une enumeration pour représenter le résultat de ce comportement.

```rust
enum ForecastResult<S> {
    /// Le groupe a été trouvé
    Found {
        /// token de début de groupe 
        start: S, 
        /// token de fin de groupe
        end: S 
        /// position de la fin du groupe relativement
        /// à la position du curseur
        end_slice: usize, 
    },
    /// Le groupe n'est pas trouvable depuis la position actuelle du scanner
    NotFound,
}
```

On peut alors en faire une fonction utilitaire qui permet de matcher tout ce qui est capable d'implémenter Forecast.

```rust
pub fn forecast<'a, U, F: Forecast<'a, u8, U>>(
    element: F,
    tokenizer: &mut Tokenizer<'a>,
) -> parser::Result<Forecasting<'a, u8, U>> {
    let source_cursor = tokenizer.cursor();
    if let ForecastResult::Found {
        start,
        end,
        end_slice,
    } = element.forecast(tokenizer)?
    {
        let data = &tokenizer.data()[source_cursor..source_cursor + end_slice];
        return Ok(Forecasting { start, end, data });
    }
    Err(ParseError::UnexpectedToken)
}
```

### Groupe délimité

Ok! Là on rentre dans le dur, on va régler le problème que toute personne qui s'attaque à la création d'un parser rencontre : les groupes imbriqués.

Voici le vrai groupe.

```
create table   users   (   id   number,   name   text(50) );
                       ^                                  ^
                       début                              fin
```

Le contenu est alors : "(   id   number,   name   text(50) )"

Il ne faut surtout pas matcher le premier token ")".

```
create table   users   (   id   number,   name   text(50) );
                       ^                                ^
                       début                            mauvaise fin
```

Sinon on se retrouve avec un groupe incorrect : "(   id   number,   name   text(50)"

L'idée est alors de compter les tokens de début et de fin pour savoir si on délimite correctement ou non.

Voici une implémentation qui renvoie un ForecastResult.

```rust
fn match_for_balanced_group(
    tokenizer: &mut Tokenizer,
    balance: &mut usize,
    start: Token,
    end: Token,
) -> parser::Result<()> {
    // try to recognize start group token
    match start.recognize(tokenizer)? {
        // if not start token try to recognize end group token
        None => match end.recognize(tokenizer)? {
            // if end group token decrement balancing counter
            Some(_end_token) => *balance -= 1,
            // if neither, move by one byte
            None => {
                tokenizer.bump_by(1);
                return Ok(());
            }
        },
        // if start group token increment balancing counter
        Some(_start_token) => *balance += 1,
    };

    Ok(())
}

pub fn match_group<'a>(
    start: Token,
    end: Token,
) -> impl Fn(&[u8]) -> parser::Result<ForecastResult<Token>> + 'a {
    move |input| {
        // 0 if number of start token equals number of end token
        // i.e: ( 5 + 3 - ( 10 * 8 ) ) => 2 "(" and 2 ")" => balanced
        //      ( 5 + 3 - ( 10 * 8 )   => 2 "(" and 1 ")" => unbalanced
        let mut balance = 0;

        let mut tokenizer = Tokenizer::new(input);

        loop {
            match_for_balanced_group(&mut tokenizer, &mut balance, start, end)?;
            // if balancing is 0 then either there is no group at all or is balanced
            if balance == 0 {
                break;
            }
        }

        // not enough bytes to create a group
        if tokenizer.cursor() == 1 {
            return Ok(ForecastResult::NotFound);
        }

        Ok(ForecastResult::Found {
            end_slice: tokenizer.cursor(),
            start,
            end,
        })
    }
}
```

On peut alors matcher les groupes qui nous intéressent.

```rust
#[test]
fn test_match_group() {
    let data = b"( 5 + 3 - ( 10 * 8 ) ) + 54";
    let result =
        match_group(Token::OpenParen, Token::CloseParen)(data).expect("failed to parse");
    assert_eq!(
        result,
        ForecastResult::Found {
            end_slice: 22,
            start: Token::OpenParen,
            end: Token::CloseParen
        }
    );
    assert_eq!(&data[..22], b"( 5 + 3 - ( 10 * 8 ) )");
}
```

On se créé alors un objet GroupKind pour lui accrocher le comportement Forecast.

```rust
pub enum GroupKind {
    Parenthesis,
}

impl GroupKind {
    fn matcher<'a>(&self) -> Box<impl Fn(&'a [u8]) -> parser::Result<ForecastResult<Token>>> {
        match self {
            GroupKind::Parenthesis => Box::new(match_group(Token::OpenParen, Token::CloseParen)),
        }
    }
}

impl<'a> Forecast<'a, u8, Token> for GroupKind {
    fn forecast(&self, data: &mut Tokenizer<'a>) -> parser::Result<ForecastResult<Token>> {
        self.matcher()(data.remaining())
    }
}
```

Et alors cela nous ouvre le droit d'utiliser notre méthode forecast

```rust
#[test]
fn test_match_group_delimited() {
    let data = b"( 5 + 3 - ( 10 * 8 ) ) + 54";
    let mut tokenizer = Tokenizer::new(data);
    let result = forecast(GroupKind::Parenthesis, &mut tokenizer).expect("failed to parse");
    assert_eq!(
        result,
        Forecasting {
            start: Token::OpenParen,
            end: Token::CloseParen,
            data: &data[0..22],
        }
    );
    assert_eq!(&data[..22], b"( 5 + 3 - ( 10 * 8 ) )");
}
```

### Separated List

Le concept de liste séparée est assez basique.

Vous avez par exemple l'enchaînement suivant `12,4,78,22`.

Et vous voulez récupérer les nombres `12`, `4`, `78` et `22`.

Nous avons alors deux critères:
- le séparateur que l'on doit reconnaître 
- les éléments qui doivent être visités

Attention, le dernier élément ne possède pas toujours de séparateur terminal.

Nous allons accrocher nos comportement sur une structure.

```rust
/// S : le séparateur
/// T : les éléments de la liste
struct SeparatedList<T, S> {
    data: Vec<T>,
    separator: PhantomData<S>,
}
```

On défini 

```rust
enum YieldResult<V> {
    /// on a visité et il est possible 
    /// qu'un autre élément soit visitable
    /// cas: 1,2,3,
    MaybeNext(V),
    /// la dernière visite indique qu'il n'y a
    /// plus rien à visiter
    /// cas: 1,2,3
    Last(V),
}
```

```rust
fn yield_element<'a, V, S>(scanner: &mut Scanner<'a, u8>) -> parser::Result<YieldResult<V>>
where
    V: Visitable<'a, u8> + Debug,
    S: Visitable<'a, u8> + Debug,
{
    // visite de l'élement (il est assuré d'en avoir au moins un, sinon c'est une erreur)
    let element: V = scanner.visit()?;

    // s'il n'y a plus rien à parser c'est le dernier élément
    if scanner.remaining().is_empty() {
        return Ok(YieldResult::Last(element));
    }

    // on consomme le séparateur si ce n'est pas encore la fin
    scanner.visit::<S>()?;

    // on retourne l'élément visité
    Ok(YieldResult::MaybeNext(element))
}
```

On peut alors accrocher le Visitable dessus

```rust
impl<'a, T, S> Visitable<'a, u8> for SeparatedList<T, S>
where
    T: Visitable<'a, u8>,
    S: Visitable<'a, u8>,
{
    fn accept(scanner: &mut Tokenizer<'a>) -> parser::Result<Self> {
        let mut elements = vec![];
        // on enregistre la position du curseur
        let cursor = scanner.cursor();

        loop {
            if let Ok(result) = yield_element::<T, S>(scanner) {
                let element: YieldResult<T> = result;

                match element {
                    YieldResult::Last(element) => {
                        elements.push(element);
                        break;
                    }
                    YieldResult::MaybeNext(element) => {
                        elements.push(element);
                    }
                }
            } else {
                // on rétabli le curseur
                scanner.jump(cursor);
                break;
            }
        }

        Ok(SeparatedList {
            data: elements,
            separator: PhantomData,
        })
    }
}
```

Pour tester je vais créer deux Visitable artificiels.

```rust
pub struct SeparatorComma;

impl<'a> Visitable<'a, u8> for SeparatorComma {
    fn accept(scanner: &mut Tokenizer<'a>) -> parser::Result<Self> {
        recognize(Token::Comma, scanner)?;
        Ok(SeparatorComma)
    }
}

pub struct Number(i64);

impl<'a> Visitable<'a, u8> for Number {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        let data = recognize(Token::Literal(Literal::Number), scanner)?;
        let value = String::from_utf8_lossy(&data.source[data.start..data.end]).parse().unwrap();
        Ok(Number(value))
    }
}
```

Et on peut alors extraire ce que l'on veut.

```rust
#[test]
fn test_parse_number_list() {
    let data = b"12,4,78,22";
    let mut tokenizer = super::Tokenizer::new(data);
    let result = tokenizer
        .visit::<SeparatedList<Number, SeparatorComma>>()
        .expect("failed to parse");
    assert_eq!(
        result.data,
        vec![Number(12), Number(4), Number(78), Number(22)]
    );
    assert_eq!(tokenizer.cursor(), 10);
}
```

{%warning()%}
Attention, le concepteur du parseur est responsable de la chaîne visitée par le SeparatedList.

Si elle contient plus de token que la liste elle-même, par exemple "1,2,3) + (4,5,6)"

Alors le parse échouera à la première ")".

La SeparatedList doit-être utilisé dans un contexte de groupe délimité.
{%end%}


## Acceptor

Allez dernier concept et on aura fini. 😅 Courage ! 🙏

Le principe d'Acceptor est de pouvoir enchaîner plusieurs essaie de Visitable et matcher le bon.

On a esquissé le `Create Table`

On va esquisser le `Select * From`

```rust
pub struct SelectCommand;

impl<'a> Visitable<'a, u8> for SelectCommand {
    fn accept(scanner: &mut Tokenizer<'a>) -> parser::Result<Self> {
        recognize(Token::Select, scanner)?;
        scanner.visit::<Whitespaces>()?;
        recognize(Token::Star, scanner)?;
        scanner.visit::<Whitespaces>()?;
        recognize(Token::From, scanner)?;

        Ok(SelectCommand)
    }
}
```

L'Acceptor permet de chaîner nos opération de visites

```rust
pub struct Acceptor<'a, 'b, T, V> {
    data: Option<V>,
    scanner: &'b mut Scanner<'a, T>,
}

impl<'a, 'b, T, V> Acceptor<'a, 'b, T, V> {
    pub fn new(scanner: &'b mut Scanner<'a, T>) -> Self {
        Self {
            data: None,
            scanner,
        }
    }
}
```

Il prend une fonction appelé un *transformer* qui permet de transformer le contenu de la variante en une variante d'énumération.

```rust
impl<'a, 'b, T, V> Acceptor<'a, 'b, T, V> {
    pub fn try_or<U: Visitable<'a, T>, F>(mut self, transformer: F) -> parser::Result<Self>
    where
        F: Fn(U) -> parser::Result<V>,
    {
        // Propagate result
        if self.data.is_some() {
            return Ok(self);
        }

        // Or apply current recognizer
        if let Ok(found) = U::accept(self.scanner) {
            self.data = Some(transformer(found)?);
        }

        Ok(self)
    }

    pub fn finish(self) -> Option<V> {
        self.data
    }
}
```

Et alors, on a un système très versatile.

```rust
enum Command {
    CreateTable(CreateTableCommand),
    Select(SelectCommand),
}

impl<'a> Visitable<'a, u8> for Command {
    fn accept(scanner: &mut Scanner<'a, u8>) -> crate::parser::Result<Self> {
        Acceptor::new(scanner)
            // le système de type de Rust vient fixer le type de Visteur en se
            // basant sur le type de la variante
            .try_or(|command| Ok(Command::Select(command)))?
            .try_or(|command| Ok(Command::CreateTable(command)))?
            .finish()
            .ok_or(ParseError::UnexpectedToken)
    }
}

#[test]
fn test_parse_select() {
    let data = b"SELECT * FROM Users";
    let mut tokenizer = Tokenizer::new(data);
    let command = tokenizer.visit::<Command>().expect("failed to parse");
    assert_eq!(command, Command::Select(SelectCommand));
}

#[test]
fn test_parse_create_table() {
    let data = b"CREATE TABLE Users";
    let mut tokenizer = Tokenizer::new(data);
    let command = tokenizer.visit::<Command>().expect("failed to parse");
    assert_eq!(
        command,
        Command::CreateTable(CreateTableCommand {
            table_name: "Users".to_string(),
        })
    );
}
```

Du point de vue du constructeur de la grammaire du parseur, il manipule des concepts de haut-niveau qui viennent abstraire les opérations de bas niveau de reconnaissance
de tokens, de leur enchaînement puis de leur transformation.


## Conclusion

Bon c'était un peu complexe et long, mais bravo si vous êtes arrivé jusqu'ici. 😎

Nous avons bâtis tous les outils nécessaire pour parser à peu près n'importe quoi.

Dans la [prochaine partie](rustqlite-6), nous mettrons en place la vraie grammaire. Promis ça sera bien plus simple. 😅

Merci de votre lecture ❤️

Vous pouvez trouver le code la partie [ici](https://gitlab.com/blog_example/sqlite-en-rust/-/tree/5-parser?ref_type=heads) et le [diff](https://gitlab.com/blog_example/sqlite-en-rust/-/compare/4-tables...5-parser) là.
